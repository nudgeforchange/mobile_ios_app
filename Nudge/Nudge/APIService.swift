import Alamofire

class APIService: NSObject {
    
    static let domain = "d.nudgeforchange.com"
    //static let domain = "dev-api.d.nudgeforchange.com"
    static let apiVersion = "v1"
    static let baseURL = "https://\(APIService.domain)/api/\(APIService.apiVersion)"
    
    private func getCookieString() -> String? {
        let allCookies = HTTPCookieStorage.shared.cookies
        let cookies = allCookies?.filter({ (cookie) -> Bool in
            cookie.domain.contains(APIService.domain)
        })
        let cookieStrings = cookies?.map({ (cookie) -> String in
//            let created = cookie.created!
//            let expires = cookie.expiresDate!
//            let maxAge = 
//            return "\(cookie.name)=\(cookie.value); expires=\(expires); HttpOnly; Max-Age=\(maxAge); Path=/; Secure"
            return "\(cookie.name)=\(cookie.value); HttpOnly; Path=/; Secure"
        })
        let cookieString = cookieStrings?.joined(separator: ", ")
        return cookieString
    }
    
    func getSecureHeader() -> HTTPHeaders? {
        let token = SettingsService.sharedInstance.nudgeToken
        let cookie = self.getCookieString()
        
        if let token = token, let cookie = cookie {
            let headers: HTTPHeaders = [
                "Authorization": "JWT \(token)",
                "Cookie": cookie
            ]
            return headers
        }
        
        return nil
        
//        let token = SettingsService.sharedInstance.nudgeToken
//        let cookie = self.getCookieString()
//        
//        if let token = token, let cookie = cookie {
//            let headers: HTTPHeaders = [
//                "Authorization": "JWT \(token)",
//                "Cookie": cookie
//            ]
//            return headers
//        }
//        
//        return nil
        
//        var sessionID = ""
//        var expirationDate = Date()
//        if let cookies = HTTPCookieStorage.shared.cookies {
//            for cookie in cookies {
//                if cookie.domain.contains("d.nudgeforchange.com") {
//                    sessionID = cookie.value
//                    expirationDate = cookie.expiresDate!
//                    break
//                }
//            }
//        }
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "EEE, dd-MMM-YYYY hh:mm:ss"
//        let formattedDate = dateFormatter.string(for: expirationDate)!
//        let dateString = "\(formattedDate) GMT"
//        let cookie = "sessionid=\(sessionID); expires=\(dateString); HttpOnly; Max-Age=1209600; Path=/"
//        let headers : HTTPHeaders = [
//            "authorization" : "JWT \(token)",
//            "cookie" : cookie]
//        return headers
    }
    
    func authenticateIfNecessary(completion: @escaping () -> Void) {
        if AuthenticationService.sharedInstance.didTokenExpire() {
            AuthenticationService.sharedInstance.authenticate(attempt: 0) {
                completion()
            }
        } else {
            completion()
        }
    }
    
}
