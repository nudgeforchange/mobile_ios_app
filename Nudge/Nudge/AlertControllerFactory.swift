import UIKit
import Social
import FBSDKShareKit

class AlertControllerFactory: NSObject {
    
    // MARK: Nudging Away
    
    static func nudgingAwayShareAlert(completion: @escaping (_ serviceType: String?) -> Void) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: "Good job!", message: "Let your voice be heard and tell your friends why you are nudging away.", preferredStyle: .alert)
        let facebookAction: UIAlertAction = UIAlertAction(title: "Share on Facebook", style: .default) { (action) in
            completion(SLServiceTypeFacebook)
        }
        let twitterAction: UIAlertAction = UIAlertAction(title: "Share on Twitter", style: .default) { (action) in
            completion(SLServiceTypeTwitter)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(nil)
        }
        alert.addAction(facebookAction)
        alert.addAction(twitterAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    static func nudgingToShareAlert(completion: @escaping (_ serviceType: String?) -> Void) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: "Good job!", message: "Let your voice be heard and tell your friends why you are nudging to this business.", preferredStyle: .alert)
        let facebookAction: UIAlertAction = UIAlertAction(title: "Share on Facebook", style: .default) { (action) in
            completion(SLServiceTypeFacebook)
        }
        let twitterAction: UIAlertAction = UIAlertAction(title: "Share on Twitter", style: .default) { (action) in
            completion(SLServiceTypeTwitter)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(nil)
        }
        alert.addAction(facebookAction)
        alert.addAction(twitterAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    static func nudgingAwayConfirmAlert(businessName: String, completion: @escaping (_ didNudgeAway: Bool) -> Void) -> UIAlertController {
        let alert: UIAlertController = UIAlertController(title: "Nudging Away?", message: "Would you like to nudge away from \(businessName)?", preferredStyle: .alert)
        let confirmAction: UIAlertAction = UIAlertAction(title: "Nudge away", style: .default) { (action) in
            AnalyticsService.shared.track(AnalyticsEvent.nudgedAway)
            completion(true)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(false)
        }
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    
    // MARK: Nudge Detail options
    
    static func blacklistConfirmedAlert(businessName: String, scope: Int) -> UIAlertController {
        let qualifier = scope == 0 ? "any" : "this"
        let location = scope == 0 ? "locations" : "location"
        let alert = UIAlertController(title: "Got it!", message: "We won't suggest \(qualifier) \(businessName) \(location) again.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        alert.addAction(okAction)
        return alert
    }
    
    static func blacklistReasonActionSheet(scope: Int, completion: @escaping (_ scope: Int, _ reason: NoNudgeBlacklistReason?) -> Void) -> UIAlertController {
        let alert = UIAlertController(title: "Why?", message: nil, preferredStyle: .actionSheet)
        let dislikeAction = UIAlertAction(title: "Just not a fan", style: .default) { (action) in
            completion(scope, .notAFan)
        }
//        let nearAction = UIAlertAction(title: "It's near my home, work, etc.", style: .default) { (action) in
//            completion(scope, .nearCommonPlace)
//        }
        let disagreeAction = UIAlertAction(title: "Disagree with scoring", style: .default) { (action) in
            completion(scope, .scoreDisagreement)
        }
        let wayOffAction = UIAlertAction(title: "Way off! (wrong kind of business)", style: .default) { (action) in
            completion(scope, .erroneous)
        }
        let otherAction = UIAlertAction(title: "Other", style: .default) { (action) in
            completion(scope, .other)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(scope, nil)
        }
        alert.addAction(dislikeAction)
//        alert.addAction(nearAction)
        alert.addAction(disagreeAction)
        alert.addAction(wayOffAction)
        alert.addAction(otherAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    static func blacklistActionSheet(businessName: String, completion: @escaping (_ scope: Int?) -> Void) -> UIAlertController {
        let alert = UIAlertController(title: "Don't suggest \(businessName) again:", message: nil, preferredStyle: .actionSheet)
        let anyAction = UIAlertAction(title: "Any of their locations", style: .default) { (action) in
            completion(0)
        }
        let thisAction = UIAlertAction(title: "This location only", style: .default) { (action) in
            completion(1)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(nil)
        }
        alert.addAction(anyAction)
        alert.addAction(thisAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    static func whitelistConfirmedAlert(businessName: String, scope: Int) -> UIAlertController {
        let qualifier = scope == 0 ? "any" : "this"
        let location = scope == 0 ? "locations" : "location"
        let alert = UIAlertController(title: "Got it!", message: "We won't nudge you away from \(qualifier) \(businessName) \(location).", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        alert.addAction(okAction)
        return alert
    }
    
    static func whitelistReasonActionSheet(scope: Int, completion: @escaping (_ scope: Int, _ reason: NoNudgeWhitelistReason?) -> Void) -> UIAlertController {
        let alert = UIAlertController(title: "Why?", message: nil, preferredStyle: .actionSheet)
        let guiltyAction = UIAlertAction(title: "It's a guilty pleasure", style: .default) { (action) in
            completion(scope, .guiltyPleasure)
        }
        let nearAction = UIAlertAction(title: "It's near my home, work, etc.", style: .default) { (action) in
            completion(scope, .nearCommonPlace)
        }
        let disagreeAction = UIAlertAction(title: "Disagree with scoring", style: .default) { (action) in
            completion(scope, .scoreDisagreement)
        }
        let wayOffAction = UIAlertAction(title: "Way off! (business or location is wrong)", style: .default) { (action) in
            completion(scope, .erroneous)
        }
        let otherAction = UIAlertAction(title: "Other", style: .default) { (action) in
            completion(scope, .other)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(scope, nil)
        }
        alert.addAction(guiltyAction)
        alert.addAction(nearAction)
        alert.addAction(disagreeAction)
        alert.addAction(wayOffAction)
        alert.addAction(otherAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    static func whitelistActionSheet(businessName: String, completion: @escaping (_ scope: Int?) -> Void) -> UIAlertController {
        let alert = UIAlertController(title: "Don't nudge me away from \(businessName):", message: nil, preferredStyle: .actionSheet)
        let anyAction = UIAlertAction(title: "Any of their locations", style: .default) { (action) in
            completion(0)
        }
        let thisAction = UIAlertAction(title: "This location only", style: .default) { (action) in
            completion(1)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(nil)
        }
        alert.addAction(anyAction)
        alert.addAction(thisAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
}
