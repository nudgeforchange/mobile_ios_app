import Mixpanel

struct AnalyticsEvent {
    
    static let appLaunched = "app launched" //
    static let appLaunchedLocation = "app launched (location)" //
    static let appForegrounded = "app foregrounded" //
    static let locationUpdated = "location updated" //
    static let geofenceTriggered = "geofence triggered" //
    static let nudgeNotificationSent = "nudge notification sent" //
    static let nudgeNotificationEngaged = "nudge notification engaged" //
    static let nudgeViewed = "nudge viewed" //
    static let nudgeRedFlagDetailViewed = "nudge red flag detail viewed" //
    static let nudgeGreenFlagDetailViewed = "nudge green flag detail viewed" //
    static let nudgedAway = "nudged away" //
    static let blacklistedLocation = "blacklisted location" //
    static let blacklistedBusiness = "blacklisted business" //
    static let whitelistedLocation = "whitelisted location" //
    static let whitelistedBusiness = "whitelisted business" //
    static let facebookShared = "facebook shared" //
    static let facebookCancelled = "facebook cancelled" //
    static let twitterShared = "twitter shared" //
    static let twitterCancelled = "twitter cancelled" //
    
    static let openedMapApp = "opened map app" //
    
    // search
    static let searchedBusinesses = "searched businesses" //
    static let searchRedFlagDetailViewed = "search red flag detail viewed" //
    static let searchGreenFlagDetailViewed = "search green flag detail viewed" //
    
    // onboarding
    static let onboardingFirstPageViewed = "onboarding first page viewed" //
    static let onboardingSecondPageViewed = "onboarding second page viewed" //
    static let onboardingThirdPageViewed = "onboarding third page viewed" //
    static let onboardingBeliefsSet = "onboarding beliefs set" //
    static let onboardingNotificationsEnabled = "onboarding notifications enabled" //
    static let onboardingLocationEnabled = "onboarding location enabled" //
    static let onboardingCompleted = "onboarding completed" //

}

class AnalyticsService: NSObject {
    
    static let shared = AnalyticsService()
    
    #if RELEASE
    let mixpanel = Mixpanel.initialize(token: "5a04e254dc752f881e49a37dc76a6790")
    #else
    let mixpanel = Mixpanel.initialize(token: "ae5f518ee0570327049d4f993ff5c2aa")
    #endif
    
    var superProperties: [AnyHashable : Any] {
        get {
            return ["something" : "something"]
        }
    }
    
    private override init() {
        if let nudgeUserId = SettingsService.sharedInstance.nudgeUserId {
            mixpanel.identify(distinctId: nudgeUserId)
        }
    }
    
    // Super Properties
    
    func setSuperProperty(_ key: String, string: String) {
        self.mixpanel.registerSuperProperties([key : string])
    }
    
    func setSuperProperty(_ key: String, bool: Bool) {
        self.mixpanel.registerSuperProperties([key : bool])
    }
    
    func setSuperProperty(_ key: String, count: Int) {
        self.mixpanel.registerSuperProperties([key : count])
    }
    
    func incrementSuperProperty(_ key: String) {
        if let count = self.mixpanel.currentSuperProperties()[key] as? Int {
            self.setSuperProperty(key, count: count+1)
        } else {
            self.setSuperProperty(key, count: 1)
        }
        self.mixpanel.people.increment(property: key, by: 1)
    }
    
    func decrementSuperProperty(_ key: String) {
        if let count = self.mixpanel.currentSuperProperties()[key] as? Int {
            self.setSuperProperty(key, count: count-1)
        } else {
            self.setSuperProperty(key, count: -1)
        }
        self.mixpanel.people.increment(property: key, by: -1)
    }
    
    // Events
    
    func track(_ event: String, properties: [AnyHashable : Any]? = nil) {
        self.incrementSuperProperty(event)
        self.mixpanel.track(event: event, properties: properties as? Properties)
    }
}
