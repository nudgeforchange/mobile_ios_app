//
//  AppDelegate.swift
//  Nudge
//
//  Created by Thanos on 12/12/16.
//  Copyright © 2016 Level 3 Studios. All rights reserved.
//

import UIKit
import ChameleonFramework
//import GoogleMaps
import UserNotifications
import GoogleMobileAds
import SwiftDate
import Alamofire
import Fabric
import Crashlytics
import Mixpanel
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let currentSlugCount = 6
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Third-party services
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        //GMSServices.provideAPIKey("AIzaSyDlrffe4sSttqPwyW4YO6gjDtsvLBaze88")
        //GMSPlacesClient.provideAPIKey("AIzaSyCo83S-AVbKHzAgKdtMvSUWHm0odZW0vxg")
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2077196498025929~6156789297")
        BITHockeyManager.shared().configure(withIdentifier: "bf2fdc8a72b24484904becd4c4575bf5")
        // Do some additional configuration if needed here
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()
        
        if let isLocationLaunch = launchOptions?[UIApplicationLaunchOptionsKey.location] as? Bool {
            if isLocationLaunch {
                AnalyticsService.shared.track(AnalyticsEvent.appLaunchedLocation)
                //LocationService.sharedInstance.startBackgroundTask()
                BusinessLocationVisitService.sharedInstance.startMonitoring()
            }
        } else {
            AnalyticsService.shared.track(AnalyticsEvent.appLaunched)
        }
        
        // Set up notification service
        NotificationService.sharedInstance.setup()
        //LocationService.sharedInstance.setupLocationService()
        
        // Authenticate
        AuthenticationService.sharedInstance.authenticate(attempt: 0) {
            // if questions answered and location tracking allowed, set up and start location services. otherwise defer to TabBarController.
            if SettingsService.sharedInstance.nudgeToken != nil {
                if let beliefs = SettingsService.sharedInstance.beliefs {
                    BeliefsService.sharedInstance.postBeliefs(beliefs)
                    //LocationService.sharedInstance.setupBackgroundUpdates()
                    if BusinessLocationVisitService.sharedInstance.canMonitorLocation() {
                        BusinessLocationVisitService.sharedInstance.startMonitoring()
                    }
                }
            }
        }
        
        let navigationBarAppearance = UINavigationBar.appearance()
        //navigationBarAppearance.barStyle = .blackTranslucent
        navigationBarAppearance.setBackgroundImage(UIImage(), for: .default)
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.isTranslucent = true
        navigationBarAppearance.tintColor = UIColor.white
        navigationBarAppearance.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white,
                                                       NSFontAttributeName : UIFont(name: AppFontNames.lightFont, size: 22)!]
        
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.barTintColor = UIColor.white
        tabBarAppearance.tintColor = AppColors.nudgeRedColor
        
        //clear badge count when app is launched
        BadgeManager.sharedInstance.clearBadges()
        
        //update run count to show review
        ReviewKit.sharedInstance.updateRunCount()
        
        //setup SwiftyBeaverLogging
        LogKit.sharedInstance.setupLogging()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if SettingsService.sharedInstance.nudgeToken != nil {
            //LocationService.sharedInstance.startBackgroundTask()
            BusinessLocationVisitService.sharedInstance.startMonitoring()
        }
        
        AnalyticsService.shared.track(AnalyticsEvent.appForegrounded)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
