import Alamofire

class AuthenticationService: APIService {
    
    static let sharedInstance = AuthenticationService()
    
    private override init() {
        super.init()
    }
    
    private func getSecretKey() -> String {
        //read the key from the document
        let bundle = Bundle.main
        let path = bundle.path(forResource: "NudgeKey", ofType: "txt")
        var secret = ""
        do {
            let string = try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
            secret = string
        }
        catch let error as NSError {
            secret = error.localizedDescription
        }
        return secret
    }
    
    private func saveCookie(cookie : HTTPCookie) {
        HTTPCookieStorage.shared.setCookie(cookie)
    }
    
    func didTokenExpire() -> Bool {
        if let createdAt = SettingsService.sharedInstance.nudgeTokenCreatedAt {
            return Date().timeIntervalSince(createdAt) > (60*60) - 30
        } else {
            return true
        }
    }

    func authenticate(attempt: Int, completion: @escaping () -> Void) {
        //get the secret key
        let secretKey = self.getSecretKey().trimmingCharacters(in: .whitespacesAndNewlines)
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dateString = dateFormatter.string(from: date)
        let deviceID = SettingsService.sharedInstance.deviceVendorId
        let clientUUID = SettingsService.sharedInstance.nudgeUserId
        
        guard clientUUID != nil else {
            guard attempt < 5 else { exit(0) }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.authenticate(attempt: attempt+1, completion: completion)
            })
            return
        }
        
        //setup the headers
        var headers : HTTPHeaders = ["date" : dateString,
                                     "x-device-id" : deviceID,
                                     "x-nudge-user-id" : clientUUID!]
        let signableString = headers.map({ "\($0.key): \($0.value)" }).joined(separator: "\n")
        let signedString = signableString.hmac(algorithm: .SHA256, key: secretKey)
        headers["Authorization"] = "Signature keyId=\"Demo\",algorithm=\"hmac-sha256\",signature=\"\(signedString)\",headers=\"date x-device-id x-nudge-user-id\""
        let urlString = "\(APIService.baseURL)/users/login"
        let request = Alamofire.request(urlString, method : .post, headers : headers)
        request.responseJSON  { response in
            //debugPrint(request)
            //debugPrint(response)
            guard response.error == nil else {
                //print(response.error!)
                return
            }
            
            //get response headers
            if let responseHeaders = response.response?.allHeaderFields {
                let headers = responseHeaders as? [String : String]
                let responseURL = response.request?.url
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers!, for: responseURL!)
                if cookies.count > 0 {
                    self.saveCookie(cookie: cookies.first!)
                }
            }
            
            if let result = response.result.value {
                // get token
                let JSON = result as! NSDictionary
                
                if let forceUpgrade = JSON.object(forKey: "force_upgrade") as? Bool {
                    if forceUpgrade {
                        RequireUpdateService.requireUpdate()
                        return
                    }
                }
                
                if let tokenString = JSON.object(forKey: "token") as? String {
                    SettingsService.sharedInstance.nudgeToken = tokenString
                    SettingsService.sharedInstance.nudgeTokenCreatedAt = Date()
                    
                    // get distance settings
                    if let locationDict = JSON.object(forKey: "settings") as? NSDictionary {
                        let locationDistance = locationDict.object(forKey: "location_update_distance") as! Double
                        SettingsService.sharedInstance.distanceFilter = locationDistance
                    }
                } else {
                    return
                }
            }
            
            completion()
        }
    }
}

enum HMACAlgorithm {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
    
    func toCCHmacAlgorithm() -> CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .MD5:
            result = kCCHmacAlgMD5
        case .SHA1:
            result = kCCHmacAlgSHA1
        case .SHA224:
            result = kCCHmacAlgSHA224
        case .SHA256:
            result = kCCHmacAlgSHA256
        case .SHA384:
            result = kCCHmacAlgSHA384
        case .SHA512:
            result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }
    
    func digestLength() -> Int {
        var result: CInt = 0
        switch self {
        case .MD5:
            result = CC_MD5_DIGEST_LENGTH
        case .SHA1:
            result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:
            result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:
            result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:
            result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:
            result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}

extension String {
    func hmac(algorithm: HMACAlgorithm, key: String) -> String {
        let cKey = key.cString(using: String.Encoding.utf8)
        let cData = self.cString(using: String.Encoding.ascii)
        var result = [CUnsignedChar](repeating: 0, count: Int(algorithm.digestLength()))
        CCHmac(algorithm.toCCHmacAlgorithm(), cKey!, Int(strlen(cKey!)), cData!, Int(strlen(cData!)), &result)
        let hmacData = NSData(bytes: result, length: (Int(algorithm.digestLength())))
        let hmacBase64 = hmacData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength76Characters)
        return String(hmacBase64)
    }
}
