import Alamofire

class BeliefsService: APIService {
    
    static let sharedInstance = BeliefsService()
    
    private override init() {}
    
    func getIssues() {
        let urlString = "\(APIService.baseURL)/issues/"
        //"Cookie" : "\(cookieString)" ]
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method : .get,  headers : self.getSecureHeader()).responseJSON(completionHandler: {response in
                //print(response.request?.allHTTPHeaderFields)
                //print(response.response)
                //print(response.result)
                
                if response.result.value != nil {
                    //print(result)
                }
            })
        }
    }
    
    func postBeliefs(_ beliefs: [String : Int]) {
        var beliefsArray = [[String : Any]]()
        for slug: String in beliefs.keys {
            let beliefDict: [String : Any] = ["issue_slug" : slug, "importance" : beliefs[slug]!]
            beliefsArray.append(beliefDict)
        }
        let parameters: Parameters = ["beliefs" : beliefsArray]
        //print(parameters)
        
        let urlString = "\(APIService.baseURL)/beliefs"
        //post beliefs to server
        
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
                //debugPrint(response)
            })
        }
    }

}
