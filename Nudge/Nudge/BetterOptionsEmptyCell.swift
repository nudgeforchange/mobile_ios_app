//
//  BetterOptionsEmptyCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 4/6/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class BetterOptionsEmptyCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
