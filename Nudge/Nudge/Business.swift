import Foundation

class Business: NSObject, NSCoding {
    
    var businessUUID: String
    var name: String
    
    init(businessUUID: String, name: String) {
        self.businessUUID = businessUUID
        self.name = name
    }
    
    init(dictionary: [String : AnyObject]) {
        self.businessUUID = dictionary["business_uuid"] as! String
        self.name = dictionary["name"] as! String
    }
    
    // MARK: NSCoding
    required init?(coder decoder: NSCoder) {
        self.businessUUID = decoder.decodeObject(forKey: "business_uuid") as! String
        self.name = decoder.decodeObject(forKey: "name") as! String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.businessUUID, forKey: "business_uuid")
        coder.encode(self.name, forKey: "name")
    }
}
