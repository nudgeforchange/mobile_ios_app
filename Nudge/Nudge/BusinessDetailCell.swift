//
//  BusinessDetailCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/20/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class BusinessDetailCell: UITableViewCell {
    
    @IBOutlet weak var topRoundedShadowView: RoundedShadowView!
    @IBOutlet weak var middleRoundedShadowView: RoundedShadowView!
    @IBOutlet weak var bottomRoundedShadowView: RoundedShadowView!
    
    @IBOutlet weak var scoreImageView: UIImageView!
    @IBOutlet weak var issueImageView: UIImageView!
    
    @IBOutlet weak var issueLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var issueTextLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    
    fileprivate var visibleRoundedShadowView: RoundedShadowView!
    
    var issue: BusinessIssueScore! {
        didSet {
            self.scoreImageView.image = UIImage(named: "radialscore_\(issue.score)")
            self.issueImageView.image = issue.issueImage
            self.issueImageView.tintColor = AppColors.tintColorForScore(issue.score)
            self.issueLabel.text = issue.name
            self.scoreLabel.text = "\(issue.score)"
            self.scoreLabel.textColor = AppColors.tintColorForScore(issue.score)
            self.issueTextLabel.text = issue.explanation
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.backgroundColor = UIColor.clear
        self.visibleRoundedShadowView = self.middleRoundedShadowView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTop() {
        self.topRoundedShadowView.isHidden = false
        self.middleRoundedShadowView.isHidden = true
        self.bottomRoundedShadowView.isHidden = true
        self.visibleRoundedShadowView = self.topRoundedShadowView
    }
    
    func setMiddle() {
        self.topRoundedShadowView.isHidden = true
        self.middleRoundedShadowView.isHidden = false
        self.bottomRoundedShadowView.isHidden = true
        self.visibleRoundedShadowView = self.middleRoundedShadowView
    }
    
    func setBottom() {
        self.topRoundedShadowView.isHidden = true
        self.middleRoundedShadowView.isHidden = true
        self.bottomRoundedShadowView.isHidden = false
        self.visibleRoundedShadowView = self.bottomRoundedShadowView
    }
    
}
