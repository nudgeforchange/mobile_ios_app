//
//  BusinessIssueScore.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/21/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation

struct IssueKey {
    static let name = "name"
    static let slug = "slug"
    static let score = "score"
    static let explanation = "explanation"
    static let sources = "sources"
}

class BusinessIssueScore: NSObject, NSCoding {
    var name: String
    var slug: String
    var score: Int
    var explanation: String
    var sources: [String]
    
    var category : ScoreCategories {
        get {
            return self.slug.categoryFromSlug()
        }
    }
    
    var issueImage: UIImage {
        get {
            switch self.slug {
            case "environment":
                return UIImage(named: "value_environment")!
            case "lgbtq-equality":
                return UIImage(named: "value_lgbtq-equality")!
            case "workers-rights":
                return UIImage(named: "value_workers-rights")!
            case "womens-equality":
                return UIImage(named: "value_womens-equality")!
            case "racial-equality":
                return UIImage(named: "value_racial-equality")!
            case "trump":
                return UIImage(named: "value_trump")!
            default:
                return UIImage(named: "UnknownPlace")!
            }
        }
    }
    
    override var description: String {
        get {
            return "\(self.slug): \(self.score)"
        }
    }
    
    init(name: String, slug: String, score: Int, explanation: String, sources: [String]) {
        self.name = name
        self.slug = slug
        self.score = score
        self.explanation = explanation
        self.sources = sources
    }
    
    init(dictionary: NSDictionary) {
        self.name = dictionary[IssueKey.name] as! String
        self.slug = dictionary[IssueKey.slug] as! String
        self.score = dictionary[IssueKey.score] as! Int
        if let explanation = dictionary[IssueKey.explanation] {
            self.explanation = explanation as! String
        } else {
            self.explanation = "No explanation entered."
        }
        self.sources = (dictionary[IssueKey.sources] as! [String]).filter({ (source) -> Bool in
            return source.trimmingCharacters(in: .whitespaces) != "" && URL(string: source) != nil
        })
    }
    
    required init?(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: IssueKey.name) as! String
        self.slug = decoder.decodeObject(forKey: IssueKey.slug) as! String
        self.score = decoder.decodeInteger(forKey: IssueKey.score)
        self.explanation = decoder.decodeObject(forKey: IssueKey.explanation) as! String
        self.sources = decoder.decodeObject(forKey: IssueKey.sources) as! [String]
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: IssueKey.name)
        coder.encode(self.slug, forKey: IssueKey.slug)
        coder.encode(self.score, forKey: IssueKey.score)
        coder.encode(self.explanation, forKey: IssueKey.explanation)
        coder.encode(self.sources, forKey: IssueKey.sources)
    }
    
    static var supportsSecureCoding: Bool {
        return true
    }
}
