//
//  Place.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/11/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation

struct BusinessKey {
    static let type = "type"
    static let business = "business"
    static let businessLocationUUID = "business_location_uuid"
    static let placeId = "place_id"
    static let name = "name"
    static let canonicalName = "canonical_name"
    static let address = "address"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let geofenceRadius = "geofence_radius"
    static let score = "score"
    static let issues = "issues"
    static let dateAdded = "date_added"
    static let shouldMonitor = "should_monitor"
    static let distance = "distance"
}

class BusinessLocation: NSObject, NSCoding {
    var business: Business?
    var businessLocationUUID: String
    var placeId: String
    var name: String
    var canonicalName: String
    var address: String
    var latitude: Double
    var longitude: Double
    var distance: Double = 0.0
    var location: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        }
    }
    var geofenceRadius: CLLocationDistance
    var score: Double
    var issues: [BusinessIssueScore]
    var dateAdded: Date
    var shouldMonitor: Bool
    
    var region: CLCircularRegion {
        get {
            let region = CLCircularRegion(center: self.location, radius: self.geofenceRadius, identifier: self.placeId)
            region.notifyOnEntry = true
            region.notifyOnExit = true
            return region
        }
    }
    
    var lowestIssueScore: Int {
        get {
            var lowestScore = 10
            for issue in self.issues {
                if issue.score < lowestScore {
                    lowestScore = issue.score
                }
            }
            return lowestScore
        }
    }
    
    var worstIssue: BusinessIssueScore {
        get {
            var worstIssue = self.issues[0]
            for issue in self.issues {
                if issue.score < worstIssue.score {
                    worstIssue = issue
                }
            }
            return worstIssue
        }
    }
    
    var bestIssue: BusinessIssueScore {
        get {
            var bestIssue = self.issues[0]
            for issue in self.issues {
                if issue.score > worstIssue.score {
                    bestIssue = issue
                }
            }
            return bestIssue
        }
    }
    
    override var description: String {
        get {
            return "\n"
                + "business: \(String(describing: self.business))\n"
                + "businessLocationUUID: \(self.businessLocationUUID)\n"
                + "placeId: \(self.placeId)\n"
                + "name: \(self.name)\n"
                + "canonicalName: \(self.canonicalName)\n"
                + "address: \(self.address)\n"
                + "latitude: \(self.latitude)\n"
                + "longitude: \(self.longitude)\n"
                + "geofenceRadius: \(self.geofenceRadius)\n"
                + "score: \(self.score)\n"
                + "issues: \(self.issues)\n"
                + "dateAdded: \(self.dateAdded)\n"
                + "shouldMonitor: \(self.shouldMonitor)"
        }
    }
    
    init(business: Business?, businessLocationUUID: String, placeId: String, name: String, canonicalName: String, address: String, latitude: Double, longitude: Double, geofenceRadius: CLLocationDistance, score: Double, issues: [BusinessIssueScore], scoredAlternatives: [BusinessLocation], unscoredAlternatives: [BusinessLocation], dateAdded: Date, shouldMonitor: Bool) {
        self.business = business
        self.businessLocationUUID = businessLocationUUID
        self.placeId = placeId
        self.name = name
        self.canonicalName = canonicalName
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.geofenceRadius = geofenceRadius
        self.score = score
        self.issues = issues
        self.dateAdded = dateAdded
        self.shouldMonitor = shouldMonitor
    }
    
    init(dictionary: [String : AnyObject], dateAdded: Date, shouldMonitor: Bool) {
        if let businessDict = dictionary[BusinessKey.business] as? [String : AnyObject] {
            self.business = Business(dictionary: businessDict)
        }
        self.businessLocationUUID = dictionary[BusinessKey.businessLocationUUID] as! String
        self.placeId = dictionary[BusinessKey.placeId] as! String
        self.name = dictionary[BusinessKey.name] as! String
        self.canonicalName = dictionary[BusinessKey.canonicalName] as! String
        self.address = dictionary[BusinessKey.address] as! String
        let location = dictionary["location"] as! NSDictionary
        self.latitude = location["lat"] as! Double
        self.longitude = location["lng"] as! Double
        //self.geofenceRadius = dictionary[BusinessKey.geofenceRadius] as! Double
        self.geofenceRadius = 15
        self.score = (dictionary[BusinessKey.score] as! NSNumber).doubleValue
        let issues = dictionary[BusinessKey.issues] as! [NSDictionary]
        self.issues = issues.map({
            (issue: NSDictionary) -> BusinessIssueScore in
            return BusinessIssueScore(dictionary: issue)
        })
        self.dateAdded = dateAdded
        self.shouldMonitor = shouldMonitor
    }
    
    init(dictionary: [String : AnyObject]) {
        if let businessDict = dictionary[BusinessKey.business] as? [String : AnyObject] {
            self.business = Business(dictionary: businessDict)
        }
        self.businessLocationUUID = dictionary[BusinessKey.businessLocationUUID] as! String
        self.placeId = dictionary[BusinessKey.placeId] as! String
        self.name = dictionary[BusinessKey.name] as! String
        if let canonicalName = dictionary[BusinessKey.canonicalName] {
            self.canonicalName = canonicalName as! String
        }
        else {
            self.canonicalName = self.name
        }
        
        self.address = dictionary[BusinessKey.address] as! String
        let location = dictionary["location"] as! NSDictionary
        self.latitude = location["lat"] as! Double
        self.longitude = location["lng"] as! Double
        //self.geofenceRadius = dictionary[BusinessKey.geofenceRadius] as! Double
        self.geofenceRadius = 15
        if let score = dictionary[BusinessKey.score] {
            self.score = (score as! NSNumber).doubleValue
        }
        else {
            self.score = -1.0
        }
        if let issues = dictionary[BusinessKey.issues]  {
            self.issues = (issues as! [NSDictionary]).map({
                (issue: NSDictionary) -> BusinessIssueScore in
                return BusinessIssueScore(dictionary: issue)
            })
        }
        else {
            self.issues = []
        }
        self.distance = (dictionary[BusinessKey.distance] as! NSNumber).doubleValue
        self.dateAdded = Date()
        self.shouldMonitor = false
    }
    
    // MARK: NSCoding
    required init?(coder decoder: NSCoder) {
        self.business = decoder.decodeObject(forKey: BusinessKey.business) as? Business
        self.businessLocationUUID = decoder.decodeObject(forKey: BusinessKey.businessLocationUUID) as! String
        self.placeId = decoder.decodeObject(forKey: BusinessKey.placeId) as! String
        self.name = decoder.decodeObject(forKey: BusinessKey.name) as! String
        self.canonicalName = decoder.decodeObject(forKey: BusinessKey.canonicalName) as! String
        self.address = decoder.decodeObject(forKey: BusinessKey.address) as! String
        self.latitude = decoder.decodeDouble(forKey: BusinessKey.latitude)
        self.longitude = decoder.decodeDouble(forKey: BusinessKey.longitude)
        self.geofenceRadius = decoder.decodeDouble(forKey: BusinessKey.geofenceRadius)
        self.score = decoder.decodeDouble(forKey: BusinessKey.score)
        self.issues = decoder.decodeObject(forKey: BusinessKey.issues) as! [BusinessIssueScore]
        let dateMilli = decoder.decodeInteger(forKey: BusinessKey.dateAdded)
        self.dateAdded = Date(milliseconds: dateMilli)
        self.shouldMonitor = decoder.decodeBool(forKey: BusinessKey.shouldMonitor)
        self.distance = decoder.decodeDouble(forKey: BusinessKey.distance)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.business, forKey: BusinessKey.business)
        coder.encode(self.businessLocationUUID, forKey: BusinessKey.businessLocationUUID)
        coder.encode(self.placeId, forKey: BusinessKey.placeId)
        coder.encode(self.name, forKey: BusinessKey.name)
        coder.encode(self.canonicalName, forKey: BusinessKey.canonicalName)
        coder.encode(self.address, forKey: BusinessKey.address)
        coder.encode(self.latitude, forKey: BusinessKey.latitude)
        coder.encode(self.longitude, forKey: BusinessKey.longitude)
        coder.encode(self.geofenceRadius, forKey: BusinessKey.geofenceRadius)
        coder.encode(self.score, forKey: BusinessKey.score)
        coder.encode(self.issues, forKey: BusinessKey.issues)
        coder.encode(self.dateAdded.milliSeconds, forKey: BusinessKey.dateAdded)
        coder.encode(self.shouldMonitor, forKey: BusinessKey.shouldMonitor)
        coder.encode(self.distance, forKey: BusinessKey.distance)
    }
}
