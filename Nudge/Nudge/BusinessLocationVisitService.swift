/*
 
 
 */

import CoreLocation
import AddressBookUI
import SwiftDate


class BusinessLocationVisitService: NSObject {
    
    static let sharedInstance = BusinessLocationVisitService()
    
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var monitoredBusinessLocations: [BusinessLocation] {
        get {
            return StorageService.sharedInstance.monitoredBusinessLocations
        }
        set {
            StorageService.sharedInstance.monitoredBusinessLocations = newValue
        }
    }
    var lastLocationUpdateDate: Date = Date(timeIntervalSinceReferenceDate: 0)
    var isPerformingNudge = false
    
    override init() { 
        super.init()
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.distanceFilter = SettingsService.sharedInstance.distanceFilter
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.pausesLocationUpdatesAutomatically = true
        self.removeOldRegions()
    }
    
    func updateDistanceFilter(_ distanceFilter: Double) {
        self.locationManager.distanceFilter = distanceFilter
    }
    
    func requestAlwaysAuthorization() {
        self.locationManager.requestAlwaysAuthorization()
    }
    
    func requestLocation() {
        self.locationManager.requestLocation()
    }
    
    func canMonitorLocation() -> Bool {
        return CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways
    }
    
    func startMonitoring() {
        self.startMonitoringBusinessLocations()
        self.locationManager.startUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        //self.locationManager.startMonitoringVisits()
    }
    
    func stopMonitoring() {
        self.locationManager.monitoredRegions.forEach { (region) in
            self.locationManager.stopMonitoring(for: region)
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    func stopMonitoringRegionsNotIn(_ businessLocations: [BusinessLocation]) {
        self.locationManager.monitoredRegions.forEach { (region) in
            if !businessLocations.contains(where: { (businessLocation) -> Bool in
                region.identifier == businessLocation.placeId
            }) {
                self.locationManager.stopMonitoring(for: region)
            }
        }
    }
    
    func removeOldRegions() {
        self.locationManager.monitoredRegions.forEach({ region in
            let businessLocation = self.getMonitoredBusinessLocationForRegion(region)
            if businessLocation?.dateAdded.isToday == false {
                self.locationManager.stopMonitoring(for: region)
                if let index = self.monitoredBusinessLocations.index(of: businessLocation!) {
                    self.monitoredBusinessLocations.remove(at: index)
                }
                StorageService.sharedInstance.saveMonitoredBusinessLocations()
            }
        })
    }
    
    func startMonitoringBusinessLocations() {
        self.stopMonitoringRegionsNotIn(self.monitoredBusinessLocations)
        self.monitoredBusinessLocations.forEach { (monitoredBL) in
            if !self.locationManager.monitoredRegions.contains(where: { (region) -> Bool in
                monitoredBL.placeId == region.identifier
            }) {
                self.locationManager.startMonitoring(for: monitoredBL.region)
            }
        }
    }
    
    func addMonitoredBusinessLocations(_ businessLocations: [BusinessLocation]) {
        // remove old BLs matching new BLs
        self.monitoredBusinessLocations = self.monitoredBusinessLocations.filter { (oldBL) -> Bool in
            !businessLocations.contains(where: { (newBL) -> Bool in
                oldBL.placeId == newBL.placeId
            })
        }
        
        // append new BLs, restrict to newest 20, save
        self.monitoredBusinessLocations.append(contentsOf: businessLocations)
        self.monitoredBusinessLocations.removeFirst(max(0, self.monitoredBusinessLocations.count - 20))
        StorageService.sharedInstance.saveMonitoredBusinessLocations()
        
        self.startMonitoringBusinessLocations()
    }
    
    func getMonitoredBusinessLocationForRegion(_ region: CLRegion) -> BusinessLocation? {
        return self.monitoredBusinessLocations.first { (monitoredBL) -> Bool in
            monitoredBL.placeId == region.identifier
        }
    }
    
    func stopMonitoringBusinessLocation(_ businessLocation: BusinessLocation) {
        self.locationManager.stopMonitoring(for: businessLocation.region)
        if let index = self.monitoredBusinessLocations.index(of: businessLocation) {
            self.monitoredBusinessLocations.remove(at: index)
        }
        StorageService.sharedInstance.saveMonitoredBusinessLocations()
    }
    
    func alreadyVisitedToday(_ businessLocation: BusinessLocation) -> Bool {
        let nudges = StorageService.sharedInstance.nudges
        let alreadyVisitedToday = nudges.contains(where: { (nudge) -> Bool in
            let placeIdMatches = nudge.businessLocation.placeId == businessLocation.placeId
            let canonicalNameMatches = nudge.businessLocation.canonicalName == businessLocation.canonicalName
            //let locationMatches = nudge.businessLocation.latitude == businessLocation.latitude && nudge.businessLocation.longitude == businessLocation.longitude
            let nudgedToday = nudge.createdAt.isToday
            //return (placeIdMatches || canonicalNameMatches || locationMatches) && nudgedToday
            return (placeIdMatches || canonicalNameMatches) && nudgedToday
        })
        return alreadyVisitedToday
    }
    
    func didPerformRecentNudge() -> Bool {
        let nudges = StorageService.sharedInstance.nudges
        if nudges.count > 0 {
            let lastNudge = nudges[0]
            let today = Date()
            let difference = today - lastNudge.createdAt
            let minutes = difference.in(.minute)!
            LogKit.sharedInstance.sendInfoMessage("it's been \(minutes) minutes since last nudge")
            //let timeDiff = lastNudge.createdAt.timeIntervalSince(Date())
            //let minutes = timeDiff.truncatingRemainder(dividingBy: 60)
            return minutes < 5
        }
        return false
    }
    
    func performNudge(forRegion region : CLRegion!) {
        if let businessLocation = getMonitoredBusinessLocationForRegion(region) {
            guard !self.isPerformingNudge else {
                LogKit.sharedInstance.sendErrorMessage("performNudge failed for \(businessLocation.canonicalName) : isPerformingNudge failed")
                return }
            
            guard !self.didPerformRecentNudge() else {
                LogKit.sharedInstance.sendErrorMessage("performNudge failed for \(businessLocation.canonicalName) : didPerformRecent failed")
                return }
            
            guard !self.alreadyVisitedToday(businessLocation) else {
                LogKit.sharedInstance.sendErrorMessage("performNudge failed for \(businessLocation.canonicalName) : alreadyVisitedToday failed")
                return }
            
            self.isPerformingNudge = true
            LogKit.sharedInstance.sendDebugMessage("starting to fetch alternatives for \(businessLocation.canonicalName) @ \(Date().shortReadableDescription())")
            BusinessService.sharedInstance.fetchAlternativeBusinessLocations(businessLocation) { scoredAlternatives, unscoredAlternatives in
                
                let nudge = Nudge(latitude: businessLocation.latitude,
                                  longitude: businessLocation.longitude,
                                  businessLocation: businessLocation,
                                  status: .created,
                                  alternatives: scoredAlternatives + unscoredAlternatives,
                                  createdAt: Date())
                
                LogKit.sharedInstance.sendDebugMessage("creating the nudge for \(businessLocation.canonicalName)")
                NudgeService.sharedInstance.createNudge(nudge) { (success, nudge) in
                    guard success else {
                        LogKit.sharedInstance.sendErrorMessage("the create nudge for \(businessLocation.canonicalName) failed")
                        self.isPerformingNudge = false
                        return
                    }
                    
                    // save to list of past nudges
                    StorageService.sharedInstance.addNudge(nudge)
                    self.stopMonitoringBusinessLocation(businessLocation)
                    self.isPerformingNudge = false
                    
                    // list complete send to appropiate process
                    LogKit.sharedInstance.sendDebugMessage("the nudge for \(businessLocation.canonicalName) has been sent to the notification center @ \(Date().shortReadableDescription())")
                    NotificationService.sharedInstance.presentNudgeNotification(nudge) { (nudge) in
                        nudge.status = .sent
                        StorageService.sharedInstance.saveNudges()
                    }
                }
            }
        }
    }
    
}

extension BusinessLocationVisitService: CLLocationManagerDelegate {
    
    // MARK: Location Updates
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        
    }
    
    //when an new location is received send to server to get a list of nearby places
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("getting locations")
        guard let mostRecentLocation = locations.last else { return }
        //LogKit.sharedInstance.sendDebugMessage("didUpdateLocations received : \(mostRecentLocation.readableDescription())")
        AnalyticsService.shared.track(AnalyticsEvent.locationUpdated, properties: ["lat" : mostRecentLocation.coordinate.latitude,
                                                                                   "lng" : mostRecentLocation.coordinate.longitude,
                                                                                   "accuracy" : mostRecentLocation.horizontalAccuracy,
                                                                                   "speed" : mostRecentLocation.speed])
        self.currentLocation = mostRecentLocation
        //let coordinate = CLLocationCoordinate2DMake(33.280386, -111.790397)
        //mostRecentLocation = CLLocation(coordinate: coordinate, altitude: 10, horizontalAccuracy: 60, verticalAccuracy: 60, course: 0,  speed: 1.2, timestamp: Date())
        // ignore updates with high inaccuracy
        //guard 0...(self.locationManager.desiredAccuracy * 1.5) ~= mostRecentLocation.horizontalAccuracy else { return }
        //guard 0...(self.locationManager.desiredAccuracy * 1.2) ~= mostRecentLocation.horizontalAccuracy else { return }
        guard mostRecentLocation.horizontalAccuracy < 150 else { return }
        
        // ignore updates where speed > ~4mph or less than 0 (invalid)
        guard mostRecentLocation.speed < 15.0 else { return }
        
        // ignore updates older than 1 minute
        guard mostRecentLocation.timestamp.timeIntervalSinceNow.in(.minute)! < 1 else { return }
        
        // ignore updates less than 5 seconds after last acceptable update
        guard mostRecentLocation.timestamp.timeIntervalSince(self.lastLocationUpdateDate).in(.second)! > 5 else { return }
        
        self.lastLocationUpdateDate = mostRecentLocation.timestamp
        
        let lat = mostRecentLocation.coordinate.latitude
        let lng = mostRecentLocation.coordinate.longitude
        let latString = String(lat)
        let lngString = String(lng)
        //BusinessService.sharedInstance.fetchQuantizedLocation(mostRecentLocation) { (lat, lng) in
        BusinessService.sharedInstance.fetchNearbyBusinessLocations(latString, lngString) { businessLocations in
            self.addMonitoredBusinessLocations(businessLocations)
            
            //check to make sure the user isn't inside a region that didn't trigger
            for region in self.locationManager.monitoredRegions {
                self.locationManager.requestState(for: region)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print(error)
    }
    
    
    // MARK: Region Monitoring
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if let business = self.getMonitoredBusinessLocationForRegion(region) {
            LogKit.sharedInstance.sendDebugMessage("did enter a region : \(business.canonicalName) at \(Date().shortReadableDescription())")
            LogKit.sharedInstance.sendDebugMessage("entered region using \(self.currentLocation.readableDescription())")
            //check speed when region was entered
            guard self.currentLocation.speed <= 6.0 else {
                LogKit.sharedInstance.sendErrorMessage("enterRegion failed for \(business.canonicalName) : speed too high")
                return }
            //check accuracy when region was entered
            guard self.currentLocation.horizontalAccuracy <= 75.0 else {
                LogKit.sharedInstance.sendErrorMessage("enterRegion failed for \(business.canonicalName) : accuracy too high")
                return }
            // ignore updates older than 10 seconds
            guard self.currentLocation.timestamp.timeIntervalSinceNow.in(.second)! <= 10 else {
                LogKit.sharedInstance.sendErrorMessage("enterRegion failed for \(business.canonicalName) : timestamp too old")
                return }
            //check distance between location and business
            let distance = self.currentLocation.distance(from: CLLocation(latitude: business.latitude, longitude: business.longitude))
            guard distance <= 105.0 else {
                LogKit.sharedInstance.sendErrorMessage("enterRegion failed for \(business.canonicalName) : distance too far @ \(distance)")
                return }
            
            if region is CLCircularRegion {
                AnalyticsService.shared.track(AnalyticsEvent.geofenceTriggered)
                LogKit.sharedInstance.sendDebugMessage("performing nudge logic for \(business.canonicalName) at \(Date().shortReadableDescription())")
                self.performNudge(forRegion: region)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        //if user is inside of a region then force the manager to initiate a hit
        if state == .inside {
            self.locationManager(manager, didEnterRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        //print(error)
    }
    
    
    // MARK: Visit Monitoring
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        
    }
}

