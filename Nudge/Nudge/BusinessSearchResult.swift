//
//  BusinessSearchResult.swift
//  Nudge
//
//  Created by Magneto on 3/24/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class BusinessSearchResult: NSObject {
    
    var placeID : String
    var placeName : String
    var placeAddress : String
    
    init(placeID: String, placeName: String, placeAddress: String) {
        self.placeID = placeID
        self.placeName = placeName
        self.placeAddress = placeAddress
    }

}
