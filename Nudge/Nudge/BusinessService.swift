import Alamofire

class BusinessService: APIService {
    
    static let sharedInstance = BusinessService()
    
    func fetchQuantizedLocation(_ location: CLLocation, completion: @escaping (_ lat: String, _ lng: String) -> Void) {
        
        let urlString = "\(APIService.baseURL)/client/location/"
        let params = [ "location" : [ "lat" : location.coordinate.latitude, "lng" : location.coordinate.longitude ],
                       "accuracy" : location.horizontalAccuracy ] as [String : Any]
        //print(params)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { response in
                guard response.error == nil else { return }
                
                if let result = response.result.value {
                    let JSON = result as! [String : AnyObject]
                    if let quantizedLocationJSON = JSON["quantized_location"] {
                        let lat: String = quantizedLocationJSON["lat"] as! String
                        let lng: String = quantizedLocationJSON["lng"] as! String
                        completion(lat, lng)
                    }
                }
            }
        }
    }
    
    /*
     func fetchNearbyBadBusinessLocations(_ lat: String, _ lng: String, completion: @escaping (_ businessLocations: [BusinessLocation]) -> Void) {
     let urlString = "\(APIService.baseURL)/businesses/?location=\(lat),\(lng)&filter=bad&use_whitelist=true"
     
     self.authenticateIfNecessary {
     let request = Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: self.getSecureHeader())
     request.responseJSON(completionHandler: { response in
     //debugPrint(response)
     guard response.error == nil else { return }
     
     //if places found nearby add them to fences
     if let result = response.result.value {
     let JSON = result as! [String : AnyObject]
     if let businessesJSON = JSON["businesses"] {
     let businesses = businessesJSON as! [[String : AnyObject]]
     let businessLocations = businesses.map({ (business) -> BusinessLocation in
     BusinessLocation(dictionary: business, dateAdded: Date(), shouldMonitor: true)
     })
     completion(businessLocations)
     }
     }
     })
     }
     } */
    
    func fetchNearbyBusinessLocations(_ lat: String, _ lng: String, completion: @escaping (_ businessLocations: [BusinessLocation]) -> Void) {
        let urlString = "\(APIService.baseURL)/businesses/?location=\(lat),\(lng)&filter=bad&use_whitelist=true"
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            request.responseJSON(completionHandler: { response in
                //debugPrint(response)
                guard response.error == nil else { return }
                
                //if places found nearby add them to fences
                if let result = response.result.value {
                    let JSON = result as! [String : AnyObject]
                    if let businessesJSON = JSON["businesses"] {
                        if businessesJSON.count > 0 {
                            let businesses = businessesJSON as! [[String : AnyObject]]
                            let businessLocations = businesses.map({ (business) -> BusinessLocation in
                                BusinessLocation(dictionary: business, dateAdded: Date(), shouldMonitor: true)
                            })
                            completion(businessLocations)
                        }
                    }
                }
            })
        }
    }
    
    func fetchAlternativeBusinessLocations(_ businessLocation: BusinessLocation, completion: @escaping (_ scoredAlternatives: [BusinessLocation], _ unscoredAlternatives: [BusinessLocation]) -> ()) {
        //        let urlPlaceId = businessLocation.placeId.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let urlString = "\(APIService.baseURL)/businesses/\(businessLocation.placeId)/alternatives/"
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .get, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                //print(request.description)
                LogKit.sharedInstance.sendInfoMessage("request description = \(response.debugDescription)")
                guard response.error == nil else {
                    LogKit.sharedInstance.sendErrorMessage("there was an error in the response")
                    completion([],[])
                    return }
                
                var scoredAlternatives: [BusinessLocation] = []
                var unscoredAlternatives: [BusinessLocation] = []
                
                if let result = response.result.value {
                    let JSON = result as! [String : AnyObject]
                    if let businessesJSON = JSON["businesses"] {
                        let businesses = businessesJSON as! [[String : AnyObject]]
                        for var business in businesses {
                            if business[BusinessKey.issues] == nil && unscoredAlternatives.count < 3 {
                                // fill out missing data for unscored alternatives
                                business[BusinessKey.placeId] = "" as AnyObject?
                                business[BusinessKey.canonicalName] = business[BusinessKey.name]
                                business[BusinessKey.score] = -1 as AnyObject?
                                business[BusinessKey.issues] = [["name": "Unknown", "slug": "Unknown", "score": -1, "explanation": "", "sources": ["n/a"]]] as AnyObject?
                                
                                let unscoredAlternative = BusinessLocation(dictionary: business, dateAdded: Date(), shouldMonitor: false)
                                unscoredAlternatives.append(unscoredAlternative)
                            } else if business[BusinessKey.issues] != nil && scoredAlternatives.count < 3 {
                                let scoredAlternative = BusinessLocation(dictionary: business, dateAdded: Date(), shouldMonitor: false)
                                // sort issues
                                for issue in businessLocation.issues.reversed() {
                                    let index = scoredAlternative.issues.index(where: { (saIssue) -> Bool in
                                        saIssue.slug == issue.slug
                                    })
                                    if index != nil {
                                        let saIssue = scoredAlternative.issues.remove(at: index!)
                                        scoredAlternative.issues.insert(saIssue, at: 0)
                                    }
                                }
                                scoredAlternatives.append(scoredAlternative)
                            }
                            
                            // cap the list size
                            if scoredAlternatives.count == 3 && unscoredAlternatives.count == 3 {
                                break
                            }
                        }
                        
                        completion(scoredAlternatives, unscoredAlternatives)
                    }
                }
                else {
                    LogKit.sharedInstance.sendErrorMessage("there was a problem parsing the result")
                    completion([],[])
                }
            }
        }
    }
    
    func fetchUnratedBusiness() {
        let urlString = "\(APIService.baseURL)/businesses/unrated/"
        
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method: .get, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
                //print(response.request)
                //print(response.response)
                //print(response.result)
            })
        }
    }
    
    func submitResearchRequest(_ businessID : String, parameters : Parameters, parent : ResearchViewController) {
        let urlString = "\(APIService.baseURL)/businesses/\(businessID)/research-request/"
        //setup the parameters
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method: .post, parameters : parameters, encoding : JSONEncoding.default, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
                //print(response.request)  // original URL request
                //print(response.request?.allHTTPHeaderFields) //all headers passed in request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if response.result.value != nil {
                    //print("JSON: \(JSON)")
                }
                //close indicator and research window when post finishes.
                parent.finishSendingRequest()
            })
        }
    }
    
    func fetchNearbyBusinesses(searchData : SearchViewController.SearchData, completion: @escaping (_ businessLocations: [BusinessLocation]) -> Void) {
        var locationString = ""
        if searchData.isNonCoordLoc == false {
            //convert location coordinates to strings
            let lat = (searchData.currentLocation?.coordinate.latitude)! as Double
            let lng = (searchData.currentLocation?.coordinate.longitude)! as Double
            let latString = String(lat)
            let lngString = String(lng)
            locationString = "\(latString),\(lngString)"
        }
        else {
            locationString = searchData.locationName
        }
        
        let parameters : Parameters = ["query" : searchData.searchText,
                                       "location" : "\(locationString)"]
        let urlString = "\(APIService.baseURL)/businesses/search/"
        
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method: .get, parameters: parameters, headers: self.getSecureHeader()).responseJSON(completionHandler: { response in
                //debugPrint(response)
                guard response.error == nil else { return }
                
                if let result = response.result.value {
                    let JSON = result as! [String : AnyObject]
                    if let businessesJSON = JSON["businesses"] {
                        let businesses = businessesJSON as! [[String : AnyObject]]
                        let businessLocations = businesses.map({ (business) -> BusinessLocation in
                            BusinessLocation(dictionary: business)
                        })
                        completion(businessLocations)
                    }
                }
            })
        }
    }
    
    func locationSearchAutoComplete(searchData : SearchViewController.SearchData, completion: @escaping (_ businessLocations: [LocationSearchResult]) -> Void) {
        let urlString = "\(APIService.baseURL)/locations/autocomplete/"
        //convert location coordinates to strings
        let lat = (searchData.currentLocation?.coordinate.latitude)! as Double
        let lng = (searchData.currentLocation?.coordinate.longitude)! as Double
        let latString = String(lat)
        let lngString = String(lng)
        
        let parameters : Parameters = ["query" : searchData.locationName,
                                       "location" : "\(latString),\(lngString)"]
        
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method: .get, parameters: parameters, headers: self.getSecureHeader()).responseJSON(completionHandler: { response in
                
                //print(response.request)
                //print(response.response)
                
                if let result = response.result.value {
                    var locationList : [LocationSearchResult] = []
                    let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    if let container = JSON["results"] {
                        if let locations = container as? [AnyObject] {
                            for location in locations {
                                if let foundLocation = location as? [String : AnyObject] {
                                    let locationID = foundLocation["place_id"] as! String
                                    let locationName = foundLocation["primary"] as! String
                                    let searchResult = LocationSearchResult(placeID: locationID, name: locationName)
                                    locationList.append(searchResult)
                                }
                            }
                            
                        }
                        completion(locationList)
                    }
                }
            })
        }
    }
    
    func businessSearchAutoComplete(searchData : SearchViewController.SearchData, completion: @escaping (_ businessLocations: [BusinessSearchResult]) -> Void) {
        let urlString = "\(APIService.baseURL)/businesses/autocomplete/"
        var locationString = ""
        if searchData.isNonCoordLoc == false {
            //convert location coordinates to strings
            let lat = (searchData.currentLocation?.coordinate.latitude)! as Double
            let lng = (searchData.currentLocation?.coordinate.longitude)! as Double
            let latString = String(lat)
            let lngString = String(lng)
            locationString = "\(latString),\(lngString)"
        }
        else {
            locationString = searchData.locationName
        }
        
        let parameters : Parameters = ["query" : searchData.searchText,
                                       "location" : "\(locationString)"]
        
        self.authenticateIfNecessary {
            Alamofire.request(urlString, method: .get, parameters: parameters, headers: self.getSecureHeader()).responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    var locationList : [BusinessSearchResult] = []
                    let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    if let container = JSON["results"] {
                        let locations = container as! [[String : AnyObject]]
                        for location in locations {
                            let locationID = location["place_id"] as! String
                            let locationName = location["primary"] as! String
                            var locationPlace = "no address"
                            if let addressValue = location["secondary"] {
                                locationPlace = addressValue as! String
                            }
                            let searchResult = BusinessSearchResult(placeID: locationID, placeName: locationName, placeAddress: locationPlace)
                            locationList.append(searchResult)
                        }
                        completion(locationList)
                    }
                }
            })
        }
    }
}
