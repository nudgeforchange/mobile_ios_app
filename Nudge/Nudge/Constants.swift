//
//  Constants.swift
//  Nudge
//
//  Created by Thanos on 12/21/16.
//  Copyright © 2016 Level 3 Studios. All rights reserved.
//

import Foundation
import UserNotifications

struct AppStrings {
    static let alertCategory = "NudgeNotificationCategory"
    static let demoTimeStampKey =  "demoTimeStampKey"
    static let questionaireBool = "QuestionaireCompletedBool"
    static let firstRunBool = "FirstRunBool"
    static let locationBool = "LocationAccessBool"
    static let nudgeCookie = "NudgeCookie"
    static let beliefEnvironmentScore = "EnvironmentScore"
    static let beliefWomenEqualityScore = "WomenEqualityScore"
    static let beliefRaceEqualityScore = "RaceEqualityScore"
    static let beliefWageEqualityScore = "WageEqualityScore"
    static let beliefLGBTQEqualityScore = "LGBTQEqualityScore"
    static let savedBusinessLocationsKey = "SavedNotifications"
    static let nudgesKey = "Nudges"
    static let blockedGeoNotificationsKey = "BlockedNotifications"
    static let monthlyCompletedNotifications = "MonthlyCompletedNotifications"
    static let firstQuestionSkip = "FirstTimeSkip"
    static let firstQuestionYes = "FirstTimeYes"
    static let firstQuestionNo = "FirstTimeNo"
    static let badgeCount = "BadgeCount"
}

struct AppColors {
    static let darkTextColor = UIColor(hexString: "#4D4946")!
    static let lightTextColor = UIColor(hexString: "#7F7982")!
    static let nudgeWhiteColor = UIColor(hexString: "#F7F7F7")!
    static let nudgeYellowColor = UIColor(hexString: "#FDBE57")!
    static let nudgeOragneColor = UIColor(hexString: "#F6883A")!
    static let nudgeRedColor = UIColor(hexString: "#F1583C")!
    static let nudgePurpleColor = UIColor(hexString: "#D73076")!
    static let nudgeOrangeSecondaryColor = UIColor(hexString: "#F6883A")!
    static let nudgeOrangePrimaryColor = UIColor(hexString: "#F1583C")!
    static let nudgeBlueColor = UIColor(hexString: "#30C0E5")!
    static let nudgeGreenColor = UIColor(hexString: "#4CAF50")!
    static let nudgeGrayColor = UIColor(hexString: "#77777A")!
    static let nudgeMutedGrayColor = UIColor(hexString: "#BBBBBF")!
    static let betterOptionsColor = UIColor(hexString: "#3DA961")!
    static let environmentPrimaryColor = UIColor(hexString: "#889E24")!
    static let environmentDarkColor = UIColor(hexString: "#648721")!
    static let environmentLightColor = UIColor(hexString: "#A9B232")!
    static let workersPrimaryColor = UIColor(hexString: "#E0A714")!
    static let workersDarkColor = UIColor(hexString: "#D18711")!
    static let workersLightColor = UIColor(hexString: "#E5BF20")!
    static let womensPrimaryColor = UIColor(hexString: "#5587A2")!
    static let womensDarkColor = UIColor(hexString: "#465F89")!
    static let womensLightColor = UIColor(hexString: "#64A7B2")!
    static let racialPrimaryColor = UIColor(hexString: "#5587A2")!
    static let racialDarkColor = UIColor(hexString: "#753F82")!
    static let racialLightColor = UIColor(hexString: "#8164B0")!
    static let lgbtqPrimaryColor = UIColor(hexString: "#D73076")!
    static let lgbtqDarkColor = UIColor(hexString: "#C62C5C")!
    static let lgbtqLightColor = UIColor(hexString: "#E246A0")!
    
    static func tintColorForScore(_ score: Int) -> UIColor {
        switch score {
        case 0...4:
            return AppColors.nudgeRedColor
        case 5...7:
            return AppColors.nudgeYellowColor
        case 8...10:
            return AppColors.nudgeGreenColor
        default:
            return AppColors.nudgeGrayColor
        }
    }
}

struct AppFontNames {
    static let regularFont = "NunitoSans-Regular"
    static let boldFont = "NunitoSans-Bold"
    static let lightFont = "NunitoSans-Light"
}

struct AppSlugs {
    static let environment = "environment"
    static let lgbtqEquality = "lgbtq-equality"
    static let workersrights = "workers-rights"
    static let femaleEquality = "womens-equality"
    static let racialEquality = "racial-equality"
    static let trump = "trump"
}

public enum BeliefScores : Int {
    case NO = -1
    case SKIP = 0
    case YES = 1
}

public enum ActionButtonTypes : Int {
    case NOTIFICATION = 0
    case LOCATION = 1
}

public enum ScoreCategories : String {
    case ENVIRONMENT = "environment"
    case EQUALITYRACE = "racial-equality"
    case WORKERSRIGHTS = "workers-rights"
    case EQUALLGBTQ = "lgbtq-equality"
    case EQUALFEMALE = "womens-equality"
    case TRUMP = "trump"
    case UNKNOWN = "Unknown"
    
    static let allValues = ["trump","environment", "racial-equality", "workers-rights", "lgbtq-equality", "womens-equality", "Unknown"]
    
    func description() -> String {
        switch self {
        case .ENVIRONMENT :
            return AppSlugs.environment
        case .EQUALITYRACE :
            return AppSlugs.racialEquality
        case .WORKERSRIGHTS :
            return AppSlugs.workersrights
        case .EQUALLGBTQ :
            return AppSlugs.lgbtqEquality
        case .EQUALFEMALE :
            return AppSlugs.femaleEquality
        case .TRUMP :
            return AppSlugs.trump
        case .UNKNOWN :
            return "Unknown"
        }
    }
    
    func darkColor() -> UIColor {
        switch self {
        case .ENVIRONMENT:
            return AppColors.environmentDarkColor
        case .EQUALITYRACE:
            return AppColors.racialDarkColor
        case .WORKERSRIGHTS:
            return AppColors.workersDarkColor
        case .EQUALLGBTQ:
            return AppColors.lgbtqDarkColor
        case .EQUALFEMALE:
            return AppColors.womensDarkColor
        case .TRUMP :
            return AppColors.darkTextColor
        case .UNKNOWN:
            return AppColors.darkTextColor
        }
    }
    
    func lightColor() -> UIColor {
        switch self {
        case .ENVIRONMENT:
            return AppColors.environmentLightColor
        case .EQUALITYRACE:
            return AppColors.racialLightColor
        case .WORKERSRIGHTS:
            return AppColors.workersLightColor
        case .EQUALLGBTQ:
            return AppColors.lgbtqLightColor
        case .EQUALFEMALE:
            return AppColors.womensLightColor
        case .TRUMP :
            return AppColors.lightTextColor
        case .UNKNOWN:
            return AppColors.lightTextColor
        }
    }
    
    func primaryColor() -> UIColor {
        switch self {
        case .ENVIRONMENT:
            return AppColors.environmentPrimaryColor
        case .EQUALITYRACE:
            return AppColors.racialPrimaryColor
        case .WORKERSRIGHTS:
            return AppColors.workersPrimaryColor
        case .EQUALLGBTQ:
            return AppColors.lgbtqPrimaryColor
        case .EQUALFEMALE:
            return AppColors.womensPrimaryColor
        case .TRUMP :
            return AppColors.darkTextColor
        case .UNKNOWN:
            return AppColors.darkTextColor
        }
    }
    
}

public class BadgeManager {
    static let sharedInstance = BadgeManager()
    func clearBadges() {
        UserDefaults.standard.set(0, forKey: AppStrings.badgeCount)
        UserDefaults.standard.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func getBadgeCount() -> Int {
        return UserDefaults.standard.integer(forKey: AppStrings.badgeCount)
    }
    func updateBadgeCount() {
        let newCount = getBadgeCount() + 1
        UserDefaults.standard.set(newCount, forKey: AppStrings.badgeCount)
        UserDefaults.standard.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = newCount
    }
}

public extension CLLocation {
    func readableDescription() -> String {
        return "COORDINATE:(\(self.coordinate.latitude), \(self.coordinate.longitude)) , SPEED:\(self.speed), ACCURACY:\(self.horizontalAccuracy), TIME:\(self.timestamp.shortReadableDescription())"
    }
}
public class TapWithIndexPath : UITapGestureRecognizer {
    var indexPath : IndexPath!
    
    init(target: Any?, action: Selector?, indexPath : IndexPath) {
        super.init(target: target, action: action)
        self.indexPath = indexPath
    }
    
}

public class NotificationWithPayload : UNNotificationRequest {
    var nudgeArray : [BusinessLocation]!
    
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

public extension Int {
    public func getTitleForScore() -> String {
        switch self {
        case 10:
            return "Best possible score"
        case 9:
            return "Excellent score"
        case 8:
            return "Very good score"
        case 7:
            return "Good score"
        case 6:
            return "Slightly above average score"
        case 5:
            return "Average score"
        case 4:
            return "Below average score"
        case 3:
            return "Low score"
        case 2:
            return "Very low score"
        case 1:
            return "Terrible score"
        case 0:
            return "Worst possible score"
        case -1:
            return "This business does not yet have a Nudge score"
        default:
            return "Out of range"
        }
    }
}



extension String {
    func categoryFromSlug() -> ScoreCategories {
        switch self {
        case AppSlugs.environment:
            return .ENVIRONMENT
        case AppSlugs.workersrights:
            return .WORKERSRIGHTS
        case AppSlugs.racialEquality:
            return .EQUALITYRACE
        case AppSlugs.femaleEquality:
            return .EQUALFEMALE
        case AppSlugs.lgbtqEquality:
            return .EQUALLGBTQ
        case AppSlugs.trump:
            return .TRUMP
        default:
            return .UNKNOWN
        }
    }
    
    func cardImageFromSlug() -> String {
        switch self {
        case AppSlugs.environment:
            return "qcard_environment"
        case AppSlugs.racialEquality:
            return "qcard_racial"
        case AppSlugs.workersrights:
            return "qcard_workers"
        case AppSlugs.lgbtqEquality:
            return "qcard_lgbtq"
        case AppSlugs.femaleEquality:
            return "qcard_women"
        case AppSlugs.trump:
            return "qcard_trump"
        default:
            return "qcard_environment"
        }
    }
}


@IBDesignable class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}

extension Double {
    public func doubleToMileString() -> String {
        let distanceString = String(format: "%.2f", self)
        return " \(distanceString) miles"
    }
}

extension UIView {
    public func addDashedLineToView(_ color : UIColor, isWideScreen : Bool) {
        //set the background color to anything for debugging
        self.backgroundColor = UIColor.clear
        let cgColor = color.cgColor
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width / 2, y: frameSize.height)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineCap = kCALineCapRound
//        if isWideScreen == true {
//            shapeLayer.lineDashPhase = -5
//            shapeLayer.lineDashPattern = [10, 8]
//        }
//        else {
//            shapeLayer.lineDashPhase = -4
        shapeLayer.lineDashPattern = [NSNumber(value: Float((frameSize.width * 0.55)/14.0)), NSNumber(value: Float((frameSize.width * 0.45)/13.0))]
//        }
        
        
        let path: CGMutablePath = CGMutablePath()
        path.move(to: .zero)
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        shapeLayer.path = path
        
        self.layer.addSublayer(shapeLayer)
        self.layer.masksToBounds = true
    }
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
        } else if let sd = vc as? TKSideDrawerController {
            return UIWindow.getVisibleViewControllerFrom(vc: sd.contentController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: pvc)
            } else {
                return vc
            }
        }
    }
}

extension Date {
    var milliSeconds : Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds : Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    func shortReadableDescription() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        return dateFormatter.string(from: self)
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage : (image?.cgImage)!)
    }
    
    func alpha(_ value:CGFloat)->UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

extension UINavigationBar {
    func updateNavigationBar() {
        self.barStyle = .blackTranslucent
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.tintColor = UIColor.white
        self.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white,
                                    NSFontAttributeName : UIFont(name: AppFontNames.lightFont, size: 22)!]
    }
}
extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .default
    }
}

extension UIViewController {
    
    func setBackgroundColors() {
        self.view.backgroundColor = AppColors.nudgeWhiteColor
        let colorView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height/3))
        let colors : [UIColor] = [AppColors.nudgeYellowColor,  AppColors.nudgePurpleColor]
        let gradientColor = UIColor(gradientStyle: .topToBottom, withFrame: colorView.frame, andColors: colors)
        colorView.backgroundColor = gradientColor
        self.view.insertSubview(colorView, at: 0)
    }
    
    func setBackgroundGradient() {
        let colors : [UIColor] = [AppColors.nudgeYellowColor, AppColors.nudgePurpleColor]
        let gradientColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: colors)
        self.view.backgroundColor = gradientColor
    }
    
}

extension NSNull {
    func length() -> Int { return 0 }
    
    func integerValue() -> Int { return 0 }
    
    func floatValue() -> Float { return 0 }
    
    open override var description: String { return "0(NSNull)" }
    
    func componentsSeparatedByString(separator: String) -> [AnyObject] { return [AnyObject]() }
    
    func objectForKey(key: AnyObject) -> AnyObject? { return nil }
    
    func boolValue() -> Bool { return false }
    
    func count() -> Int {return 0 }
}

