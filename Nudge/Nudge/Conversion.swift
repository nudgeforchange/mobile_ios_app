//
//  Conversion.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/29/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation

extension Double {
    func metersToMiles() -> Double {
        return self/1609.344
    }
}
