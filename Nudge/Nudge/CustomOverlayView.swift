//
//  CustomOverlayView.swift
//  Nudge
//
//  Created by Thanos on 12/23/16.
//  Copyright © 2016 Level 3 Studios. All rights reserved.
//

import UIKit
import Koloda

private let overlayRightImageName = "overlay_like"
private let overlayLeftImageName = "overlay_skip"
private let overlayDownImageName = "overlay_meh"

class CustomOverlayView: OverlayView {
    
    @IBOutlet lazy var overlayImageView: UIImageView! = {
        [unowned self] in
        
        var imageView = UIImageView(frame: self.bounds)
        self.addSubview(imageView)
        
        return imageView
        }()
    
    override var overlayState: SwipeResultDirection?  {
        didSet {
            switch overlayState {
            case .left? :
                overlayImageView.image = UIImage(named: overlayLeftImageName)
            case .right? :
                overlayImageView.image = UIImage(named: overlayRightImageName)
            case .down? :
                overlayImageView.image = UIImage(named: overlayDownImageName)
            default:
                overlayImageView.image = nil
            }
            
        }
    }
}
