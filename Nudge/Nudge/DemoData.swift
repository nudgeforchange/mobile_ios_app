//
//  DemoData.swift
//  Nudge
//
//  Created by Thanos on 5/1/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import UserNotifications


func setupBadBusiness() -> BusinessLocation {
    let envIssue = BusinessIssueScore(name: "Environment", slug: "environment", score: 2, explanation: "Fined 4 times by the EPA since 2012. Scored 1/9 for deforestation-free palm oil by panda.org, and no plan in place to reduce unsustainable palm oil use. Pledged to use cage-free eggs by 2025, but no reports released on progress.", sources: [])
    let workIssue = BusinessIssueScore(name: "Workers' Rights", slug: "workers-rights", score: 1, explanation: "Average employees (tipped) start at <$5/hr and (untipped) employees start at <$8.50/hr. No benefits for part-time employees. Full-time employees offered health insurance benefits, no mental, dental or vision insurance. Previoulsy rated one of the Worst Places to Work by WornOutWorkers.com.", sources: [])
    let raceIssue = BusinessIssueScore(name: "Racial Equality", slug: "racial-equality", score: 1, explanation: "Multiple lawsuits settled since 2010 including racial and religious discrimination, hostile workplace, blocked promotions, and wage discrepancy. No people of color on the board of directors. 24% of employees are POC.", sources: [])
    
    let badPlace = BusinessLocation(business: nil, businessLocationUUID: "", placeId: "", name: "Cyclops Grill", canonicalName: "Cyclops Grill", address: "", latitude: 0.0, longitude: 0.0, geofenceRadius: 20, score: 4.0/3.0, issues: [envIssue, workIssue, raceIssue], scoredAlternatives: [], unscoredAlternatives: [], dateAdded: Date(), shouldMonitor: false)
    return badPlace
}

func setupFirstScoredBusiness() -> BusinessLocation {
    let g1EnvIssue = BusinessIssueScore(name: "Environment", slug: "environment", score: 10, explanation: "In 2010 commited to only use cage-free eggs, free-range beef, and antibiotic-free pork by 2020; releases regular reports and currently ahead of schedule. >70% of all locations use low-energy LED lighting and HVAC systems. Decreased total energy use by 30% since 2005. Publicly reports sustainabiliy results, sets clear and specific goals, and funds local habitat restoration initiatives.", sources: [])
    let g1WorkIssue = BusinessIssueScore(name: "Workers' Rights", slug: "workers-rights", score: 9, explanation: "Average wait staff paid <$9/hr (tipped) and host staff paid <$11/hr (untipped). Provides comprehensive health insurance (medical, vision, dental, mental) benefits to employees at 20+ hrs/week. Recognized as one of the best places to work by EmployerExplorer.com from 2012-2016.", sources: [])
    let g1WomenIssue = BusinessIssueScore(name: "Women's Equality", slug: "womens-equality", score: 7, explanation: "4/11 members of the board of directors are women, and 52% of all employees are women. Provides paid maternity, paternity and adoptive parent leave for all employees (20+ hr/week) after six months. Recognized as one of the Best Places to Work for WorkingMother.com in 2016 and 2013. Settled sexual harrasment lawsuit in 2014 for $2m, and sexual discrimination lawsuit in 2001 for $500k.", sources: [])
    let green1BL = BusinessLocation(business: nil, businessLocationUUID: "", placeId: "", name: "Blue Dot Tappas", canonicalName: "Blue Dot Tappas", address: "", latitude: 0.0, longitude: 0.0, geofenceRadius: 20, score: 26.0/3.0, issues: [g1EnvIssue, g1WorkIssue, g1WomenIssue], scoredAlternatives: [], unscoredAlternatives: [], dateAdded: Date(), shouldMonitor: false)
    return green1BL
}

func setupSecondScoredBusiness() -> BusinessLocation {
    let g2EnvIssue = BusinessIssueScore(name: "Environment", slug: "environment", score: 8, explanation: "New and remodeled restaurants have energy-efficient LED lighting and HVAC systems. Awarded 5/9 for palm oil sustainability and on track to use 100% sustainable palm oil by 2020 according to WWF.Panda.org. Previoulsy partnered with EPA Energy Star and EPA SmartWays.", sources: [])
    let g2WorkIssue = BusinessIssueScore(name: "Workers' Rights", slug: "workers-rights", score: 10, explanation: "Employees (tipped) start at >$10.50/hr. Untipped employees start at >$12.50/hr. Part-time employees (20+ hours) offered vision, dental and life insurance, 401K, and paid sick days and vacation. Full-time employees receive those benefits plus medical health insurnace. Previously recognized as one of the best places to work by Forbes. No history of OSHA violations for 10+ years.", sources: [])
    let g2RaceIssue = BusinessIssueScore(name: "Racial Equality", slug: "racial-equality", score: 9, explanation: "Organizes unconscious bias learning sessions for employees. Publicily commited to supplier diversity and consistently supports minorty suppliers. 3/11 members of the board of directors are people of color. 55% of all employees identify as members of a minority group.", sources: [])
    let green2BL = BusinessLocation(business: nil, businessLocationUUID: "", placeId: "", name: "Adventurer Bar & Grill", canonicalName: "Adventurer Bar & Grill", address: "", latitude: 0.0, longitude: 0.0, geofenceRadius: 20, score: 27.0/3.0, issues: [g2EnvIssue, g2WorkIssue, g2RaceIssue], scoredAlternatives: [], unscoredAlternatives: [], dateAdded: Date(), shouldMonitor: false)
    return green2BL
}

func setupUnscoredBusiness() -> BusinessLocation {
    return BusinessLocation(business: nil, businessLocationUUID: "", placeId: "", name: "Interstellar Steaks", canonicalName: "Interstellar Steaks", address: "", latitude: 0.0, longitude: 0.0, geofenceRadius: 20, score: -1, issues: [], scoredAlternatives: [], unscoredAlternatives: [], dateAdded: Date(), shouldMonitor: false)
}

func createDemoNudge() -> Nudge {
    let nudge = Nudge(latitude: 0.0, longitude: 0.0, businessLocation: setupBadBusiness(), status: .created, alternatives: [setupFirstScoredBusiness(), setupSecondScoredBusiness(), setupUnscoredBusiness()], createdAt: Date())
    nudge.isDemo = true
    return nudge
}

func presentDemoNudge(_ nudge: Nudge) {
    let title = nudge.businessLocation.canonicalName + "?"
    let message = "They have a low \(nudge.businessLocation.issues[0].name) score. Want more info and better options nearby?"
    let identifier = Bundle.main.bundleIdentifier! + "." + nudge.businessLocation.placeId + "." + nudge.createdAt.description
    let userInfo = ["nudge" : NSKeyedArchiver.archivedData(withRootObject: nudge)]
    
    if UIApplication.shared.applicationState == .active {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "More info", style: .default, handler: { (okAction) in
            
            showDemoNudgeScreen(nudge)
            
        })
        let cancelAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let tabBarVC: TabBarController = appDelegate.window?.rootViewController as! TabBarController
        tabBarVC.present(alert, animated: true, completion: nil)
    } else {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.userInfo = userInfo
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = AppStrings.alertCategory
        let localRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: nil)
        let notificaitonCenter = UNUserNotificationCenter.current()
        notificaitonCenter.add(localRequest, withCompletionHandler: nil)
    }
}

func showDemoNudgeScreen(_ nudge : Nudge) {
    
    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
    let tabBarVC = appDelegate.window!.rootViewController as! TabBarController
    
    //now from the root view show the nudge screen
    let summaryStoryboard = UIStoryboard(name: "NudgeSummaryViewController", bundle: nil)
    let nudgeVC = summaryStoryboard.instantiateViewController(withIdentifier: "NudgeSummaryVC") as! NudgeSummaryViewController
    nudgeVC.modalPresentationCapturesStatusBarAppearance = true
    nudgeVC.nudge = nudge
    
    tabBarVC.selectedIndex = 0
    let homeNC = tabBarVC.viewControllers?[0] as! UINavigationController
    homeNC.popToRootViewController(animated: false)
    homeNC.pushViewController(nudgeVC, animated: false)
}


