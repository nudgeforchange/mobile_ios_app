//
//  APIHelper.swift
//  Nudge
//
//  Created by Ultron on 2/3/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation
import Alamofire


class APIHelper {
    static let sharedInstance = APIHelper()
    static let baseURL = "https://d.nudgeforchange.com/api/v1"
    private init() {}
    
    private func saveCookie(cookie : HTTPCookie) {
        HTTPCookieStorage.shared.setCookie(cookie)
    }
    
    private func saveToken(tokenString : String) {
        let defaults = UserDefaults.standard
        defaults.set(tokenString, forKey: AppStrings.nudgeToken)
        defaults.synchronize()
        //after token is saved upload beliefs to sync with token
        self.uploadBeliefs()
    }
    
    private func saveDistanceFilter(_ distanceFilter : Double) {
        let defaults = UserDefaults.standard
        defaults.set(distanceFilter, forKey: AppStrings.locationTraveled)
        defaults.synchronize()
    }
    
    private func getSecureHeader() -> HTTPHeaders {
        var tokenString = ""
        if let token = UserDefaults.standard.string(forKey: AppStrings.nudgeToken) {
            tokenString = token
        }
        else {
            self.getToken()
        }
        var sessionID = ""
        var expirationDate = Date()
        if let storedCookie = HTTPCookieStorage.shared.cookies?.first {
            sessionID = storedCookie.value
            expirationDate = storedCookie.expiresDate!
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd-MMM-YYYY hh:mm:ss"
        let formattedDate = dateFormatter.string(for: expirationDate)!
        let dateString = "\(formattedDate) GMT"
        let cookie = "sessionid=\(sessionID); expires=\(dateString); HttpOnly; Max-Age=1209600; Path=/"
        let headers : HTTPHeaders = [
            "authorization" : "JWT \(tokenString)",
            "cookie" : cookie]
        return headers
    }
    
    func getToken() {
        //get the secret key
        let secretKey = getSecretKey().trimmingCharacters(in: .whitespacesAndNewlines)
        //print(secretKey)
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dateString = dateFormatter.string(from: date)
        let deviceID =  UIDevice.current.identifierForVendor?.uuidString
        let clientUUID = UUID().uuidString
        //setup the headers
        var headers : HTTPHeaders = ["date" : dateString,
                                     "x-device-id" : deviceID!,
                                     "x-nudge-user-id" : clientUUID]
        let signableString = headers.map({ "\($0.key): \($0.value)" }).joined(separator: "\n")
        let signedString = signableString.hmac(algorithm: .SHA256, key: secretKey)
        headers["Authorization"] = "Signature keyId=\"Demo\",algorithm=\"hmac-sha256\",signature=\"\(signedString)\",headers=\"date x-device-id x-nudge-user-id\""
        let urlString = "\(APIHelper.baseURL)/users/login"
        Alamofire.request(urlString, method : .post, headers : headers).responseJSON(completionHandler: {response in
            //get response headers
            if let responseHeaders = response.response?.allHeaderFields {
                let headers = responseHeaders as? [String : String]
                let responseURL = response.request?.url
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers!, for: responseURL!)
                if cookies.count > 0 {
                    self.saveCookie(cookie: cookies.first!)
                }
            }
            //get token and distance settings
            if let result = response.result.value {
                let JSON = result as! NSDictionary
                let tokenString = JSON.object(forKey: "token") as! String
                self.saveToken(tokenString: tokenString)
                let locationDict = JSON.object(forKey: "settings") as! NSDictionary
                let locationDistance = locationDict.object(forKey: "location_update_distance") as! Double
                self.saveDistanceFilter(locationDistance)
            }
        })
    }
    
    func uploadBeliefs() {
        //get the scores from preferences
        let preferences = UserDefaults.standard
        let environmentScore = preferences.integer(forKey: AppSlugs.environment)
        let workerScore = preferences.integer(forKey: AppSlugs.workersrights)
        let lgbtqScore = preferences.integer(forKey: AppSlugs.lgbtqEquality)
        let womensScore = preferences.integer(forKey: AppSlugs.femaleEquality)
        let raceScore = preferences.integer(forKey: AppSlugs.racialEquality)
        //{'issue_slug': 'workers-rights', 'importance': 1
        let urlString = "\(APIHelper.baseURL)/beliefs"
        //create the score dictionaries
        let environmentDict : [String : Any] = ["issue_slug" : AppSlugs.environment,
                                                "importance" : environmentScore]
        let workerDict : [String : Any] = ["issue_slug" : AppSlugs.workersrights,
                                           "importance" : workerScore]
        let lgbtqDict : [String : Any] = ["issue_slug" : AppSlugs.lgbtqEquality,
                                         "importance" : lgbtqScore]
        let womenDict : [String : Any] = ["issue_slug" : AppSlugs.femaleEquality,
                                          "importance" : womensScore]
        let raceDict : [String : Any] = ["issue_slug" : AppSlugs.racialEquality,
                                         "importance" : raceScore]
        //setup parameters
        let parameters : Parameters = [
            "beliefs" : [environmentDict, workerDict, lgbtqDict, womenDict, raceDict]
        ]
        //post beliefs to server
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
        })
    }
    
    func getIssues() {
        let urlString = "\(APIHelper.baseURL)/issues/"
        //"Cookie" : "\(cookieString)" ]
        Alamofire.request(urlString, method : .get,  headers : self.getSecureHeader()).responseJSON(completionHandler: {response in
            //print(response.request?.allHTTPHeaderFields)
            //print(response.response)
            //print(response.result)
            
            if let result = response.result.value {
                //print(result)
            }
        })
    }
    
    func uploadUserLocation(_ location : CLLocation) {
        let urlString = "\(APIHelper.baseURL)/location"
        let latString = location.coordinate.latitude.description
        let lngString = location.coordinate.longitude.description
        //let latString = "37.688185"
        //let lngString = "-122.131651"
        let parameters : Parameters = [
            "lat" : latString,
            "lng" : lngString]
        //print("sending current location to server")
        Alamofire.request(urlString, method : .post, parameters : parameters, encoding : JSONEncoding.default, headers : self.getSecureHeader()).responseJSON(completionHandler: {response in
            //print(response.response)
            //if places found nearby add them to fences
            if let result = response.result.value {
                //print("found places to avoid")
                let JSON = result as! [String : AnyObject]
                //print(JSON)
                if let container = JSON["businesses"] {
                    //print(container.count)
                    let businessContainer = container  as! [[String : AnyObject]]
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    for business in businessContainer {
                        //print("add geofence for business : \(business)")
                        //get values from response
                        let placeID = business["place_id"] as! String
                        let placeName = business["name"] as! String
                        let canonicalName = business["canonical_name"] as! String
                        let score = business["score"] as! Int
                        let location = business["location"] as! NSDictionary
                        let locationLat = location.value(forKey: "lat") as! Double
                        let locationLng = location.value(forKey: "lng") as! Double
                        let issues = business["issues"] as! NSArray
                        let radius = business["geofence_radius"] as! Double
                        //create notification item
                        let notificationItem = GeoNotificationPlace(identifier: placeID, displayName: placeName, canonicalName: canonicalName, latitude: locationLat, longitude: locationLng, radius: radius, score: score, issues: issues, eventType: .onEntry, dateMilli: Date().milliSeconds, isMonitored: true)
                        //save to preferences
                        delegate.addBusinessLocationToPreferences(notificationItem)
                    }
                }
            }
            
        })
    }
    
    func getAlternativesToPlace(_ place : GeoNotificationPlace, inBackground : Bool) {
        let urlString = "\(APIHelper.baseURL)/businesses/\(place.identifier)/alternatives"
        Alamofire.request(urlString, method: .get, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
            //print(response.request)
            //print(response.response)
            //print(response.result)
            var placeList : [AlternatePlace] = []
            //first add the nudged place to the list
            let placeIssues : [[String : AnyObject]] = place.issues as! [[String : AnyObject]]
            var placeScoreArray : [PlaceScore] = []
            let placeCoordinate = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            for issues in placeIssues {
                //print("issues with current place")
                let issueName = issues["slug"] as! String
                let issueScore = issues["score"] as! Int
                var issueMessage = "No explanation entered"
                if let explanation = issues["explanation"] {
                    issueMessage = explanation as! String
                }
                //print(issueName)
                //print(issueScore)
                let category = issueName.categoryFromSlug()
                let placeScore = PlaceScore(category: category, score: issueScore, message: issueMessage, isBetterOption: false)
                placeScoreArray.append(placeScore)
            }
            let nudgedPlace = AlternatePlace(scoreArray: placeScoreArray, placeName: place.placeName, placeDistance: 0, isGoodAlternate: false, placeIdentifier: place.identifier, placeCanonicalName: place.canonicalName, placeScore: place.score, latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            placeList.append(nudgedPlace)
            //get list of alternatives
            if let result = response.result.value {
                let JSON = result as! [String : AnyObject]
                if let container = JSON["businesses"] {
                    //if alternative places were found build the alert payload
                    let businessContainer = container as! [[String : AnyObject]]
                    for business in businessContainer {
                        //print("found alternate business")
                        //get values from response
                        let placeName = business["name"] as! String
                        let location = business["location"] as! [String : AnyObject]
                        let locationLat = location["lat"] as! Double
                        let locationLng = location["lng"] as! Double
                        let coordinate = CLLocation(latitude: locationLat, longitude: locationLng)
                        //scored alternates will have additional fields
                        var placeID : String = ""
                        if let businessID = business["place_id"] {
                            placeID = businessID as! String
                        }
                        var placeCanonicalName : String = placeName
                        if let businessCanonicalName = business["canonical_name"] {
                            placeCanonicalName = businessCanonicalName as! String
                        }
                        var placeScore : Int = 0
                        if let businessPlaceScore = business["score"] {
                            placeScore = businessPlaceScore as! Int
                        }
                        var scoreArray : [PlaceScore] = []
                        if let businessScores = business["issues"] {
                            let issueDict = businessScores as! [[String : AnyObject]]
                            for issues in issueDict {
                                let issueName = issues["slug"] as! String
                                let issueScore = issues["score"] as! Int
                                let issueMessage = issues["explanation"] as! String
                                let category = issueName.categoryFromSlug()
                                let placeScore = PlaceScore(category: category, score: issueScore, message: issueMessage, isBetterOption: true)
                                scoreArray.append(placeScore)
                            }
                        }
                        else {
                            scoreArray.append(PlaceScore(category: .UNKNOWN, score: -1, message: "", isBetterOption: true))
                        }
                        //create the alternate place
                        let distance = placeCoordinate.distance(from: coordinate) * 0.000621371192 //in miles
                        let alternatePlace = AlternatePlace(scoreArray: scoreArray, placeName: placeName, placeDistance: distance, isGoodAlternate: true, placeIdentifier: placeID, placeCanonicalName: placeCanonicalName, placeScore: placeScore, latitude: locationLat, longitude: locationLng)
                        placeList.append(alternatePlace)
                        //cap the list size
                        if placeList.count == 3 {
                            break
                        }
                    }
                    //list complete send to appropiate process
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    if inBackground == true {
                        delegate.generateAlertWithData(placeList)
                    }
                    else {
                        delegate.window?.rootViewController?.showAlternatesWindow(placeList)
                    }
                }
            }
        })
    }
    
    func genUnratedBusiness() {
        let urlString = "\(APIHelper.baseURL)/businesses/unrated"
        Alamofire.request(urlString, method: .get, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
            //print(response.request)
            //print(response.response)
            //print(response.result)
        })
    }
    
    func submitResearchRequest(_ businessID : String, parameters : Parameters, parent : ResearchViewController) {
        let urlString = "\(APIHelper.baseURL)/businesses/\(businessID)/research-request"
        //setup the parameters
        Alamofire.request(urlString, method: .post, parameters : parameters, encoding : JSONEncoding.default, headers: self.getSecureHeader()).responseJSON(completionHandler: {response in
            //print(response.request)  // original URL request
            //print(response.request?.allHTTPHeaderFields) //all headers passed in request
            //print(response.response) // URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            
            if let JSON = response.result.value {
                //print("JSON: \(JSON)")
            }
            //close indicator and research window when post finishes.
            parent.finishSendingRequest()
        })
    }
}
