//
//  AlternatePlace.swift
//  Nudge
//
//  Created by Ultron on 2/7/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

struct AltPlaceKey {
    static let placeLat = "latitude"
    static let placeLng = "longitude"
    static let distance = "distance"
    
    static let identifier = "identifer"
    static let name = "name"
    static let canonicalName = "canonical_name"
    static let scores = "scores"
    static let overal_score = "overal_score"
    
    static let isGood = "isgood"
}

class AlternatePlace: NSObject, NSSecureCoding {
    
    var placeCoordinate : CLLocationCoordinate2D
    var placeDistance : Double
    
    var placeIdentifier : String
    var placeName : String
    var placeCanonicalName : String
    var scoreArray : [PlaceScore] = []
    var placeScore : Int
    
    var isGoodAlternate : Bool
    
    
    init(scoreArray : [PlaceScore], placeName : String, placeDistance : Double, isGoodAlternate : Bool,
         placeIdentifier : String, placeCanonicalName : String, placeScore : Int, latitude : Double, longitude : Double) {
        self.scoreArray = scoreArray
        self.placeName = placeName
        self.placeDistance = placeDistance
        self.isGoodAlternate = isGoodAlternate
        self.placeIdentifier = placeIdentifier
        self.placeCanonicalName = placeCanonicalName
        self.placeScore = placeScore
        self.placeCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    required init?(coder aDecoder: NSCoder) {
        let scoreData = aDecoder.decodeObject(forKey: AltPlaceKey.scores) as! Data
        scoreArray = NSKeyedUnarchiver.unarchiveObject(with: scoreData) as! [PlaceScore]
        placeName = aDecoder.decodeObject(forKey: AltPlaceKey.name) as! String
        placeDistance = aDecoder.decodeDouble(forKey: AltPlaceKey.distance)
        isGoodAlternate = aDecoder.decodeBool(forKey: AltPlaceKey.isGood)
        placeIdentifier = aDecoder.decodeObject(forKey: AltPlaceKey.identifier) as! String
        placeCanonicalName = aDecoder.decodeObject(forKey: AltPlaceKey.canonicalName) as! String
        placeScore = aDecoder.decodeInteger(forKey: AltPlaceKey.overal_score)
        let lat = aDecoder.decodeDouble(forKey: AltPlaceKey.placeLat)
        let lng = aDecoder.decodeDouble(forKey: AltPlaceKey.placeLng)
        placeCoordinate = CLLocationCoordinate2DMake(lat, lng)
    }
    
    func encode(with aCoder: NSCoder) {
        let scoreData = NSKeyedArchiver.archivedData(withRootObject: scoreArray)
        aCoder.encode(scoreData, forKey: AltPlaceKey.scores)
        aCoder.encode(placeName, forKey: AltPlaceKey.name)
        aCoder.encode(placeDistance, forKey: AltPlaceKey.distance)
        aCoder.encode(isGoodAlternate, forKey: AltPlaceKey.isGood)
        aCoder.encode(placeIdentifier, forKey: AltPlaceKey.identifier)
        aCoder.encode(placeCanonicalName, forKey: AltPlaceKey.canonicalName)
        aCoder.encode(placeScore, forKey: AltPlaceKey.overal_score)
        aCoder.encode(placeCoordinate.latitude, forKey: AltPlaceKey.placeLat)
        aCoder.encode(placeCoordinate.longitude, forKey: AltPlaceKey.placeLng)
    }
    
    static var supportsSecureCoding: Bool {
        return true
    }
    
}
