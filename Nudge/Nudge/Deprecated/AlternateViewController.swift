//
//  AlternateViewController.swift
//  Nudge
//
//  Created by Thanos on 1/2/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import ChameleonFramework
import GoogleMobileAds

class AlternateViewController: UIViewController {
    
    @IBOutlet weak var mainTable: UITableView!
    @IBOutlet weak var headerTopLabel: UILabel!
    @IBOutlet weak var headerBottomLabel: UILabel!
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var forwardButton: UIButton!
    
    public var titleString : String = ""
    public var dataSource : AlternatePlace!
    public var isAlternatePlace : Bool = true
    var tapGesture : UITapGestureRecognizer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white
        //setup tap gesture
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(AlternateViewController.addressTouched(tap:)))
        tapGesture.numberOfTapsRequired = 1
        //update the navigation bar
        setupHeaderStrings()
        //update the bar chart
        setupTableView()
        //hide the export button if scores were found
        //if dataSource.scoreArray[0].score != -1 {
            self.forwardButton.isHidden = true
        //}
    }
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func forwardButtonPressed(_ sender: Any) {
        //show research view
        let storyboard = UIStoryboard(name: "ResearchViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ResearchVC") as! ResearchViewController
        viewController.alternatePlace = self.dataSource
        let navCon = UINavigationController(rootViewController: viewController)
        self.present(navCon, animated: true, completion: nil)
    }
    
    
    func setupHeaderStrings() {
        if isAlternatePlace == false {
            //update container background color
            headerContainerView.backgroundColor = AppColors.nudgeOrangePrimaryColor
            //update the font and text of the top label
            let topFont = UIFont(name: AppFontNames.boldFont, size: 16)!
            headerTopLabel.font = topFont
            headerTopLabel.text = "Nudged away from:"
            headerTopLabel.textColor = UIColor.white
            //update the font and text of the bottom label
            let bottomFont = UIFont(name: AppFontNames.boldFont, size: 24)!
            headerBottomLabel.font = bottomFont
            headerBottomLabel.text = dataSource.placeCanonicalName
            headerBottomLabel.textColor = UIColor.white
        }
        else {
            //update container background color
            headerContainerView.backgroundColor = AppColors.betterOptionsColor
            //update the font and text of the top label
            let topFont = UIFont(name: AppFontNames.boldFont, size: 24)!
            headerTopLabel.font = topFont
            headerTopLabel.text = dataSource.placeCanonicalName
            headerTopLabel.textColor = UIColor.white
            //update the font and text of the bottom label
            let bottomFont = UIFont(name: AppFontNames.regularFont, size: 14)!
            let distance = dataSource.placeDistance
            let formattedDistance = String(format: "%.2f", distance)
            let distanceString = " \(formattedDistance) miles"
            let mapPointImage = NudgeStyleKit.imageOfCanvasMapPoint()
            let textAttachment = NSTextAttachment()
            textAttachment.image = mapPointImage
            textAttachment.bounds = CGRect(x: 0, y: -5, width: mapPointImage.size.width, height: mapPointImage.size.height)
            let imageString = NSAttributedString(attachment: textAttachment)
            let attributedString = NSAttributedString(string: distanceString, attributes: [NSFontAttributeName : bottomFont])
            let mutableString = NSMutableAttributedString()
            mutableString.append(imageString)
            mutableString.append(attributedString)
            headerBottomLabel.attributedText = mutableString
            headerBottomLabel.backgroundColor = UIColor.white
            headerBottomLabel.layer.cornerRadius = 10
            headerBottomLabel.layer.masksToBounds = true
            headerBottomLabel.textColor = AppColors.betterOptionsColor
            headerBottomLabel.baselineAdjustment = .alignCenters
            //add the tap gestures to both name and distance
            headerTopLabel.isUserInteractionEnabled = true
            let topTapGesture = UITapGestureRecognizer(target: self, action: #selector(AlternateViewController.addressTouched(tap:)))
            topTapGesture.numberOfTapsRequired = 1
            headerTopLabel.addGestureRecognizer(topTapGesture)
            headerBottomLabel.isUserInteractionEnabled = true
            let bottomTapGesture = UITapGestureRecognizer(target: self, action: #selector(AlternateViewController.addressTouched(tap:)))
            bottomTapGesture.numberOfTapsRequired = 1
            headerBottomLabel.addGestureRecognizer(bottomTapGesture)
        }
    }
    
    func setupBannerView() {
        let bannerView = GADBannerView(frame: CGRect(x: 0, y: self.view.frame.size.height - 100, width: self.view.frame.size.width, height: 50))
        bannerView.adUnitID = "ca-app-pub-7207616568756381/4300076757"
        bannerView.rootViewController = self
        bannerView.adSize = kGADAdSizeSmartBannerPortrait
        //seutp the request
        let adRequest = GADRequest()
        adRequest.testDevices = [kGADSimulatorID, "844c0b73b146fe583f247ce6c84f61ad"]
        //bannerView.load(adRequest)
        //self.view.addSubview(bannerView)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addressTouched(tap : UITapGestureRecognizer) {
        //open google maps with the name of the place highlighted
        var placeName = self.dataSource.placeName
        let placeLocation = self.dataSource.placeCoordinate
        placeName = placeName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        var mapURL : URL!
        var mapLinkString = ""
        if placeLocation.latitude == 0 && placeLocation.longitude == 0 {
            mapLinkString = placeName
        }
        else {
            mapLinkString = "\(placeLocation.latitude),\(placeLocation.longitude)"
        }
        if UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL) {
            //open in google maps
            mapURL = URL(string: "comgooglemaps://?daddr=\(mapLinkString)")!
        }
        else {
            mapURL = URL(string: "http://maps.apple.com/?address=\(mapLinkString)")!
        }
         UIApplication.shared.open(mapURL, options: [:], completionHandler: nil)
    }
    
    func setupTableView() {
        self.mainTable.delegate = self
        self.mainTable.dataSource = self
        self.mainTable.isScrollEnabled = true
        self.mainTable.backgroundColor = UIColor.clear
        self.mainTable.backgroundView = UIView(frame: CGRect.zero)
    }
    
}

//MARK: UITableView Datasource/Delegate
extension AlternateViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.scoreArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.isAlternatePlace == false {
            return 100
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.isAlternatePlace == false {
            //create the top label
            let fontName = UIFont(name: AppFontNames.boldFont, size: 18)!
            let centerParagraphStyle = NSMutableParagraphStyle()
            centerParagraphStyle.alignment = .center
            let topString = NSAttributedString(string: "Better options this way:", attributes: [NSFontAttributeName : fontName,
                                                                                                NSParagraphStyleAttributeName : centerParagraphStyle])
            let topLabel = UILabel(frame: CGRect(x: 0, y: 35, width: tableView.frame.size.width, height: 22))
            topLabel.backgroundColor = UIColor.clear
            topLabel.attributedText = topString
            //create the bottom label
            let arrowImage = NudgeStyleKit.imageOfCanvasArrow()
            let textAttachment = NSTextAttachment()
            textAttachment.image = arrowImage
            let imageString = NSAttributedString(attachment: textAttachment)
            let arrowLabel = UILabel(frame: CGRect(x: 0, y: 76, width: tableView.frame.size.width - 12, height: 22))
            arrowLabel.attributedText = imageString
            arrowLabel.textAlignment = .right
            let bottomFrame = UIView(frame: CGRect(x: 10, y: 83, width: tableView.frame.size.width - 36, height: 10))
            let dashImage = NudgeStyleKit.imageOfCanvasDarkGrayDashLine()
            bottomFrame.backgroundColor = UIColor(patternImage: dashImage)
            //create the view
            let footerView = UIView(frame: .zero)
            footerView.addSubview(topLabel)
            footerView.addSubview(bottomFrame)
            footerView.addSubview(arrowLabel)
            footerView.backgroundColor = UIColor.clear
            return footerView
        }
        return nil
    }
    
    func didTouchBetterLabel() {
        //print("should swip right")
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell")!
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        //get outlets from cell
        let imageView = cell.viewWithTag(99) as! UIImageView
        let scoreLabel = cell.viewWithTag(98) as! UILabel
        let titleLabel = cell.viewWithTag(97) as! PaddingLabel
        let messageLabel = cell.viewWithTag(95) as! PaddingLabel
        let colorView = cell.viewWithTag(96)!
        //let borderView = cell.viewWithTag(95)!
        //get the item for the row
        let item = self.dataSource.scoreArray[indexPath.row]
        //set the image
        imageView.image = item.category.defaultIcon()
        imageView.tintColor = UIColor.white
        //set the background color of the color view
        colorView.backgroundColor = item.category.primaryColor()
        colorView.layer.cornerRadius = 10
        //set the score
        scoreLabel.text = item.score == -1 ? "--" : "\(item.score)"
        scoreLabel.textColor = UIColor.white
        //set the title
        titleLabel.text = item.title
        titleLabel.backgroundColor = UIColor.white
        //set the message
        messageLabel.text = item.message
        messageLabel.backgroundColor = UIColor.white
        messageLabel.adjustsFontSizeToFitWidth = true
        messageLabel.sizeToFit()
        //update ui for un-scored place
        if item.score == -1 {
            
            //remove title
            titleLabel.text = ""
            //change message to title
            messageLabel.text = item.title
            //update message label font
            let boldFont = UIFont(name : "Lato-Bold", size : 16)!
            messageLabel.font = boldFont
        }
        //add a single line across the bottom of the cell
        let breakView = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - 1, width: cell.frame.size.width, height: 1))
        breakView.backgroundColor = UIColor(hexString: "#7F7982")
        cell.addSubview(breakView)
        
        return cell
    }
}
