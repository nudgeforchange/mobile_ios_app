//
//  AlternatesViewController.swift
//  Nudge
//
//  Created by Thanos on 1/2/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import ChameleonFramework
import GoogleMobileAds

class AlternatesPageViewController: UIPageViewController {
    
    var orderedViewControllers: [UIViewController] = []
    var pageCount = 0
    var selectedIndex = 0
    var alternatePlaceArray : [BusinessLocation] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
        self.setStatusBarStyle(UIStatusBarStyleContrast)
        //set the data source for the page view controller
        dataSource = self
        self.setViewControllers()
        //setup the page control
        self.setupPageControl()
        //setup the content controller 
        let pageContentController = self.orderedViewControllers[selectedIndex]
        setViewControllers([pageContentController], direction: .forward, animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor(hexString: "#B8B7B8")
        appearance.currentPageIndicatorTintColor = UIColor(hexString: "#4B484C")
        appearance.backgroundColor = UIColor.white
    }
    
    
    public func setupPrimaryAltController() -> AlternateViewController {
        let storyboard = UIStoryboard(name: "AlternateViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AlternateVC") as! AlternateViewController
        let place = self.alternatePlaceArray[1]
        //add the title
        viewController.titleString = place.placeName
        viewController.isAlternatePlace = place.isGoodAlternate
        viewController.dataSource = place
        return viewController
    }
    
    public func setupSecondaryAltController() -> AlternateViewController {
        let storyboard = UIStoryboard(name: "AlternateViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AlternateVC") as! AlternateViewController
        let place = self.alternatePlaceArray[2]
        //add the title
        viewController.titleString = place.placeName
        viewController.isAlternatePlace = place.isGoodAlternate
        viewController.dataSource = place
        return viewController
    }
    
    public func setupReasonController() -> AlternateViewController {
        let storyboard = UIStoryboard(name: "AlternateViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AlternateVC") as! AlternateViewController
        let place = self.alternatePlaceArray[0]
        //add the title
        viewController.titleString = place.placeName
        viewController.isAlternatePlace = place.isGoodAlternate
        viewController.dataSource = place
        return viewController
    }
    
    public func setViewControllers() {
        //always add the reason slide first
        self.orderedViewControllers.append(setupReasonController())
        //add primary alternate
        self.orderedViewControllers.append(setupPrimaryAltController())
        //add secondary alternate
        self.orderedViewControllers.append(setupSecondaryAltController())
    }

}

//MARK: PageViewController Delegate
extension AlternatesPageViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return selectedIndex
    }
    
    func goToNextPage(){
        
        guard let currentViewController = self.viewControllers?.first else { return }
        
        guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
        
        setViewControllers([nextViewController], direction: .forward, animated: false, completion: nil)
        
    }
    
    
    func goToPreviousPage(){
        
        guard let currentViewController = self.viewControllers?.first else { return }
        
        guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
        
        setViewControllers([previousViewController], direction: .reverse, animated: false, completion: nil)
        
    }
}
