//
//  GeoNotificationPlace.swift
//  Nudge
//
//  Created by Ultron on 2/4/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

struct GeoKey {
    static let eventType = "eventType"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let radius = "radius"
    
    static let identifier = "identifier"
    static let displayName = "display_name"
    static let canonicalName = "canonical_name"
    static let score = "score"
    static let issues = "issues"
    
    static let dateAdded = "date_added"
    static let addedToday = "added_today"
}

enum EventType: String {
    case onEntry = "On Entry"
    case onExit = "On Exit"
}

class GeoNotificationPlace: NSObject, NSCoding {
    
    var isMonitored : Bool
    var eventType: EventType
    var coordinate: CLLocationCoordinate2D
    var radius: CLLocationDistance
    
    var identifier: String
    var placeName : String
    var canonicalName : String
    var score : Int
    var issues : NSArray
    
    var dateAdded : Date
    
    init(identifier: String, displayName : String, canonicalName : String, latitude: Double, longitude: Double, radius: CLLocationDistance, score : Int, issues : NSArray, eventType: EventType, dateMilli : Int, isMonitored : Bool) {
        self.identifier = identifier
        self.placeName = displayName
        self.canonicalName = canonicalName
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.radius = radius
        self.score = score
        self.issues = issues
        self.eventType = eventType
        self.dateAdded = Date(milliseconds: dateMilli)
        self.isMonitored = isMonitored
    }
    
    init(dictionary: [String : AnyObject], eventType: EventType, date: Date, isMonitored: Bool) {
        self.identifier = dictionary["place_id"] as! String
        self.placeName = dictionary["name"] as! String
        self.canonicalName = dictionary["canonical_name"] as! String
        let location = dictionary["location"] as! NSDictionary
        let latitude = location.value(forKey: "lat") as! Double
        let longitude = location.value(forKey: "lng") as! Double
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.radius = dictionary["geofence_radius"] as! Double
        self.score = dictionary["score"] as! Int
        self.issues = dictionary["issues"] as! NSArray
        self.eventType = eventType
        self.dateAdded = date
        self.isMonitored = isMonitored
    }
    
    // MARK: NSCoding
    required init?(coder decoder: NSCoder) {
        identifier = decoder.decodeObject(forKey: GeoKey.identifier) as! String
        placeName = decoder.decodeObject(forKey: GeoKey.displayName) as! String
        canonicalName = decoder.decodeObject(forKey: GeoKey.canonicalName) as! String
        let latitude = decoder.decodeDouble(forKey: GeoKey.latitude)
        let longitude = decoder.decodeDouble(forKey: GeoKey.longitude)
        coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        radius = decoder.decodeDouble(forKey: GeoKey.radius)
        score = decoder.decodeInteger(forKey: GeoKey.score)
        issues = decoder.decodeObject(forKey: GeoKey.issues) as! NSArray
        eventType = EventType(rawValue: decoder.decodeObject(forKey: GeoKey.eventType) as! String)!
        let dateMilli = decoder.decodeInteger(forKey: GeoKey.dateAdded)
        dateAdded = Date(milliseconds: dateMilli)
        isMonitored = decoder.decodeBool(forKey: GeoKey.addedToday)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(identifier, forKey: GeoKey.identifier)
        coder.encode(placeName, forKey: GeoKey.displayName)
        coder.encode(canonicalName, forKey: GeoKey.canonicalName)
        coder.encode(coordinate.latitude, forKey: GeoKey.latitude)
        coder.encode(coordinate.longitude, forKey: GeoKey.longitude)
        coder.encode(radius, forKey: GeoKey.radius)
        coder.encode(score, forKey: GeoKey.score)
        coder.encode(issues, forKey: GeoKey.issues)
        coder.encode(eventType.rawValue, forKey: GeoKey.eventType)
        coder.encode(dateAdded.milliSeconds, forKey : GeoKey.dateAdded)
        coder.encode(isMonitored, forKey : GeoKey.addedToday)
    }

}


