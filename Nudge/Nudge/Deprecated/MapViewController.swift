//
//  MapViewController.swift
//  Nudge
//
//  Created by Ultron on 2/4/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class MapViewController: UIViewController {
    
    var mapView : MKMapView!
    var alertTableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = AppColors.nudgeRedColor
        //add a map to the screen
        mapView = MKMapView(frame: self.view.frame)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.showsUserLocation = true
        mapView.isZoomEnabled = true
        mapView.delegate = self
        self.view.addSubview(mapView)
        //update maps zoom and center
//        self.updateMapLocation()
        //add the add button
        //let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(MapViewController.addButtonPressed))
        //self.navigationItem.rightBarButtonItem = addButton
        //update map with stored places
//        self.updateMapWithPlaces()
        //add the demo button
        let demoButton = UIBarButtonItem(title: "Demo", style: .done, target: self, action: #selector(MapViewController.demoButtonPressed))
        self.navigationItem.rightBarButtonItem = demoButton
    }
    
    func demoButtonPressed() {
        //setup alert with default style
        let alert = TKAlert()
        alert.delegate = self
        self.setAlertStyle(alert)
        //add title and message
        let titleString = "Demo Options"
        let titleFont = UIFont(name: AppFontNames.lightFont, size: 16)!
        let titleAttributedString = NSAttributedString(string: titleString, attributes: [NSFontAttributeName : titleFont])
        alert.attributedTitle = titleAttributedString
        let messageString = "Tap show nudge to view an example right now or schedule nudge to have an alert sent from the background in 10 seconds (you need to close the app for the notification to show)"
        let messageFont = UIFont(name: AppFontNames.regularFont, size: 16)!
        let messageAttributedString = NSAttributedString(string: messageString, attributes: [NSFontAttributeName : messageFont])
        alert.attributedMessage = messageAttributedString
        //add buttons
        let showNowAction = TKAlertAction(title: "Show nudge", handler: {(alert, action) -> Bool in
//            self.showDemoScreen()
            return true
        })
        showNowAction.backgroundColor = AppColors.nudgeOrangePrimaryColor
        showNowAction.titleColor = UIColor.white
        showNowAction.cornerRadius = 3
        alert.addAction(showNowAction)
        let scheduleAction = TKAlertAction(title: "Schedule nudge", handler: {(alert, action) -> Bool in
//            self.scheduleDemo()
            return true
        })
        scheduleAction.backgroundColor = AppColors.nudgeOrangeSecondaryColor
        scheduleAction.titleColor = UIColor.white
        scheduleAction.cornerRadius = 3
        alert.addAction(scheduleAction)
        let cancelButton = TKAlertAction(title: "Cancel", handler: {(alert, action) -> Bool in
            return true
        })
        cancelButton.backgroundColor = AppColors.lightTextColor
        cancelButton.titleColor = UIColor.white
        cancelButton.cornerRadius = 3
        alert.addAction(cancelButton)
        //update button layout
        alert.actionsLayout = .vertical
        //add alert
        alert.show(true)
    }
    
//    func showDemoScreen() {
//        let storyBoard = UIStoryboard(name: "NudgeSummaryViewController", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "NudgeSummaryVC") as! NudgeSummaryViewController
//        viewController.alternatePlaces = setupDemoArray()
//        let navCon = UINavigationController(rootViewController: viewController)
//        self.present(navCon, animated: true, completion: nil)
//    }
//    
//    func scheduleDemo() {
//        NotificationService.sharedInstance.showBackgroundStateAlert(setupDemoArray(), timeInterval: 10)
//    }
//   
//    func setupDemoArray() -> [AlternatePlace] {
//        return [setupNudgePlace(), setupFirstAlternate(), setupSecondAleternate()]
//    }
//    
//    func setupNudgePlace() -> AlternatePlace {
//        var placeScores : [PlaceScore] = []
//        let score1 = PlaceScore(category: .WORKERSRIGHTS, score: 0, message: "Low wages, denial of benefits, owner publicly opposes sick pay and overtime", isBetterOption: false)
//        placeScores.append(score1)
//        let score2 = PlaceScore(category: .ENVIRONMENT, score: 0, message: "Use deforestation forms of palm oil, vast carbon footprint, no environmental action plan", isBetterOption: false)
//        placeScores.append(score2)
//        let place = AlternatePlace(scoreArray: placeScores, placeName: "Carl's Jr.", placeDistance: 0, isGoodAlternate: false, placeIdentifier: "", placeCanonicalName: "Carl's Jr.", placeScore: 0, latitude: 0, longitude: 0)
//        return place
//    }
//    
//    func setupFirstAlternate() -> AlternatePlace {
//        var placeScores : [PlaceScore] = []
//        let score1 = PlaceScore(category: .ENVIRONMENT, score: 10, message: "Pasture-raised livestock, no synthetic hormones, reduced pesticides, non-GMO", isBetterOption: true)
//        placeScores.append(score1)
//        let score2 = PlaceScore(category: .EQUALLGBTQ, score: 9, message: "Publicly pro-LGBTQ, non-discriminatory employee benefits, gay pride initiatives, refuse to support homophobic orgs/cos", isBetterOption: true)
//        placeScores.append(score2)
//        let score3 = PlaceScore(category: .WORKERSRIGHTS, score: 8, message: "American owned/operated, majority of ingredients from US sources, support American farmers", isBetterOption: true)
//        placeScores.append(score3)
//        let place = AlternatePlace(scoreArray: placeScores, placeName: "Chipotle", placeDistance: 2, isGoodAlternate: true, placeIdentifier: "", placeCanonicalName: "Chipotle", placeScore: 9, latitude: 0, longitude: 0)
//        return place
//    }
//    
//    func setupSecondAleternate() -> AlternatePlace {
//        var placeScores : [PlaceScore] = []
//        let score1 = PlaceScore(category: .WORKERSRIGHTS, score: 10, message: "American owned/operated, all food USA-sourced, philanthropy programs", isBetterOption: true)
//        placeScores.append(score1)
//        let score2 = PlaceScore(category: .ENVIRONMENT, score: 6, message: "Deforestation-free oil, reduced livestock antibiotics, all food prep in-house, no freezing", isBetterOption: true)
//        placeScores.append(score2)
//        let place = AlternatePlace(scoreArray: placeScores, placeName: "In-N-Out Burger", placeDistance: 1.3, isGoodAlternate: true, placeIdentifier: "", placeCanonicalName: "In-N-Out Burger", placeScore: 7, latitude: 0, longitude: 0)
//        return place
//    }
    
    func updateMapLocation() {
        var region = MKCoordinateRegion()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let currentLocation = delegate.locationManager.location!
        region.center = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        region.span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        self.mapView.setRegion(region, animated: true)
    }
    
//    func updateMapWithPlaces() {
//        //check if any places were found
//        let defaults = UserDefaults.standard
//        let dataArray = defaults.data(forKey: AppStrings.savedBusinessLocationsKey)
//        var notificationArray : [GeoNotificationPlace] = []
//        if dataArray != nil {
//            notificationArray = NSKeyedUnarchiver.unarchiveObject(with: dataArray!) as! [GeoNotificationPlace]
//        }
//        if notificationArray.count > 0 {
//            for notification in notificationArray {
//                let circleView = MKCircle(center: notification.coordinate, radius: notification.radius)
//                circleView.title = "Show Nudge"
//                circleView.subtitle = notification.identifier
//                self.mapView.add(circleView)
//            }
//        }
//    }
// 
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    func getGeoNotifcationPlace(_ identifier : String) -> GeoNotificationPlace? {
//        let defaults = UserDefaults.standard
//        let storedData = defaults.object(forKey: AppStrings.savedBusinessLocationsKey) as! Data
//        let notificationArray = NSKeyedUnarchiver.unarchiveObject(with: storedData) as! [GeoNotificationPlace]
//        for notification in notificationArray {
//            if notification.identifier == identifier {
//                return notification
//            }
//        }
//        return nil
//    }
}


extension MapViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            //add the overlay to the map as an annotation
            self.mapView.addAnnotation(overlay)
            let circleRender = MKCircleRenderer(overlay: overlay)
            circleRender.lineWidth = 1.0
            circleRender.strokeColor = UIColor.flatRed
            circleRender.fillColor = UIColor.flatRed.withAlphaComponent(0.4)
            return circleRender
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
        let detailButton = UIButton(type: .detailDisclosure)
        let label = UILabel(frame: annotationView.frame)
        let placeID = annotation.subtitle!
        let businessLocation = getGeoNotifcationPlace(placeID!)!
        label.text = "for: \(businessLocation.placeName)"
        let labelFont = UIFont(name: AppFontNames.regularFont, size: 14)
        label.font = labelFont!
        annotationView.detailCalloutAccessoryView = label
        annotationView.rightCalloutAccessoryView = detailButton
        annotationView.canShowCallout = true
        annotationView.isEnabled = true
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let placeID = view.annotation?.subtitle!
        //print("should show nudge for placeID : \(placeID)")
        //show activity indicator
        self.showIndicator("Getting alternate places...")
        //get place from id
        let place = getGeoNotifcationPlace(placeID!)
        BusinessService.sharedInstance.fetchAlternativeBusinessLocations(place!, inBackground: false)
        //hide the annotations
        mapView.deselectAnnotation(view.annotation, animated: true)
    }
}

