//
//  NudgeSummaryViewController.swift
//  Nudge
//
//  Created by Thanos on 1/11/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import GoogleMobileAds

class NudgeSummaryViewController: UIViewController {
    
    
    @IBOutlet weak var mainTable: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var alternatePlacesArray : [AlternatePlace] = []
    var collectionViewDataSource : [PlaceScore]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //self.setupPlaceArray()
        self.setupTitle()
        //setup table delegate/datasource
        self.mainTable.delegate = self
        self.mainTable.dataSource = self
        self.setupBackgroundColor()
        //add a close button
        let closeButton = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(NudgeSummaryViewController.closeButtonPressed))
        self.navigationItem.leftBarButtonItem = closeButton
    }
    
    func closeButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupTitle() {
        //set the title to the place name
        let nudgePlace = self.alternatePlacesArray[0]
        let placeName = nudgePlace.placeName
        let titleString = "Nudged from: \(placeName)"
        self.navigationItem.title = titleString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    func setupBackgroundColor() {
        
        self.view.backgroundColor = UIColor.white
        self.mainTable.backgroundView = UIView(frame: CGRect.zero)
        self.mainTable.backgroundColor = UIColor.clear
    }
    
    func setupBannerView() {
        bannerView.adUnitID = "ca-app-pub-7207616568756381/4300076757"
        bannerView.rootViewController = self
        bannerView.adSize = kGADAdSizeSmartBannerPortrait
        //seutp the request
        let adRequest = GADRequest()
        adRequest.testDevices = [kGADSimulatorID, "844c0b73b146fe583f247ce6c84f61ad"]
        ///bannerView.load(adRequest)
        bannerView.backgroundColor = UIColor.clear
    }
    
}

//MARK: TableView Delegate/DataSource
extension NudgeSummaryViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return alternatePlacesArray.count - 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Nudged away from:"
        }
        return "Better options:"
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 40
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        //set the font
        let font = UIFont(name: "Lato-Bold", size: 18)!
        header.textLabel?.font = font
        //set the text color
        header.textLabel?.textColor = AppColors.darkTextColor
        //set the frame
        header.textLabel?.frame = header.frame
        //set the alignment
        header.textLabel?.textAlignment = .center
        //set text case
        header.textLabel?.text = header.textLabel?.text?.capitalized
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let dashImage = NudgeStyleKit.imageOfCanvasGrayDashLine()
            let dashView = UIView(frame: CGRect(x: 20, y: 0, width: tableView.frame.size.width - 40, height: 10))
            dashView.backgroundColor = UIColor(patternImage: dashImage)
            return dashView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //get the place item
        var rowItem : AlternatePlace!
        if indexPath.section == 0 {
            rowItem = self.alternatePlacesArray[indexPath.row]
        }
        else {
            rowItem = self.alternatePlacesArray[indexPath.row + 1]
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell2")!
        self.setupTableCell(cell: cell, rowItem: rowItem, indexPath: indexPath)
        return cell
        
    }
    
    func setupTableCell(cell : UITableViewCell, rowItem : AlternatePlace, indexPath : IndexPath) {
        
        //get outlets
        let backgroundView = cell.viewWithTag(99)!
        let headerLabel = cell.viewWithTag(98) as! UILabel
        let collectionView = cell.viewWithTag(97) as! UICollectionView
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        //update the background view corner radius and shadow
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOffset = CGSize(width: 3, height: 0)
        backgroundView.layer.shadowOpacity = 0.25
        backgroundView.layer.shadowRadius = 3
        //add the place name and distance for better places and background color
        if rowItem.isGoodAlternate == true {
            let distance = rowItem.placeDistance
            let distanceString = String(format: "%.2f", distance)
            let titleString = "\(rowItem.placeName) - \(distanceString) miles"
            headerLabel.text = titleString
            //use good color
            backgroundView.backgroundColor = AppColors.betterOptionsColor
        }
        else {
            headerLabel.text = rowItem.placeName
            //use nudge primary color
            backgroundView.backgroundColor = AppColors.nudgeOrangePrimaryColor
        }
        //update label colors
        headerLabel.textColor = UIColor.white
        headerLabel.backgroundColor = UIColor.clear
        //set the collectionview
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewDataSource = rowItem.scoreArray
        //add a view with tap gestures to capture all touches
        let tapGesture = TapWithIndexPath(target: self, action: #selector(NudgeSummaryViewController.cellTapped(_:)), indexPath : indexPath)
        tapGesture.numberOfTapsRequired = 1
        let tapView = UIView(frame: cell.contentView.frame)
        tapView.backgroundColor = UIColor.clear
        tapView.isUserInteractionEnabled = true
        tapView.addGestureRecognizer(tapGesture)
        cell.addSubview(tapView)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func cellTapped(_ sender : TapWithIndexPath) {
        var showIndex = 0
        if sender.indexPath.section == 0 {
            showIndex = sender.indexPath.row
        }
        else {
            showIndex = sender.indexPath.row + 1
        }
        
        //show the place details
        let storyboard = UIStoryboard(name: "AlternatesPageViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AlternatesVC") as! AlternatesPageViewController
        viewController.selectedIndex = showIndex
        viewController.alternatePlaceArray = self.alternatePlacesArray
        viewController.modalPresentationCapturesStatusBarAppearance = true
        self.present(viewController, animated: true, completion: nil)
    }
}

extension NudgeSummaryViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.collectionViewDataSource.count > 3 {
            return 3
        }
        return self.collectionViewDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scoreCell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        //get outlets
        let imageView = cell.viewWithTag(99) as! UIImageView
        let scoreLabel = cell.viewWithTag(98) as! UILabel
        //get the score item
        let scoreItem = self.collectionViewDataSource[indexPath.row]
        //update label
        scoreLabel.text = scoreItem.score == -1 ? "--" : "\(scoreItem.score)"
        scoreLabel.textColor = UIColor.white
        scoreLabel.backgroundColor = UIColor.clear
        //update the image
        imageView.image = scoreItem.category.defaultIcon()
        imageView.tintColor = UIColor.white
        
        return cell
    }
    
}

extension NudgeSummaryViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let itemCount = self.collectionViewDataSource.count > 3 ? 3 : self.collectionViewDataSource.count
        let edgeInsets = (collectionView.frame.size.width - (CGFloat(itemCount) * 75) - (CGFloat(itemCount) * 10)) / 2
        return UIEdgeInsets(top: 0, left: edgeInsets, bottom: 0, right: edgeInsets)
    }
}
