//
//  PlaceScore.swift
//  Nudge
//
//  Created by Ultron on 2/7/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

struct ScoreKey {
    static let category = "category"
    static let score = "score"
    static let title = "title"
    static let message = "message"
    static let isBetter = "isbetter"
}

class PlaceScore: NSObject, NSSecureCoding {

    var category : ScoreCategories
    var score : Int
    var title : String
    var message : String
    var isBetterOption : Bool
    
    init(category : ScoreCategories, score : Int, message : String, isBetterOption : Bool) {
        self.category = category
        self.score = score
        self.title = score.getTitleForScore()
        self.message = message
        self.isBetterOption = isBetterOption
    }
    
    required init?(coder aDecoder: NSCoder) {
        let categoryString = aDecoder.decodeObject(forKey: ScoreKey.category) as! String
        category = categoryString.categoryFromSlug()
        score = aDecoder.decodeInteger(forKey: ScoreKey.score)
        title = aDecoder.decodeObject(forKey: ScoreKey.title) as! String
        message = aDecoder.decodeObject(forKey: ScoreKey.message) as! String
        isBetterOption = aDecoder.decodeBool(forKey: ScoreKey.isBetter)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(category.description(), forKey: ScoreKey.category)
        aCoder.encode(score, forKey: ScoreKey.score)
        aCoder.encode(title, forKey: ScoreKey.title)
        aCoder.encode(message, forKey: ScoreKey.message)
        aCoder.encode(isBetterOption, forKey: ScoreKey.isBetter)
    }
    
    static var supportsSecureCoding: Bool {
        return true
    }
}
