//
//  SplashViewController.swift
//  Nudge
//
//  Created by Thanos on 1/20/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupActionButton()
    }
    
    func setupActionButton() {
        self.actionButton.backgroundColor = UIColor.flatGreen
        self.actionButton.layer.cornerRadius = 12
        self.actionButton.layer.borderWidth = 1
        self.actionButton.layer.borderColor = UIColor.flatGreenDark.cgColor
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionButtonPressed(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC")
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
