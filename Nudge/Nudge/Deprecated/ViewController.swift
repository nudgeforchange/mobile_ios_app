//
//  ViewController.swift
//  Nudge
//
//  Created by Thanos on 12/12/16.
//  Copyright © 2016 Level 3 Studios. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import UserNotifications
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dashContainerView: UIView!
    
    
    let titleString = "Nudge for Change"
    //var placesClient : GMSPlacesClient!
    //var deferringUpdates : Bool = false
    var hideStatusBar = false
    var shouldShowFeedback : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //add the title
        self.navigationItem.title = titleString
        //add the side menu button
        let image = NudgeStyleKit.imageOfCanvasMenu(imageSize: CGSize(width: 22, height: 22))
        let showSideButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(ViewController.showSideMenu))
        self.navigationItem.leftBarButtonItem = showSideButton
        //add the search menu button
        let searchImage = NudgeStyleKit.imageOfCanvasSearch(imageSize: CGSize(width: 22, height: 22))
        let searchButton = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(ViewController.showMapScreen))
        self.navigationItem.rightBarButtonItem = searchButton
        //setup the side menu
        self.sideDrawer.delegate = self
        self.setupSideMenu()
        //setup the home page
        self.setupHomePage()
    }
    
    func setupHomePage() {
        //set the background color
        let colors : [UIColor] = [AppColors.nudgeOrangeSecondaryColor, AppColors.nudgeOrangePrimaryColor, AppColors.nudgeOrangePrimaryColor]
        let gradientColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: colors)
        self.view.backgroundColor = gradientColor
        //setup the tally box
        self.topLabel.layer.cornerRadius = 10
        self.topLabel.layer.borderWidth = 2
        self.topLabel.layer.borderColor = AppColors.darkTextColor.cgColor
        self.topLabel.backgroundColor = UIColor.white
        self.topLabel.clipsToBounds = true
        self.topLabel.layer.masksToBounds = true
        self.topLabel.textColor = AppColors.darkTextColor
        
        let titleString = NSAttributedString(string: "You've been nudging for change!\n", attributes: [NSFontAttributeName : UIFont(name:"Lato-Bold", size:12)!])
        let randomNumber1 = arc4random_uniform(10) + 10
        let randomNumber2 = arc4random_uniform(5) + 1
        let scoreFont = UIFont(name: "Lato-Regular", size: 12)!
        let monthString = NSAttributedString(string: "\(randomNumber1) successful nudges this month\n", attributes: [NSFontAttributeName : scoreFont])
        let weekString = NSAttributedString(string: "\(randomNumber2) successful nudges this week", attributes: [NSFontAttributeName : scoreFont])
        let attributedText = NSMutableAttributedString()
        attributedText.append(titleString)
        attributedText.append(monthString)
        attributedText.append(weekString)
        self.topLabel.attributedText = attributedText
        //set the title
        self.titleLabel.text = "Welcome back."
        //set the message text
        self.messageLabel.text = "Nudge for Change empowers you to put your money where your beliefs are. We make doing good easy by nudging you toward purchases that support your core values (and away from ones that don't)\n\nYou're doing an awesome job of voting with your wallet to make the world a better place for all of us every single day.\n\nKeep up the good work!\n\n"
        //add the dash image
        let dashImage = NudgeStyleKit.imageOfCanvasDashLine()
        self.dashContainerView.backgroundColor = UIColor(patternImage: dashImage)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //original title
        self.navigationItem.title = titleString
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //back title
        self.navigationItem.title = "Back"
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //get bools for questionaire and location question
        let questionaireCompleted = UserDefaults.standard.bool(forKey: AppStrings.questionaireBool)
        if questionaireCompleted == false {
            //print("show the questionaire")
            self.showQuestionaire()
        }
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        //print("prefer status bar animation called")
        return .fade
    }
    
    override var prefersStatusBarHidden : Bool {
        //print("prefer status bar hidden called")
        return hideStatusBar
    }
    
    func setupSideMenu() {
        //setup the transition type
        self.sideDrawer.transition = .slideAlong
        self.sideDrawer.transitionDuration = 0.5
        //set the background color
        self.sideDrawer.fill = TKSolidFill(color: AppColors.lightTextColor)
        //add the reset button to the top section
        let topSection = self.sideDrawer.addSection(withTitle: "")!
        topSection.addItem(withTitle: "Reset Questions")
        //add the feedback button to a second section
        let secondSection = self.sideDrawer.addSection(withTitle: "")!
        secondSection.addItem(withTitle: "Send Feedback")
        //add shadow to the host view
        self.sideDrawer.style.shadowMode = .hostview
        self.sideDrawer.style.shadowRadius = 5
        self.sideDrawer.style.shadowOffset = CGSize(width: -2, height: -0.5)
    }
    
    func showSideMenu() {
        //hide the status bar
        hideStatusBar = !hideStatusBar
        
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            //print("animate status bar")
            self.setNeedsStatusBarAppearanceUpdate()
        })
        //show the side menu
        self.sideDrawer.show()
    }
    
    func showMapScreen() {
        //show the map
        let storyboard = UIStoryboard(name: "MapViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MapVC") as! MapViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showQuestionaire() {
        let storyBoard = UIStoryboard(name: "OnboardingPageViewController", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "OnboardingPageVC")
        viewController.modalPresentationCapturesStatusBarAppearance = true
        //setup the navigation controller
        let navCon = UINavigationController(rootViewController: viewController)
        navCon.isNavigationBarHidden = true
        self.present(navCon, animated: false, completion: nil)
    }
    
    func showFeedBackWindow() {
        let storyBoard = UIStoryboard(name: "FeedbackViewController", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FeedbackVC")
        viewController.modalPresentationCapturesStatusBarAppearance = true
        let navCon = UINavigationController(rootViewController: viewController)
        self.present(navCon, animated: true, completion: nil)
    }
}

extension ViewController : TKSideDrawerDelegate {
    func didDismiss(_ sideDrawer: TKSideDrawer!) {
        //show the status bar
        hideStatusBar = !hideStatusBar
        //print(hideStatusBar)
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            //print("animate status bar")
            self.setNeedsStatusBarAppearanceUpdate()
        })
        //check if the questionaire needs to be shown
        let questionFlag = UserDefaults.standard.bool(forKey: AppStrings.questionaireBool)
        if !questionFlag {
            self.showQuestionaire()
        }
        else if self.shouldShowFeedback == true {
            self.showFeedBackWindow()
            self.shouldShowFeedback = false 
        }
    }
    
    func sideDrawer(_ sideDrawer: TKSideDrawer!, didSelectItemAt indexPath: IndexPath!) {
        //print(indexPath)
        if indexPath.section == 0 {
            //reset the questionaire flag
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: AppStrings.questionaireBool)
            defaults.removeObject(forKey: AppStrings.locationBool)
            defaults.removeObject(forKey: AppStrings.savedBusinessLocationsKey)
            defaults.synchronize()
        }
        else if indexPath.section == 1 {
            //show the feedback window
            self.shouldShowFeedback = true
        }
    }
    
    func sideDrawer(_ sideDrawer: TKSideDrawer!, heightForItemAt indexPath: IndexPath!) -> CGFloat {
        return 44
    }
    
    func sideDrawer(_ sideDrawer: TKSideDrawer!, updateVisualsForItemAt indexPath: IndexPath!) {
        let section = sideDrawer.sections[indexPath.section] as! TKSideDrawerSection
        let rowItem = section.items[indexPath.item] as! TKSideDrawerItem
        //update the font
        rowItem.style.font = UIFont(name: "Nunito-Bold", size: 18)!
        rowItem.style.textColor = UIColor.white
        rowItem.style.separatorColor = TKSolidFill(color: .clear)
    }
    
    
}

extension ViewController : CLLocationManagerDelegate {
    //MARK: Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        //print("monitoring failed for region : \(region?.identifier)")
    }
}

//MARK: Data Source Item
class LocationItem : NSObject {
    var name : String!
    var distance : Double!
    
    override init() {
        super.init()
    }
    
    init(name : String, distance : Double) {
        self.name = name
        self.distance = distance
    }
}
