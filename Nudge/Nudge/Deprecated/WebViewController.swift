//
//  WebViewController.swift
//  Nudge
//
//  Created by Magneto on 3/27/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let policyURL = "https://nudgeforchange.com/privacy/?app"
        let webView = UIWebView(frame: self.view.frame)
        let requestURL = URL(string: policyURL)!
        webView.loadRequest(URLRequest(url: requestURL))
        self.view.addSubview(webView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
