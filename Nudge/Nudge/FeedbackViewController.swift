//
//  FeedbackViewController.swift
//  Nudge
//
//  Created by Thanos on 1/26/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {
   
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Feedback"
        self.setBackgroundColors()  
        //self.navigationController?.navigationBar.updateNavigationBar()
        //add the web view
        let webView = UIWebView(frame: self.view.frame)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(webView)
        let formAddress = "https://docs.google.com/forms/d/1apBPRJIpC_Bu_sZAFq8GAK6zLRLtKSk7dGrvcvzIw7s/viewform"
        let formURL = URL(string: formAddress)!
        let formRequest = URLRequest(url: formURL)
        webView.loadRequest(formRequest)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
