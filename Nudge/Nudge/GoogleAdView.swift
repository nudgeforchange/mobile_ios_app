//
//  GoogleAdView.swift
//  Nudge
//
//  Created by Magneto on 5/22/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation
import GoogleMobileAds

func setupAdView(_ rootView : UIViewController) -> GADBannerView {
    
    let bannerView = GADBannerView(frame: CGRect(x: 0, y: rootView.view.bounds.size.height-100, width: 320, height: 50))
    bannerView.adUnitID = "ca-app-pub-2077196498025929/2908944890"
    bannerView.rootViewController = rootView
    let request = GADRequest()
    request.testDevices = [kGADSimulatorID]
    bannerView.load(request)
    return bannerView
}
