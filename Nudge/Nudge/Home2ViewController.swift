import UIKit

class Home2ViewController: UIViewController {
    
    @IBOutlet weak var orangeViewWidth: NSLayoutConstraint!
    @IBOutlet weak var orangeView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var visitedBusinessLocations: [BusinessLocation]! {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.insertSubview(orangeView, at: 0)
        orangeViewWidth.constant = self.view.bounds.size.width
        //tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        
        let headerViewNib: UINib = UINib(nibName: "HomeHeaderView", bundle: nil)
        tableView.register(headerViewNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        let scoredBusinessCellNib: UINib = UINib(nibName: "ScoredBusinessCell", bundle: nil)
        tableView.register(scoredBusinessCellNib, forCellReuseIdentifier: "ScoredBusinessCell")
        let spacerNib: UINib = UINib(nibName: "ScoredBusinessSpacerCell", bundle: nil)
        tableView.register(spacerNib, forCellReuseIdentifier: "ScoredBusinessSpacerCell")
        
        self.visitedBusinessLocations = StorageService.sharedInstance.loadVisitedBusinessLocations()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
        
        self.visitedBusinessLocations = StorageService.sharedInstance.loadVisitedBusinessLocations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Home2ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.visitedBusinessLocations.count * 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0 {
            let businessLocation = self.visitedBusinessLocations[indexPath.row / 2]
            let cell: ScoredBusinessCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessCell", for: indexPath) as! ScoredBusinessCell
            cell.businessLocation = businessLocation
            return cell
        } else {
            let cell: ScoredBusinessSpacerCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessSpacerCell", for: indexPath) as! ScoredBusinessSpacerCell
            return cell
        }
    }
}

extension Home2ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UITableViewHeaderFooterView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView")!
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row % 2 == 0 ? 140 : 8
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 2 == 0 {
            let storyBoard = UIStoryboard(name: "NudgeSummaryViewController", bundle: nil)
            let nudgeSummaryVC = storyBoard.instantiateViewController(withIdentifier: "NudgeSummaryVC") as! NudgeSummaryViewController
            nudgeSummaryVC.businessLocation = self.visitedBusinessLocations[indexPath.row / 2]
            self.navigationController?.pushViewController(nudgeSummaryVC, animated: true)
        }
    }
}
