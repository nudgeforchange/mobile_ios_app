//
//  HomeHeaderEmptyView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 4/5/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class HomeHeaderEmptyView: UITableViewHeaderFooterView {

    @IBOutlet weak var colorView: UIView!

}
