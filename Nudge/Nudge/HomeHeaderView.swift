//
//  HomeHeaderView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/18/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class HomeHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var colorTop: NSLayoutConstraint!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
