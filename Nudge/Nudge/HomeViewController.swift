import UIKit
import DZNEmptyDataSet

class HomeViewController: UIViewController {
    
    @IBOutlet weak var statusBarBackgroundView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var colorTop: NSLayoutConstraint!
    
    var headerHeight: NSLayoutConstraint?
    
    var lastY: CGFloat = 0.0
    let currentSlugCount = 6
    
    var nudges: [Nudge] {
        get {
            return StorageService.sharedInstance.nudges
        }
    }
    
    deinit {
        StorageService.sharedInstance.removeNudgesObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gradientColor(height: CGFloat) -> UIColor {
        let colors = [AppColors.nudgeYellowColor, AppColors.nudgePurpleColor]
        return UIColor(gradientStyle: .topToBottom, withFrame: CGRect(x: 0, y: 0, width: self.colorView.bounds.size.width, height: height), andColors: colors)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.statusBarBackgroundView.backgroundColor = AppColors.nudgeYellowColor
//        self.tableView.bounces = false
        self.tableView.estimatedSectionHeaderHeight = self.nudges.count > 0 ? 160 : 210
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        
        self.view.insertSubview(colorView, at: 0)
        colorView.backgroundColor = AppColors.nudgePurpleColor
        //tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        let headerEmptyViewNib: UINib = UINib(nibName: "HomeHeaderEmptyView", bundle: nil)
        tableView.register(headerEmptyViewNib, forHeaderFooterViewReuseIdentifier: "HeaderEmptyView")
        let headerViewNib: UINib = UINib(nibName: "HomeHeaderView", bundle: nil)
        tableView.register(headerViewNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        let scoredBusinessCellNib: UINib = UINib(nibName: "ScoredBusinessCell", bundle: nil)
        tableView.register(scoredBusinessCellNib, forCellReuseIdentifier: "ScoredBusinessCell")
        let spacerNib: UINib = UINib(nibName: "ScoredBusinessSpacerCell", bundle: nil)
        tableView.register(spacerNib, forCellReuseIdentifier: "ScoredBusinessSpacerCell")
        
        StorageService.sharedInstance.addNudgesObserver(self)
        
        //check belief count for users that have already answered before a new belief was added.
        if let currentBeliefs = SettingsService.sharedInstance.beliefs {
            if currentBeliefs.count > 0 && currentBeliefs.count < self.currentSlugCount {
                let alert = UIAlertController(title: "Want to #resist Trump?", message: "Tell us if you would like to be nudged away from businesses that support Trump.", preferredStyle: .alert)
                let okayAction = UIAlertAction(title: "Choose now", style: .default, handler: {(action) -> Void in
                    var allSlugs = ScoreCategories.allValues
                    allSlugs.removeLast()
                    let currentSlugs = [String](currentBeliefs.keys)
                    var missingCards : [String] = []
                    var missingBeliefKeys : [String] = []
                    for slug in allSlugs {
                        if !currentSlugs.contains(slug) {
                            missingCards.append(slug.cardImageFromSlug())
                            missingBeliefKeys.append(slug)
                        }
                    }
                    let storyBoard = UIStoryboard(name: "QuestionsViewController", bundle: nil)
                    let viewController = storyBoard.instantiateViewController(withIdentifier: "MissedQuestionVC") as! MissedQuestionViewController
                    viewController.beliefKeyArray = missingBeliefKeys
                    viewController.cardArray = missingCards
                    self.present(viewController, animated: true, completion: nil)
                })
                alert.addAction(okayAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        //self.view.addSubview(setupAdView(self))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.alignColorView(self.tableView.contentOffset.y)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
        
        //remove badge count when view appears
        UserDefaults.standard.set(0, forKey: AppStrings.badgeCount)
        UserDefaults.standard.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alignColorView(_ yOffset: CGFloat) {
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        self.colorTop.constant = headerRect.maxY - yOffset
    }
    
    func shareButtonPressed(_ sender: UIButton) {
        //print("nudgingAwayButtonPressed")
        let businessLocation = self.nudges[sender.tag].businessLocation
        self.presentNudgeAwayShareAlert(businessLocation: businessLocation)
    }
    
    func presentNudgeAwayShareAlert(businessLocation: BusinessLocation) {
        let shareAlert = AlertControllerFactory.nudgingAwayShareAlert(completion: { (serviceType) in
            if let serviceType = serviceType {
                if let shareController = SharingService.nudgingAwayController(businessLocation: businessLocation, serviceType: serviceType) {
                    self.present(shareController, animated: true, completion: { })
                }
            }
        })
        self.present(shareAlert, animated: true) { }
    }

}

extension HomeViewController: NudgeObserver {
    func nudgesDidChange() {
        self.tableView.estimatedSectionHeaderHeight = self.nudges.count > 0 ? 160 : 210
        self.tableView.reloadData()
        self.alignColorView(self.tableView.contentOffset.y)
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nudges.count * 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0 {
            let nudge = self.nudges[indexPath.row / 2]
            let cell: ScoredBusinessCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessCell", for: indexPath) as! ScoredBusinessCell
            cell.businessLocation = nudge.businessLocation
//            cell.businessNameLabel.textColor = AppColors.nudgeRedColor
            cell.nudgingAwayView.collapse()
            cell.shareButton.tintColor = AppColors.nudgeRedColor
            cell.shareButton.tag = indexPath.row / 2
            cell.shareButton.removeTarget(nil, action: nil, for: .touchUpInside)
            cell.shareButton.addTarget(self, action: #selector(shareButtonPressed(_:)), for: .touchUpInside)
            return cell
        } else {
            let cell: ScoredBusinessSpacerCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessSpacerCell", for: indexPath) as! ScoredBusinessSpacerCell
            return cell
        }
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 160
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.nudges.count > 0 {
            self.tableView.estimatedSectionHeaderHeight = 160
            let headerView: HomeHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HomeHeaderView
            headerView.colorView.backgroundColor = self.gradientColor(height: 160)
            headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            headerView.layer.shadowRadius = 10
            headerView.layer.shadowOpacity = 0
            self.headerHeight = NSLayoutConstraint(item: headerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 160)
            headerView.addConstraint(self.headerHeight!)
            return headerView
        } else {
            self.tableView.estimatedSectionHeaderHeight = 210
            let headerView: HomeHeaderEmptyView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderEmptyView") as! HomeHeaderEmptyView
            headerView.colorView.backgroundColor = self.gradientColor(height: 210)
            let headerHeight = NSLayoutConstraint(item: headerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 210)
            headerView.addConstraint(headerHeight)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row % 2 == 0 ? 140 : 8
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 2 == 0 {
            let storyBoard = UIStoryboard(name: "NudgeSummaryViewController", bundle: nil)
            let nudgeSummaryVC = storyBoard.instantiateViewController(withIdentifier: "NudgeSummaryVC") as! NudgeSummaryViewController
            nudgeSummaryVC.nudge = self.nudges[indexPath.row / 2]
            self.navigationController?.pushViewController(nudgeSummaryVC, animated: true)
        }
    }
}

extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = tableView.contentOffset.y
        
//        if let headerHeight = self.headerHeight {
//            headerHeight.constant = max(50, 160 - max(0, yOffset))
            scrollView.contentInset.top = min(0, max(-110, -yOffset))
            
            alignColorView(yOffset)
            
            // fade color view logic
            let firstCellObscurage = yOffset + scrollView.contentInset.top
            self.colorView.alpha = 1.0 - max(0, min(1, firstCellObscurage/50.0))
            
            if let headerView = self.tableView.headerView(forSection: 0) as? HomeHeaderView {
                headerView.colorTop.constant = -scrollView.contentInset.top
                headerView.colorView.backgroundColor = self.gradientColor(height: headerView.bounds.height + scrollView.contentInset.top)
                
                // shadow logic
                headerView.layer.shadowOpacity = (Float(1.0) - Float(colorView.alpha))/Float(5.0)
            }
//        }
        
//        // overscrolling logic
//        let currentY = scrollView.contentOffset.y
//        if currentY > lastY {
//            //"scrolling down"
//            tableView.bounces = true
//        } else {
//            //"scrolling up"
//            // Check that we are not in bottom bounce
//            if scrollView.contentInset.top >= 0 {
//                tableView.bounces = false
//            }
//        }
//        lastY = scrollView.contentOffset.y
    }
    
}

extension HomeViewController : DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "graphic_nudgehog-nudge")?.alpha(0.5)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string = "The nudgehog is doing its work"
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = .center
        let attributes = [NSFontAttributeName : UIFont(name: AppFontNames.regularFont, size: 16)!,
                          NSForegroundColorAttributeName : AppColors.nudgeMutedGrayColor,
                          NSParagraphStyleAttributeName : paragraphStyle]
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return AppColors.nudgeWhiteColor
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return self.tableView.rectForHeader(inSection: 0).height/2
    }
}

extension HomeViewController : DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}
