//
//  IgnoreListViewController.swift
//  Nudge
//
//  Created by Magneto on 3/15/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class IgnoreListViewController: UIViewController {
    
    var dataSource : [String] = []
    var tableView : UITableView!
    var shadowContainer : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "My No Nudge List"
        self.setBackgroundColors()
        self.addTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func addTableView() {
        tableView = UITableView(frame: CGRect(x: 12, y: 80, width: self.view.frame.size.width-24, height: self.view.frame.size.height-120), style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.backgroundColor = UIColor.white
        tableView.layer.cornerRadius = 8
        tableView.layer.masksToBounds = true
        tableView.tableFooterView = UIView()
        //add the shadow behind the table
        shadowContainer = UIView(frame: tableView.frame)
        shadowContainer.backgroundColor = UIColor.white
        shadowContainer.layer.cornerRadius = 8
        shadowContainer.layer.shadowColor = AppColors.lightTextColor.cgColor
        shadowContainer.layer.shadowOffset = CGSize.zero
        shadowContainer.layer.shadowOpacity = 0.5
        shadowContainer.layer.shadowRadius = 8
        self.view.addSubview(shadowContainer)
        self.view.addSubview(tableView)
    }
}

extension IgnoreListViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        cell?.textLabel?.text = ""
        
        return cell!
    }
}

extension IgnoreListViewController : DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "graphic_nudgehog-box")?.alpha(0.5)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string = "Nudgehog didn't find anything here"
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = .center
        let attributes = [NSFontAttributeName : UIFont(name: AppFontNames.regularFont, size: 16)!,
                          NSForegroundColorAttributeName : AppColors.nudgeMutedGrayColor,
                          NSParagraphStyleAttributeName : paragraphStyle]
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return AppColors.nudgeWhiteColor
    }
}

extension IgnoreListViewController : DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetDidAppear(_ scrollView: UIScrollView!) {
        self.shadowContainer.isHidden = true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}

