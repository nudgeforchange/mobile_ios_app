//
//  LocationSearchResult.swift
//  Nudge
//
//  Created by Magneto on 3/24/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class LocationSearchResult: NSObject {
    var placeID : String
    var name : String
    
    init(placeID: String, name: String) {
        self.placeID = placeID
        self.name = name    
    }

}
