//
//  LocationService.swift
//  Nudge
//
//  Created by Thanos on 10/16/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation
import SwiftLocation
import SwiftDate
/*
class LocationService {
    static let sharedInstance = LocationService()
    
    var currentLocation : CLLocation!
    var oneShotRequest : LocationRequest!
    var backgroundRequest : LocationRequest!
    var isPerformingNudge = false
    var locationManager : CLLocationManager!
    
    var monitoredBusinessLocations : [BusinessLocation] {
        get {
            return StorageService.sharedInstance.monitoredBusinessLocations
        }
        set {
            StorageService.sharedInstance.monitoredBusinessLocations = newValue
        }
    }
    
    var lastLocationUpdate : Date = Date(timeIntervalSinceReferenceDate: 0)
    
    func setupLocationService() {
        self.locationManager = CLLocationManager()
        Location.autoPauseUpdates = true
        self.startMonitoringBusinessLocations()
    }
    
    
    func getCurrentLocation(completion : @escaping (_ location : CLLocation) -> Void) {
        if self.currentLocation == nil || self.currentLocation.timestamp.timeIntervalSinceNow.in(.minute)! > 6 {
            self.stopBackgroundTask()
            self.oneShotRequest = Location.getLocation(accuracy: .room, frequency: .oneShot, success: {(request, location) in
               // print("current location found")
                self.currentLocation = location
                self.startBackgroundTask()
                completion(self.currentLocation)
            }, error: {(request, last, error) in
               // print(error)
                request.cancel()
                if last != nil {
                    self.currentLocation = last
                    self.startBackgroundTask()
                    completion(self.currentLocation)
                }
            })
        }
        else {
            completion(self.currentLocation)
        }
    }
 
    
    func oneShotForRegion(_ monitoredBL : BusinessLocation) {
        self.stopBackgroundTask()
        LogKit.sharedInstance.sendDebugMessage("starting request for one shot for : \(monitoredBL.canonicalName) at : \(Date().shortReadableDescription())")
        Location.getLocation(accuracy: .room, frequency: .oneShot, success: {(request, location) in
            self.currentLocation = location
            LogKit.sharedInstance.sendInfoMessage("got one shot for \(monitoredBL.canonicalName) check : \(location.readableDescription())")
            //check to see if device is moving to fast to generate a nudge
            guard location.speed <= 6.0 else {
                LogKit.sharedInstance.sendErrorMessage("the speed is too fast to generate a nudge")
                self.startMonitoringBusinessLocations()
                self.startBackgroundTask()
                return
            }
            //check accuracy level
            guard location.horizontalAccuracy <= 75.0 else {
                LogKit.sharedInstance.sendErrorMessage("the horizontal accuracy is too high for a nudge")
                self.startMonitoringBusinessLocations()
                self.startBackgroundTask()
                return
            }
            //check the actual distance from the business and the device location
            let distance = location.distance(from: CLLocation(latitude: monitoredBL.location.latitude, longitude: monitoredBL.location.longitude))
            guard distance <= 175 else {
                LogKit.sharedInstance.sendErrorMessage("device too far away to trigger nudge")
                self.startMonitoringBusinessLocations()
                self.startBackgroundTask()
                return
            }
            //close enough to trigger a nudge
            LogKit.sharedInstance.sendInfoMessage("device close enough for nudge to be sent at : \(Date().shortReadableDescription())")
            AnalyticsService.shared.track(AnalyticsEvent.geofenceTriggered)
            self.performNudge(forBusiness: monitoredBL)
            
        }, error: {(request, last, error) in
            //try again on error
            LogKit.sharedInstance.sendErrorMessage("there was an error with the one shot location for region : \(error.localizedDescription)")
            self.oneShotForRegion(monitoredBL)
        })
    }
    
    func startBackgroundTask() {
        if self.backgroundRequest == nil {
            self.setupBackgroundUpdates()
        }
        else {
            LogKit.sharedInstance.sendVerboseMessage("staring background request")
            Location.start(self.backgroundRequest)
        }
    }
    
    func stopBackgroundTask() {
        if self.backgroundRequest != nil {
            LogKit.sharedInstance.sendVerboseMessage("stopping background request")
            Location.pause(self.backgroundRequest)
        }
    }
    
    func setupBackgroundUpdates() {
        
        self.backgroundRequest = Location.getLocation(accuracy: .any, frequency: .significant, success: {(request, recentLocation) in
            self.currentLocation = recentLocation
            //post location update to Analytics
            AnalyticsService.shared.track(AnalyticsEvent.locationUpdated, properties: ["lat" : recentLocation.coordinate.latitude,
                                                                                       "lng" : recentLocation.coordinate.longitude,
                                                                                       "accuracy" : recentLocation.horizontalAccuracy,
                                                                                       "speed" : recentLocation.speed])
            
            //update timestamp
            self.lastLocationUpdate = recentLocation.timestamp
            let description = "got a location update with : \(recentLocation.readableDescription())"
            LogKit.sharedInstance.sendInfoMessage(description)
            //
            self.getNearbyBusinessesForLocation(recentLocation)
            
        }, error: {(request, last, error) in
            if last != nil {
                self.currentLocation = last
            }
        })
        self.backgroundRequest.activity = .fitness
        self.backgroundRequest.minimumDistance = SettingsService.sharedInstance.distanceFilter
    }
    
    func getNearbyBusinessesForLocation(_ location : CLLocation) {
        //get nearby places to monitor
        let latString = String(location.coordinate.latitude)
        let lngString = String(location.coordinate.longitude)
        BusinessService.sharedInstance.fetchNearbyBusinessLocations(latString, lngString, completion: { businessLocations in
            //print("found businesses to add to monitor")
            LogKit.sharedInstance.sendInfoMessage("found \(businessLocations.count) businesses to monitor")
            self.addMonitoredBusinessLocations(businessLocations)
        })
    }
    
    func addMonitoredBusinessLocations(_ businessLocations: [BusinessLocation]) {
        // remove old BLs matching new BLs
        self.monitoredBusinessLocations = self.monitoredBusinessLocations.filter { (oldBL) -> Bool in
            !businessLocations.contains(where: { (newBL) -> Bool in
                oldBL.placeId == newBL.placeId
            })
        }
        // append new BLs, restrict to newest 20, save
        self.monitoredBusinessLocations.append(contentsOf: businessLocations)
        self.monitoredBusinessLocations.removeFirst(max(0, self.monitoredBusinessLocations.count - 20))
        StorageService.sharedInstance.saveMonitoredBusinessLocations()
        
        self.startMonitoringBusinessLocations()
    }
    
    
    func startMonitoringBusinessLocations() {
        //TODO : Need to remove items from pool
        self.stopMonitoringRegionsNotIn(self.monitoredBusinessLocations)
        self.removeOldRegions()
        self.monitoredBusinessLocations.forEach({(monitoredBL) in
            do {
                try Location.monitor(region: monitoredBL.region, enter: {(request) in
                    let message = "you just entered \(monitoredBL.canonicalName) at \(Date().shortReadableDescription())"
                    LogKit.sharedInstance.sendInfoMessage(message)
                    self.oneShotForRegion(monitoredBL)
                }, exit: nil, error: {(request, error) in
                    let message = "there was an error with region monitoring : \(error)"
                    LogKit.sharedInstance.sendErrorMessage(message)
                    request.cancel()
                })
            }
            catch {
                print(error)
                print("there was a problem creating the fence")
            }
        })
    }
    
    func getMonitoredBusinessLocationForRegion(_ region: CLRegion) -> BusinessLocation? {
        return self.monitoredBusinessLocations.first { (monitoredBL) -> Bool in
            monitoredBL.placeId == region.identifier
        }
    }    
    
    func stopMonitoringRegionsNotIn(_ businessLocations: [BusinessLocation]) {
        
        let regions = locationManager.monitoredRegions
        regions.forEach { (region) in
            if !businessLocations.contains(where: { (businessLocation) -> Bool in
                region.identifier == businessLocation.placeId
            }) {
                locationManager.stopMonitoring(for: region)
            }
        }
    }
    
    func removeOldRegions() {
        let regions = locationManager.monitoredRegions
        regions.forEach({ region in
            let businessLocation = self.getMonitoredBusinessLocationForRegion(region)
            if businessLocation?.dateAdded.isToday == false {
                locationManager.stopMonitoring(for: region)
                if let index = self.monitoredBusinessLocations.index(of: businessLocation!) {
                    self.monitoredBusinessLocations.remove(at: index)
                }
                StorageService.sharedInstance.saveMonitoredBusinessLocations()
            }
        })
    }
    
    func stopMonitoringBusinessLocation(_ businessLocation: BusinessLocation) {
        locationManager.stopMonitoring(for: businessLocation.region)
        if let index = self.monitoredBusinessLocations.index(of: businessLocation) {
            self.monitoredBusinessLocations.remove(at: index)
        }
        StorageService.sharedInstance.saveMonitoredBusinessLocations()
    }
    
    
    func performNudge(forBusiness businessLocation : BusinessLocation) {
        LogKit.sharedInstance.sendInfoMessage("starting perform nudge logic @ \(Date().shortReadableDescription())")
        guard !self.isPerformingNudge else {
            LogKit.sharedInstance.sendErrorMessage("The isPeformingNudge was false : \(self.isPerformingNudge)")
            return }
        
        
        guard !self.didPerformRecentNudge() else {
            LogKit.sharedInstance.sendErrorMessage("the didPerformRecent came back false : \(self.didPerformRecentNudge())")
            return }
        guard !self.alreadyVisitedToday(businessLocation) else {
            LogKit.sharedInstance.sendErrorMessage("the alreadyVisitedToday came back true for \(businessLocation.canonicalName)")
            return }
        
        self.isPerformingNudge = true
        
        LogKit.sharedInstance.sendInfoMessage("fetching alternatives for \(businessLocation.canonicalName)")
        BusinessService.sharedInstance.fetchAlternativeBusinessLocations(businessLocation) { scoredAlternatives, unscoredAlternatives in
            LogKit.sharedInstance.sendInfoMessage("alternatives found creating nudge")
            let nudge = Nudge(latitude: businessLocation.latitude,
                              longitude: businessLocation.longitude,
                              businessLocation: businessLocation,
                              status: .created,
                              alternatives: scoredAlternatives + unscoredAlternatives,
                              createdAt: Date())
            
            NudgeService.sharedInstance.createNudge(nudge) { (success, nudge) in
                guard success else {
                    LogKit.sharedInstance.sendErrorMessage("there was a problem creating the nudge")
                    self.isPerformingNudge = false
                    return
                }
                
                // save to list of past nudges
                StorageService.sharedInstance.addNudge(nudge)
                LogKit.sharedInstance.sendInfoMessage("nudge created and saved in storage")
                //TODO : need to stop the monitor
                self.stopMonitoringBusinessLocation(businessLocation)
                self.isPerformingNudge = false
                
                // list complete send to appropiate process
                LogKit.sharedInstance.sendDebugMessage("this is when the nudge should show")
                NotificationService.sharedInstance.presentNudgeNotification(nudge) { (nudge) in
                    nudge.status = .sent
                    StorageService.sharedInstance.saveNudges()
                }
            }
            
        }
        
    }
    
    func alreadyVisitedToday(_ businessLocation: BusinessLocation) -> Bool {
        let nudges = StorageService.sharedInstance.nudges
        let alreadyVisitedToday = nudges.contains(where: { (nudge) -> Bool in
            let placeIdMatches = nudge.businessLocation.placeId == businessLocation.placeId
            let canonicalNameMatches = nudge.businessLocation.canonicalName == businessLocation.canonicalName
            //let locationMatches = nudge.businessLocation.latitude == businessLocation.latitude && nudge.businessLocation.longitude == businessLocation.longitude
            let nudgedToday = nudge.createdAt.isToday
            //return (placeIdMatches || canonicalNameMatches || locationMatches) && nudgedToday
            return (placeIdMatches || canonicalNameMatches) && nudgedToday
        })
        return alreadyVisitedToday
    }
    
    func didPerformRecentNudge() -> Bool {
        let nudges = StorageService.sharedInstance.nudges
        if nudges.count > 0 {
            let lastNudge = nudges[0]
            let today = Date()
            let difference = today - lastNudge.createdAt
            let minutes = difference.in(.minute)!
            LogKit.sharedInstance.sendDebugMessage("it's been \(minutes) since the last nudge")
            //let timeDiff = lastNudge.createdAt.timeIntervalSince(Date())
            //let minutes = timeDiff.truncatingRemainder(dividingBy: 60)
            return minutes < 5
        }
        return false
    }
}
*/
