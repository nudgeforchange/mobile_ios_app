//
//  LogKit.swift
//  Nudge
//
//  Created by Thanos on 10/26/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation
import SwiftyBeaver

class LogKit {
    static let sharedInstance = LogKit()
    let log = SwiftyBeaver.self
    
    func setupLogging() {
        let platform = SBPlatformDestination(appID: "9GzdLM", appSecret: "qvw1sRqHexWknulz4EolzqoIll3lrd0v", encryptionKey: "lxugUyr680TtgfdcmdrbraoyrxbavmUb")
        platform.sendingPoints.threshold = 1
        self.log.addDestination(platform)
    }
    
    func sendVerboseMessage(_ message : String) {
        //log.verbose(message)
    }
    
    func sendDebugMessage(_ message : String) {
        //log.debug(message)
    }
    
    func sendInfoMessage(_ message : String) {
       //log.info(message)
    }
    
    func sendWarningMessage(_ message : String) {
        //log.warning(message)
    }
    
    func sendErrorMessage(_ message : String) {
        //log.error(message)
    }
}
