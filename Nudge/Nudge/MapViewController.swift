//
//  MapViewController.swift
//  Nudge
//
//  Created by Thanos on 8/10/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import MapKit
import AudioToolbox

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    let fenceArray = StorageService.sharedInstance.monitoredBusinessLocations
    var selectedLocation : CLLocationCoordinate2D?
    var isScanning : Bool = false
    
    var scannedBusinessLocations : [BusinessLocation] = [] {
        didSet {
            scannedBusinessLocations.forEach({location in
                let overlay = MKCircle(center: location.location, radius: location.geofenceRadius)
                overlay.title = "scan"
                self.mapView.add(overlay)
            })
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //setup navigation bar and button
        self.title = "Map"
        let closeButton = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(MapViewController.closeScreen))
        self.navigationItem.leftBarButtonItem = closeButton
        let scanButton = UIBarButtonItem(title: "Scan", style: .done, target: self, action: #selector(MapViewController.scanArea))
        self.navigationItem.rightBarButtonItem = scanButton
        let colors = [AppColors.nudgeRedColor, AppColors.nudgeOragneColor]
        let gradient = UIColor(gradientStyle: .topToBottom, withFrame: (self.navigationController?.navigationBar.frame)!, andColors: colors)
        self.navigationController?.navigationBar.backgroundColor = gradient
        //setup map view
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        self.mapView.isZoomEnabled = true
        self.mapView.mapType = .standard
        self.setupCurrentLocation()
        //draw the stored geofences
        var overlays : [MKOverlay] = []
        for location in fenceArray {
            let circle = MKCircle(center: location.location, radius: location.geofenceRadius)
            circle.title = "bad"
            overlays.append(circle)
        }
        self.mapView.addOverlays(overlays)
        //add touch interaction to map
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(MapViewController.longPressOnMap(sender:)))
        longPressGesture.minimumPressDuration = 2
        self.mapView.addGestureRecognizer(longPressGesture)
        //setup the table view
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCurrentLocation() {
        if let location = BusinessLocationVisitService.sharedInstance.currentLocation {
        //LocationService.sharedInstance.getCurrentLocation(completion: {location in
            self.selectedLocation = location.coordinate
            let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
            self.mapView.setRegion(region, animated: true)
        //})
        }
    }
    
    func longPressOnMap(sender : UILongPressGestureRecognizer) {
        if sender.state == .ended {
            let touchLocation = sender.location(in: self.mapView)
            let location = self.mapView.convert(touchLocation, toCoordinateFrom: self.mapView)
            self.selectedLocation = location
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            return
        }
    }
    
    @IBAction func goToPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Enter Coordinates", message: "", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { latTextField in
            latTextField.placeholder = "latitude"
        })
        alert.addTextField(configurationHandler: { lngTextField in
            lngTextField.placeholder = "longitude"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let confirmAction = UIAlertAction(title: "Go", style: .default, handler: { action in
            let latTextField = alert.textFields![0] as UITextField
            let lngTextField = alert.textFields![1] as UITextField
            if latTextField.text != nil || lngTextField.text != nil {
                let location = CLLocationCoordinate2DMake(Double(latTextField.text!)!, Double(lngTextField.text!)!)
                self.selectedLocation = location
                var region = self.mapView.region
                region.center = location
                self.mapView.setRegion(region, animated: true)
            }
        })
        alert.addAction(confirmAction)
        self.present(alert, animated: true, completion: nil)
    }
    func closeScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func clearOldScanOverlays() {
        let overlays = self.mapView.overlays
        overlays.forEach({overlay in
            if overlay.title! == "scan" {
                self.mapView.remove(overlay)
            }
        })
    }
    
    func scanArea() {
        if selectedLocation != nil {
            self.clearOldScanOverlays()
            let lat = selectedLocation?.latitude
            let lng = selectedLocation?.longitude
            let latString = String(lat!)
            let lngString = String(lng!)
            print("starting scan for places")
            BusinessService.sharedInstance.fetchNearbyBusinessLocations(latString, lngString, completion: { businessLocations in
                let message = "found \(businessLocations.count) places"
                NotificationService.sharedInstance.presentNotification(title: "Map Search", message: message, identifier: "map_demo", userInfo: nil, debug: true, completion: {})
                if businessLocations.count > 0 {
                    self.scannedBusinessLocations = businessLocations
                }
            })
        }
    }
}

extension MapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKCircleRenderer(overlay: overlay)
        if overlay.title! == "bad" {
            renderer.fillColor = AppColors.nudgeOragneColor.withAlphaComponent(0.2)
            renderer.strokeColor = AppColors.nudgeOragneColor.withAlphaComponent(0.7)
        }
        else if overlay.title! == "good" {
            renderer.fillColor = AppColors.nudgeGreenColor.withAlphaComponent(0.2)
            renderer.strokeColor = AppColors.nudgeGreenColor.withAlphaComponent(0.7)
        }
        else {
            renderer.fillColor = AppColors.nudgeBlueColor.withAlphaComponent(0.2)
            renderer.strokeColor = AppColors.nudgeBlueColor.withAlphaComponent(0.7)
        }
        renderer.lineWidth = 2
        return renderer
        
    }
}

extension MapViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var region = self.mapView.region
        if indexPath.section == 0 {
            let fence = fenceArray[indexPath.row]
            region.center = fence.location
        }
        else {
            let scannedBusiness = self.scannedBusinessLocations[indexPath.row]
            region.center = scannedBusiness.location
        }
        self.mapView.setRegion(region, animated: true)
    }
    
}

extension MapViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return fenceArray.count
        }
        else {
            return self.scannedBusinessLocations.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Stored Fences"
        }
        else {
            return "Scanned Fences"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if indexPath.section == 0 {            
            let fence = fenceArray[indexPath.row]
            cell?.textLabel?.text = fence.canonicalName
            cell?.detailTextLabel?.text = fence.dateAdded.shortReadableDescription()
        }
        else {
            let scannedBusiness = self.scannedBusinessLocations[indexPath.row]
            cell?.textLabel?.text = scannedBusiness.canonicalName
            cell?.detailTextLabel?.text = "\(scannedBusiness.distance) miles away"
        }
        return cell!
    }
}
