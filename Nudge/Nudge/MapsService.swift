import Foundation

class MapsService: NSObject {
    
    static func openMapApp(address: String) {
        AnalyticsService.shared.track(AnalyticsEvent.openedMapApp)
        
        var mapURL: URL!
        let urlAddress = address.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        //open the address in Google maps if available
        if UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL) {
            mapURL = URL(string: "comgooglemaps://?daddr=\(urlAddress!)")!
        } else {
            mapURL = URL(string: "http://maps.apple.com/?address=\(urlAddress!)")!
        }
        UIApplication.shared.open(mapURL, options: [:], completionHandler: nil)
    }
    
    static func openMapApp(name: String, latitude: Double, longitude: Double) {
        AnalyticsService.shared.track(AnalyticsEvent.openedMapApp)
        
        var mapURL: URL!
        let urlName = name.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        if UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL) {
            mapURL = URL(string: "comgooglemaps://?daddr=\(latitude),\(longitude)")
        } else {
            mapURL = URL(string: "http://maps.apple.com/?q=\(urlName!)&ll=\(latitude),\(longitude)")!
        }
        UIApplication.shared.open(mapURL, options: [:], completionHandler: nil)
    }

}
