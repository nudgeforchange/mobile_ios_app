//
//  MenuHeaderView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 4/11/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class MenuHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var secretView: UIView!

}
