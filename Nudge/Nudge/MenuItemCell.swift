//
//  MenuItemCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 4/11/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class MenuItemCell: UITableViewCell {
    
    @IBOutlet weak var topRoundedShadowView: RoundedShadowView!
    @IBOutlet weak var middleRoundedShadowView: RoundedShadowView!
    @IBOutlet weak var bottomRoundedShadowView: RoundedShadowView!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    fileprivate var visibleRoundedShadowView: RoundedShadowView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.backgroundColor = UIColor.clear
        self.visibleRoundedShadowView = self.middleRoundedShadowView
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        self.visibleRoundedShadowView.backgroundColor = selected ? UIColor(white: 0.95, alpha: 1) : UIColor.white
    }
    
    func setTop() {
        self.topRoundedShadowView.isHidden = false
        self.middleRoundedShadowView.isHidden = true
        self.bottomRoundedShadowView.isHidden = true
        self.visibleRoundedShadowView = self.topRoundedShadowView
    }
    
    func setMiddle() {
        self.topRoundedShadowView.isHidden = true
        self.middleRoundedShadowView.isHidden = false
        self.bottomRoundedShadowView.isHidden = true
        self.visibleRoundedShadowView = self.middleRoundedShadowView
    }
    
    func setBottom() {
        self.topRoundedShadowView.isHidden = true
        self.middleRoundedShadowView.isHidden = true
        self.bottomRoundedShadowView.isHidden = false
        self.visibleRoundedShadowView = self.bottomRoundedShadowView
    }
    
}
