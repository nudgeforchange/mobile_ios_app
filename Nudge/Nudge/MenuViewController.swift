//
//  MenuViewController.swift
//  Nudge
//
//  Created by Magneto on 3/13/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var statusBarBackgroundView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var colorTop: NSLayoutConstraint!
    
    let menuTitles : [String] = ["Do Not Nudge List",
                                 "Provide Feedback",
                                 "Reset Value Questions",
                                 "Request Research",
                                 "Donate",
                                 "Privacy Policy",
                                 "Terms of Service",
                                 "System Settings"]
    let menuImages : [UIImage] = [UIImage(named: "ClipboardIcon")!,
                                  UIImage(named: "FeedbackIcon")!,
                                  UIImage(named: "ResetIcon")!,
                                  UIImage(named: "SearchIcon")!,
                                  UIImage(named: "icons8-receive_cash_filled")!,
                                  UIImage(named: "PrivacyIcon")!,
                                  UIImage(named: "icon_terms")!,
                                  UIImage(named: "small_gear")!]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gradientColor(height: CGFloat) -> UIColor {
        let colors = [AppColors.nudgeYellowColor, AppColors.nudgePurpleColor]
        return UIColor(gradientStyle: .topToBottom, withFrame: CGRect(x: 0, y: 0, width: self.colorView.bounds.size.width, height: height), andColors: colors)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.statusBarBackgroundView.backgroundColor = AppColors.nudgeYellowColor
        self.colorView.backgroundColor = AppColors.nudgePurpleColor
        self.view.insertSubview(self.colorView, at: 0)
        
        let menuHeaderViewNib: UINib = UINib(nibName: "MenuHeaderView", bundle: nil)
        self.tableView.register(menuHeaderViewNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        
        let menuItemCellNib = UINib(nibName: "MenuItemCell", bundle: nil)
        tableView.register(menuItemCellNib, forCellReuseIdentifier: "MenuItemCell")
        
        let spacerNib: UINib = UINib(nibName: "ScoredBusinessSpacerCell", bundle: nil)
        tableView.register(spacerNib, forCellReuseIdentifier: "ScoredBusinessSpacerCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.alignColorView(self.tableView.contentOffset.y)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alignColorView(_ yOffset: CGFloat) {
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        self.colorTop.constant = headerRect.maxY - yOffset
    }
    
    func showDemoAlert() {
        
        let alert = UIAlertController(title: "Show Demo", message: "Use this to showcase how Nudges work", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        let showNowAction = UIAlertAction(title: "Show Now", style: .default, handler: {(action) -> Void in
            //print("show it now")
            self.presentDemo()
            
        })
        alert.addAction(showNowAction)
        let scheduleAction = UIAlertAction(title: "Schedule in 10 seconds", style: .default, handler: {(action) -> Void in
            //print("show it later")
            Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(MenuViewController.presentDemo), userInfo: nil, repeats: false)
        })
        alert.addAction(scheduleAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showMapScreen(gesture : UILongPressGestureRecognizer) {
        if gesture.state == .ended {
            let storyboard = UIStoryboard(name: "MapViewController", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "MapViewController")
            let navCon = UINavigationController(rootViewController: viewController)
            self.present(navCon, animated: true, completion: nil)
        }
    }
    
    func presentDemo() {
        presentDemoNudge(createDemoNudge())
    }
    
    func showQuestionaire() {
        let storyBoard = UIStoryboard(name: "QuestionsViewController", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionsViewController
        viewController.modalPresentationCapturesStatusBarAppearance = true
        viewController.userReset = true
        //setup the navigation controller
        let navCon = UINavigationController(rootViewController: viewController)
        navCon.isNavigationBarHidden = true
        self.present(navCon, animated: true, completion: nil)
    }
    
    func openSettingsApp() {
        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)!
        UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
    }
}

extension MenuViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuTitles.count * 2 - 1
    }
    
    func menuItemCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuItemCell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell", for: indexPath) as! MenuItemCell
        cell.itemLabel.text = self.menuTitles[indexPath.row/2]
        cell.iconImageView.image = self.menuImages[indexPath.row/2]
        
        switch indexPath.row {
        case 0:
            cell.setTop()
        case self.menuTitles.count * 2 - 2:
            cell.setBottom()
        default:
            cell.setMiddle()
        }
        return cell
    }
    
    func spacerCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ScoredBusinessSpacerCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessSpacerCell", for: indexPath) as! ScoredBusinessSpacerCell
        cell.colorView.backgroundColor = UIColor(hexString: "E4E4E4")
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            return spacerCell(tableView: tableView, indexPath: indexPath)
        }
        
        return menuItemCell(tableView: tableView, indexPath: indexPath)
    }
    
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: MenuHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! MenuHeaderView
        headerView.colorView.backgroundColor = self.gradientColor(height: 44)
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.shadowRadius = 10
        headerView.layer.shadowOpacity = 0
        
        //add the secret tap code
        headerView.secretView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.showDemoAlert))
        tapGesture.numberOfTapsRequired = 5
        let holdGesture = UILongPressGestureRecognizer(target: self, action: #selector(MenuViewController.showMapScreen(gesture:)))
        holdGesture.minimumPressDuration = 2.0
        headerView.secretView.gestureRecognizers = [tapGesture, holdGesture]
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row % 2 == 1 ? 0.5 : 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row % 2 == 0 else { return }
        
        let selectedRow = indexPath.row/2
        switch selectedRow {
        case 0:
            let storyboard = UIStoryboard(name: "NoNudgeViewController", bundle: nil)
            let noNudgeVC = storyboard.instantiateViewController(withIdentifier: "NoNudgeVC") as! NoNudgeViewController
            self.navigationController?.pushViewController(noNudgeVC, animated: true)
        case 1, 3:
            let storyBoard = UIStoryboard(name: "WebViewController", bundle: nil)
            let webVC = storyBoard.instantiateViewController(withIdentifier: "WebVC") as! WebViewController
            webVC.titleText = self.menuTitles[selectedRow]
            webVC.url = "https://docs.google.com/forms/d/1apBPRJIpC_Bu_sZAFq8GAK6zLRLtKSk7dGrvcvzIw7s/viewform"
            self.navigationController?.pushViewController(webVC, animated: true)
        case 2:
            // TODO: make sure Nudge alerts don't interfere with this
            //show alert first to ask if user is sure they want to reset their questions
            let title = "Reset Questions"
            let message = "Are you sure you want to reset your value questions?"
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let resetAction = UIAlertAction(title: "Reset", style: .destructive, handler: { (resetAction) in
                if let indexPath = tableView.indexPathForSelectedRow {
                    tableView.deselectRow(at: indexPath, animated: true)
                }
                StorageService.sharedInstance.monitoredBusinessLocations = []
                StorageService.sharedInstance.saveMonitoredBusinessLocations()
                //reset the questionaire flag
                let defaults = UserDefaults.standard
                defaults.removeObject(forKey: AppStrings.questionaireBool)
                defaults.removeObject(forKey: AppStrings.savedBusinessLocationsKey)
                defaults.synchronize()
                //show the questionaire
                self.showQuestionaire()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (cancelAction) in
                if let indexPath = tableView.indexPathForSelectedRow {
                    tableView.deselectRow(at: indexPath, animated: true)
                }
            })
            alert.addAction(resetAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        case 4:
            print("donate button pressed")
        case 5:
            let storyBoard = UIStoryboard(name: "WebViewController", bundle: nil)
            let webVC = storyBoard.instantiateViewController(withIdentifier: "WebVC") as! WebViewController
            webVC.titleText = self.menuTitles[selectedRow]
            webVC.url = "https://nudgeforchange.com/privacy/?app"
            self.navigationController?.pushViewController(webVC, animated: true)
        case 6:
            let storyBoard = UIStoryboard(name: "WebViewController", bundle: nil)
            let webVC = storyBoard.instantiateViewController(withIdentifier: "WebVC") as! WebViewController
            webVC.titleText = self.menuTitles[selectedRow]
            webVC.url = "https://nudgeforchange.com/terms/?app"
            self.navigationController?.pushViewController(webVC, animated: true)
        case 7:
            self.openSettingsApp()
            tableView.deselectRow(at: indexPath, animated: true)
        default:
            break
        }
    }
}

extension MenuViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        alignColorView(yOffset)
        
        // fade color view logic
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        let firstCellRect = self.tableView.rectForRow(at: IndexPath(row: 0, section: 0))
        let firstCellObscurage = yOffset + headerRect.height - firstCellRect.origin.y
        self.colorView.alpha = 1.0 - max(0, min(1, firstCellObscurage/50.0))
        
        // shadow logic
        if let headerView = self.tableView.headerView(forSection: 0) {
            headerView.layer.shadowOpacity = (Float(1.0) - Float(colorView.alpha))/Float(5.0)
        }
        
        //        }
        
        //        // overscrolling logic
        //        let currentY = scrollView.contentOffset.y
        //        if currentY > lastY {
        //            //"scrolling down"
        //            tableView.bounces = true
        //        } else {
        //            //"scrolling up"
        //            // Check that we are not in bottom bounce
        //            if scrollView.contentInset.top >= 0 {
        //                tableView.bounces = false
        //            }
        //        }
        //        lastY = scrollView.contentOffset.y
    }
    
}
