//
//  MissedQuestionViewController.swift
//  Nudge
//
//  Created by Ultron on 5/17/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import Koloda
import pop

class MissedQuestionViewController: UIViewController {

    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var disagreeButton: UIButton!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var neutralButton: UIButton!
    
    
    let systemDefaults = UserDefaults.standard
    var cardArray : [String] = []
    var beliefKeyArray : [String] = []
    var beliefs: [String : Int] = [String : Int]()
    let frameAnimationSpringBounciness: CGFloat = 9
    let frameAnimationSpringSpeed: CGFloat = 16
    let kolodaCountOfVisibleCards = 1
    let kolodaAlphaValueSemiTransparent: CGFloat = 0.1
    
    var boolWelcomeDeleted = true
    var boolNotificationDeleted = true
    var userReset : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //setup Stepper
        //self.setupStepper()
        //add the koloda view datasource and delegate
        kolodaView.backgroundColor = UIColor.clear
        kolodaView.delegate = self
        kolodaView.dataSource = self
        //set the background color and add the orange header color
        self.setBackgroundColors()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func disagreeButtonPressed(_ sender: Any) {
        //get the current index
        self.saveScoreForIndex(index: self.kolodaView.currentCardIndex, score: BeliefScores.NO)
        //mimic swipe action
        self.kolodaView.swipe(.left)
    }
    
    @IBAction func neutralButtonPressed(_ sender: Any) {
        self.saveScoreForIndex(index: self.kolodaView.currentCardIndex, score: BeliefScores.SKIP)
        self.kolodaView.swipe(.down)
    }
    @IBAction func agreeButtonPressed(_ sender: Any) {
        self.saveScoreForIndex(index: self.kolodaView.currentCardIndex, score: BeliefScores.YES)
        kolodaView.swipe(.right)
    }
    
    //MARK : Score Saving
    func saveScoreForIndex(index : Int, score : BeliefScores) {
        //get the key string
        let keyString = self.beliefKeyArray[index]
        self.beliefs[keyString] = score.rawValue
    }
    
}

extension MissedQuestionViewController : KolodaViewDataSource, KolodaViewDelegate {
    //MARK: Koloda View Delegate/Datasource
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        //UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.cardArray.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let cardName : String = self.cardArray[index]
        let imageView = UIImageView(image: UIImage(named: cardName))
        imageView.contentMode = .scaleAspectFit
        //imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return imageView
    }
    
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        
    }
    
    func kolodaSwipeThresholdRatioMargin(_ koloda: KolodaView) -> CGFloat? {
        return 0.75
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("CustomOverlayView", owner: self, options: nil)?[0] as? OverlayView
        
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right, .down]
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        if direction == .left {
            self.saveScoreForIndex(index: index, score: BeliefScores.NO)
        }
        else if direction == .right {
            self.saveScoreForIndex(index: index, score: BeliefScores.YES)
        }
        else if direction == .down {
            self.saveScoreForIndex(index: index, score: BeliefScores.SKIP)
        }
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        //AnalyticsService.shared.track(AnalyticsEvent.onboardingBeliefsSet)
        //add the new scores to the existing scores
        if var existingBeliefs = SettingsService.sharedInstance.beliefs {
            for belief in beliefs {
                existingBeliefs[belief.key] = belief.value
            }
            SettingsService.sharedInstance.beliefs = existingBeliefs
            SettingsService.sharedInstance.didCompleteOnboarding = true
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }

}
