import Foundation

enum NoNudgeWhitelistReason: String {
    case guiltyPleasure = "guilty_pleasure"
    case nearCommonPlace = "near_common_place"
    case scoreDisagreement = "score_disagreement"
    case erroneous = "erroneous"
    case other = "other"
}

enum NoNudgeBlacklistReason: String {
    case notAFan = "not_a_fan"
    case nearCommonPlace = "near_common_place"
    case scoreDisagreement = "score_disagreement"
    case erroneous = "erroneous"
    case other = "other"
}

class NoNudge: NSObject {
    
    var businessUUID: String?
    var businessLocationUUID: String?
    var name: String!
    var address: String?
    
    init(businessDictionary: [String : AnyObject]) {
        self.businessUUID = businessDictionary["business_uuid"] as? String
        self.name = businessDictionary["name"] as! String
    }
    
    init(businessLocationDictionary: [String : AnyObject]) {
        self.businessLocationUUID = businessLocationDictionary["business_location_uuid"] as? String
        self.name = businessLocationDictionary["business"]!["name"] as! String
        self.address = businessLocationDictionary["place"]?["formatted_address"] as? String
    }
    
}

extension NoNudge: Comparable {
    static func < (lhs: NoNudge, rhs: NoNudge) -> Bool {
        return lhs.name < rhs.name
    }
    
    static func > (lhs: NoNudge, rhs: NoNudge) -> Bool {
        return lhs.name > rhs.name
    }
    
    static func == (lhs: NoNudge, rhs: NoNudge) -> Bool {
        return lhs.name == rhs.name
    }
    
    
}
