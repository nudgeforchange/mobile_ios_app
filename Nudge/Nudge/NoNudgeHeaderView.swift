//
//  NoNudgeHeaderView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 5/21/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class NoNudgeHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var secretView: UIView!

}
