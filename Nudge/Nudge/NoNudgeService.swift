import Alamofire

class NoNudgeService: APIService {
    
    static let sharedInstance = NoNudgeService()
    
    func businessParams(businessUUID: String, reason: String) -> Parameters {
        let params: Parameters = [
            "business": [
                "business_uuid": businessUUID
            ],
            "reason": reason.lowercased()
        ]
        return params
    }
    
    func businessLocationParams(businessLocationUUID: String, reason: String) -> Parameters {
        let params: Parameters = [
            "business_location": [
                "business_location_uuid": businessLocationUUID
            ],
            "reason": reason.lowercased()
        ]
        return params
    }
    
    func whitelistBusiness(businessUUID: String, reason: NoNudgeWhitelistReason, completion: @escaping () -> Void) {
        let urlString = "\(APIService.baseURL)/client/whitelisted_businesses/"
        let params = businessParams(businessUUID: businessUUID, reason: reason.rawValue)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
                    completion()
                    return
                }
                
                if response.result.value != nil {
                    //let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    completion()
                }
            }
        }
    }
    
    func whitelistBusinessLocation(businessLocationUUID: String, reason: NoNudgeWhitelistReason, completion: @escaping () -> Void) {
        let urlString = "\(APIService.baseURL)/client/whitelisted_business_locations/"
        let params = businessLocationParams(businessLocationUUID: businessLocationUUID, reason: reason.rawValue)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
                    completion()
                    return
                }
                
                if response.result.value != nil {
                    //let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    completion()
                }
            }
        }
    }
    
    func fetchWhitelistedBusinesses(completion: @escaping (_ whiltelistedBusinesses: [NoNudge]) -> Void) {
        let urlString = "\(APIService.baseURL)/client/whitelisted_businesses/"
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
//                    completion()
                    return
                }
                
                if let result = response.result.value {
                    let JSON = result as! [[String : AnyObject]]
                    let businesses = JSON.map({ (businessJSON) -> NoNudge in
                        return NoNudge(businessDictionary: businessJSON["business"] as! [String : AnyObject])
                    })
                    //print(result)
                    completion(businesses)
                }
            }
        }
    }
    
    func fetchWhitelistedBusinessLocations(completion: @escaping (_ whitelistedBusinessLocations: [NoNudge]) -> Void) {
        let urlString = "\(APIService.baseURL)/client/whitelisted_business_locations/"
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
//                    completion()
                    return
                }
                
                if let result = response.result.value {
                    let JSON = result as! [[String : AnyObject]]
                    let businessLocations = JSON.map({ (businessLocationJSON) -> NoNudge in
                        return NoNudge(businessLocationDictionary: businessLocationJSON["business_location"] as! [String : AnyObject])
                    })
                    //print(result)
                    completion(businessLocations)
                }
            }
        }
    }
    
    func deleteWhitelistedBusiness(businessUUID: String, completion: @escaping () -> Void) {
        let urlString = "\(APIService.baseURL)/client/whitelisted_businesses/\(businessUUID)/"
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .delete, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
                    completion()
                    return
                }
                
                if response.result.value != nil {
//                    let JSON = result as! [String : AnyObject]
                    //print(result)
                    completion()
                }
            }
        }
    }
    
    func deleteWhitelistedBusinessLocation(businessLocationUUID: String, completion: @escaping () -> Void) {
        let urlString = "\(APIService.baseURL)/client/whitelisted_business_locations/\(businessLocationUUID)/"
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .delete, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
                    completion()
                    return
                }
                
                if response.result.value != nil {
//                    let JSON = result as! [String : AnyObject]
                    //print(result)
                    completion()
                }
            }
        }
    }
    
    func blacklistBusiness(businessUUID: String, reason: NoNudgeBlacklistReason, completion: @escaping () -> Void) {
        let urlString = "\(APIService.baseURL)/client/blacklisted_businesses/"
        let params = businessParams(businessUUID: businessUUID, reason: reason.rawValue)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
                    completion()
                    return
                }
                
                if response.result.value != nil {
                    //let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    completion()
                }
            }
        }
    }
    
    func blacklistBusinessLocation(businessLocationUUID: String, reason: NoNudgeBlacklistReason, completion: @escaping () -> Void) {
        let urlString = "\(APIService.baseURL)/client/blacklisted_business_locations/"
        let params = businessLocationParams(businessLocationUUID: businessLocationUUID, reason: reason.rawValue)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    //print(response.response!)
                    completion()
                    return
                }
                
                if response.result.value != nil {
                    //let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    completion()
                }
            }
        }
    }
    
    func fetchBlacklist(completion: @escaping () -> Void) {
        
    }
    
}
