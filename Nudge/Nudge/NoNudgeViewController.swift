//
//  NoNudgeViewController.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 5/21/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class NoNudgeViewController: UIViewController {
    
    @IBOutlet weak var statusBarBackgroundView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var colorTop: NSLayoutConstraint!
    
    var noNudgeItems: [NoNudge] = [NoNudge]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gradientColor(height: CGFloat) -> UIColor {
        let colors = [AppColors.nudgeRedColor, AppColors.nudgeOragneColor]
        return UIColor(gradientStyle: .topToBottom, withFrame: CGRect(x: 0, y: 0, width: self.colorView.bounds.size.width, height: height), andColors: colors)
    }
    
    func alignColorView(_ yOffset: CGFloat) {
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        self.colorTop.constant = headerRect.maxY - yOffset
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.statusBarBackgroundView.backgroundColor = AppColors.nudgeRedColor
        self.colorView.backgroundColor = AppColors.nudgeOragneColor
        self.view.insertSubview(self.colorView, at: 0)
        
        let headerViewNib: UINib = UINib(nibName: "NoNudgeHeaderView", bundle: nil)
        self.tableView.register(headerViewNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateList()
    }
    
    func updateList() {
        NoNudgeService.sharedInstance.fetchWhitelistedBusinesses { (whitelistedBusinesses: [NoNudge]) in
            
            NoNudgeService.sharedInstance.fetchWhitelistedBusinessLocations { (whitelistedBusinessLocations: [NoNudge]) in
                
                let noNudgeItems: [NoNudge] = whitelistedBusinesses + whitelistedBusinessLocations
                self.noNudgeItems = noNudgeItems.sorted()
                self.tableView.reloadData()
            }
        }
    }
    
    func backButtonPressed(_ sender: UIButton?) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}

extension NoNudgeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("no nudge items: \(self.noNudgeItems.count)")
        return self.noNudgeItems.count
    }
    
    func noNudgeItemCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let noNudgeItem = self.noNudgeItems[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoNudgeCell", for: indexPath)
        cell.textLabel?.font = UIFont(name: AppFontNames.regularFont, size: 17)
        cell.detailTextLabel?.font = UIFont(name: AppFontNames.regularFont, size: 10)
        cell.detailTextLabel?.textColor = AppColors.darkTextColor
        cell.textLabel?.text = noNudgeItem.name
        if let address = noNudgeItem.address {
            cell.detailTextLabel?.text = address
        } else {
            cell.detailTextLabel?.text = nil
        }
        return cell
    }
    
    func spacerCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ScoredBusinessSpacerCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessSpacerCell", for: indexPath) as! ScoredBusinessSpacerCell
        cell.colorView.backgroundColor = UIColor(hexString: "E4E4E4")
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return noNudgeItemCell(tableView: tableView, indexPath: indexPath)
    }
    
}

extension NoNudgeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: NoNudgeHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! NoNudgeHeaderView
        headerView.colorView.backgroundColor = self.gradientColor(height: 44)
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.shadowRadius = 10
        headerView.layer.shadowOpacity = 0
        
        headerView.backButton.removeTarget(nil, action: nil, for: .allEvents)
        headerView.backButton.addTarget(self, action: #selector(backButtonPressed(_:)), for: .touchUpInside)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            let deletedItem = self.noNudgeItems.remove(at: indexPath.row)
            if let businessUUID = deletedItem.businessUUID {
                NoNudgeService.sharedInstance.deleteWhitelistedBusiness(businessUUID: businessUUID, completion: {
                    
                })
            } else if let businessLocationUUID = deletedItem.businessLocationUUID {
                NoNudgeService.sharedInstance.deleteWhitelistedBusinessLocation(businessLocationUUID: businessLocationUUID, completion: { 
                    
                })
            }
            tableView.endUpdates()
        }
    }
    
}

extension NoNudgeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        alignColorView(yOffset)
        
        // fade color view logic
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        let firstCellRect = self.tableView.rectForRow(at: IndexPath(row: 0, section: 0))
        let firstCellObscurage = yOffset + headerRect.height - firstCellRect.origin.y
        self.colorView.alpha = 1.0 - max(0, min(1, firstCellObscurage/50.0))
        
        // shadow logic
        if let headerView = self.tableView.headerView(forSection: 0) {
            headerView.layer.shadowOpacity = (Float(1.0) - Float(colorView.alpha))/Float(5.0)
        }
        
        //        }
        
        //        // overscrolling logic
        //        let currentY = scrollView.contentOffset.y
        //        if currentY > lastY {
        //            //"scrolling down"
        //            tableView.bounces = true
        //        } else {
        //            //"scrolling up"
        //            // Check that we are not in bottom bounce
        //            if scrollView.contentInset.top >= 0 {
        //                tableView.bounces = false
        //            }
        //        }
        //        lastY = scrollView.contentOffset.y
    }
    
}
