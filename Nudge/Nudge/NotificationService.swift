import UserNotifications
import Alamofire

class NotificationService: NSObject {
    
    static let sharedInstance = NotificationService()
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    private override init() {
        super.init()
        self.notificationCenter.delegate = self
    }
    
    func setup() {
        self.setActions()
    }
    
    private func setActions() {
        let moreInfo = UNNotificationAction(
            identifier: "moreInfo",
            title: "More info",
            options: [.foreground]
        )
        let dismiss = UNNotificationAction(
            identifier: UNNotificationDismissActionIdentifier,
            title: "Dismiss",
            options: [.destructive]
        )
        let category = UNNotificationCategory(
            identifier: AppStrings.alertCategory,
            actions: [moreInfo, dismiss],
            intentIdentifiers: []
        )
        
        notificationCenter.setNotificationCategories([category])
    }
    
    func presentNotification(title: String, message: String, identifier: String, userInfo: [AnyHashable : Any]?, debug: Bool, completion: @escaping () -> Void) {
        //        if debug {
        //            print(title + "\n" + message + "\n" + identifier)
        //            return
        //        }
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        if let userInfo = userInfo {
            content.userInfo = userInfo
        }
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = AppStrings.alertCategory
        let localRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: nil)
        self.notificationCenter.add(localRequest, withCompletionHandler: nil)
        //update badge count
        if UIApplication.shared.applicationState == .background {
            BadgeManager.sharedInstance.updateBadgeCount()
        }
        completion()
    }
    
    func showDebugNotification(message : String) {
        let identifier = UUID().uuidString
        let content = UNMutableNotificationContent()
        content.title = "Debugger Notification"
        content.body = message
        content.sound = UNNotificationSound.default()
        let localRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: nil)
        self.notificationCenter.add(localRequest, withCompletionHandler: nil)
    }
    
    func presentNudgeNotification(_ nudge: Nudge, completion: @escaping (_ nudge: Nudge) -> Void) {
        AnalyticsService.shared.track(AnalyticsEvent.nudgeNotificationSent, properties: ["type" : UIApplication.shared.applicationState == .active ? "active" : "background"])
        var title : String
        var message : String
        
        
        title = nudge.businessLocation.canonicalName + "?"
        message = "They have a low \(nudge.businessLocation.issues[0].name) score. Want more info and better options nearby?"
        
        
        let identifier = Bundle.main.bundleIdentifier! + "." + nudge.businessLocation.placeId + "." + nudge.createdAt.description
        let userInfo = ["nudge" : NSKeyedArchiver.archivedData(withRootObject: nudge)]
        
        self.presentNotification(title: title, message: message, identifier: identifier, userInfo: userInfo, debug: false) {
            completion(nudge)
        }
        
        // don't present Nudge notification if already looking at Nudge?
        // limit/collapse previous pending Nudges?
        // filter for dupes?
    }
    
}

extension NotificationService : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Determine the user action
        switch response.actionIdentifier {
        //when the notification is dismissed, restart the manager
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        //when app is opened from tapping on the notification
        case UNNotificationDefaultActionIdentifier:
            AnalyticsService.shared.track(AnalyticsEvent.nudgeNotificationEngaged)
            //print("Default")
            //get the custom dictionary passed with the alert
            let customDict = response.notification.request.content.userInfo
            //get the encoded data
            let encodedData = customDict["nudge"] as! Data
            //convert data back to array and show summary screen
            showNudgeScreen(encodedData)
        //when the alternate button is pressed, show alternate window
        case "moreInfo":
            AnalyticsService.shared.track(AnalyticsEvent.nudgeNotificationEngaged)
            //print("Altenate Button Pressed")
            let customDict = response.notification.request.content.userInfo
            let encodedData = customDict["nudge"] as! Data
            showNudgeScreen(encodedData)
        //if there's a random issue, start the manager again
        default:
            print("Unknown action")
        }
        completionHandler()
    }
    
    func showNudgeScreen(_ encodedData : Data) {
        //convert the encoded data to an dictionary
        let nudge = NSKeyedUnarchiver.unarchiveObject(with: encodedData) as! Nudge
        
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        let tabBarVC = appDelegate.window!.rootViewController as! TabBarController
        
        //now from the root view show the nudge screen
        let summaryStoryboard = UIStoryboard(name: "NudgeSummaryViewController", bundle: nil)
        let nudgeVC = summaryStoryboard.instantiateViewController(withIdentifier: "NudgeSummaryVC") as! NudgeSummaryViewController
        nudgeVC.modalPresentationCapturesStatusBarAppearance = true
        nudgeVC.nudge = nudge
        
        tabBarVC.selectedIndex = 0
        let homeNC = tabBarVC.viewControllers?[0] as! UINavigationController
        homeNC.popToRootViewController(animated: false)
        homeNC.pushViewController(nudgeVC, animated: false)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound when app is in foregoround
        completionHandler(UNNotificationPresentationOptions.alert)
    }
}
