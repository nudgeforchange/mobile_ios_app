//
//  Nudge.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 4/7/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation

struct NudgeKey {
    static let nudgeUUID = "nudge_uuid"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let businessLocation = "businessLocation"
    static let status = "status"
    static let alternatives = "alternatives"
    static let createdAt = "created_at"
    static let sentAt = "sent_at"
    static let seenAt = "seen_at"
    static let acceptedAt = "accepted_at"
    static let declinedAt = "declined_at"
    static let markedInvalidAt = "marked_invalid_at"
    static let alternativeViewedAt = "alternative_viewed_at"
    static let leftBusinessAt = "left_business_at"
    static let visitedAlternativeAt = "visited_alternative_at"
    static let isDemo = "is_demo"
}

enum NudgeStatus: String {
    case created = "created"
    case sent = "sent"
    case seen = "seen"
    case accepted = "accepted"
    case declined = "declined"
    case markedInvalid = "marked_invalid"
    case alternativeViewed = "alternative_viewed"
    case leftBusiness = "left_business"
    case visitedAlternative = "visited_alternative"
}

class Nudge: NSObject, NSCoding {
    var nudgeUUID: String?
    var latitude: Double
    var longitude: Double
    var businessLocation: BusinessLocation
    var status: NudgeStatus {
        didSet {
            let date = Date()
            
            switch self.status {
            case .created: self.createdAt = date
            case .sent: self.sentAt = date
            case .seen: self.seenAt = date
            case .accepted: self.acceptedAt = date
            case .declined: self.declinedAt = date
            case .markedInvalid: self.markedInvalidAt = date
            case .alternativeViewed: self.alternativeViewedAt = date
            case .leftBusiness: self.leftBusinessAt = date
            case .visitedAlternative: self.visitedAlternativeAt = date
            }
            
            NudgeService.sharedInstance.updateNudge(self, status: self.status, statusChangedAt: date) { (success, nudge) in
                
            }
        }
    }
    var alternatives: [BusinessLocation]
    var createdAt: Date
    var sentAt: Date?
    var seenAt: Date?
    var acceptedAt: Date?
    var declinedAt: Date?
    var markedInvalidAt: Date?
    var alternativeViewedAt: Date?
    var leftBusinessAt: Date?
    var visitedAlternativeAt: Date?
    
    var scoredAlternatives: [BusinessLocation] {
        get {
            return self.alternatives.filter { (alternative) -> Bool in
                alternative.score > -1
            }
        }
    }
    
    var unscoredAlternatives: [BusinessLocation] {
        get {
            return self.alternatives.filter({ (alternative) -> Bool in
                alternative.score == -1
            })
        }
    }
    
    var isDemo: Bool = false
    
    init(latitude: Double, longitude: Double, businessLocation: BusinessLocation, status: NudgeStatus, alternatives: [BusinessLocation], createdAt: Date) {
        self.latitude = latitude
        self.longitude = longitude
        self.businessLocation = businessLocation
        self.status = status
        self.alternatives = alternatives
        self.createdAt = createdAt
    }
    
    // MARK: NSCoding
    required init?(coder decoder: NSCoder) {
        self.nudgeUUID = decoder.decodeObject(forKey: NudgeKey.nudgeUUID) as? String
        self.latitude = decoder.decodeDouble(forKey: NudgeKey.latitude)
        self.longitude = decoder.decodeDouble(forKey: NudgeKey.longitude)
        self.businessLocation = decoder.decodeObject(forKey: NudgeKey.businessLocation) as! BusinessLocation
        self.status = NudgeStatus(rawValue: decoder.decodeObject(forKey: NudgeKey.status) as! String)!
        self.alternatives = decoder.decodeObject(forKey: NudgeKey.alternatives) as! [BusinessLocation]
        self.createdAt = decoder.decodeObject(forKey: NudgeKey.createdAt) as! Date
        self.sentAt = decoder.decodeObject(forKey: NudgeKey.sentAt) as? Date
        self.seenAt = decoder.decodeObject(forKey: NudgeKey.seenAt) as? Date
        self.acceptedAt = decoder.decodeObject(forKey: NudgeKey.acceptedAt) as? Date
        self.declinedAt = decoder.decodeObject(forKey: NudgeKey.declinedAt) as? Date
        self.markedInvalidAt = decoder.decodeObject(forKey: NudgeKey.markedInvalidAt) as? Date
        self.alternativeViewedAt = decoder.decodeObject(forKey: NudgeKey.alternativeViewedAt) as? Date
        self.leftBusinessAt = decoder.decodeObject(forKey: NudgeKey.leftBusinessAt) as? Date
        self.visitedAlternativeAt = decoder.decodeObject(forKey: NudgeKey.visitedAlternativeAt) as? Date
        self.isDemo = decoder.decodeBool(forKey: NudgeKey.isDemo)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.nudgeUUID, forKey: NudgeKey.nudgeUUID)
        coder.encode(self.latitude, forKey: NudgeKey.latitude)
        coder.encode(self.longitude, forKey: NudgeKey.longitude)
        coder.encode(self.businessLocation, forKey: NudgeKey.businessLocation)
        coder.encode(self.status.rawValue, forKey: NudgeKey.status)
        coder.encode(self.alternatives, forKey: NudgeKey.alternatives)
        coder.encode(self.createdAt, forKey: NudgeKey.createdAt)
        coder.encode(self.sentAt, forKey: NudgeKey.sentAt)
        coder.encode(self.seenAt, forKey: NudgeKey.seenAt)
        coder.encode(self.acceptedAt, forKey: NudgeKey.acceptedAt)
        coder.encode(self.declinedAt, forKey: NudgeKey.declinedAt)
        coder.encode(self.markedInvalidAt, forKey: NudgeKey.markedInvalidAt)
        coder.encode(self.alternativeViewedAt, forKey: NudgeKey.alternativeViewedAt)
        coder.encode(self.leftBusinessAt, forKey: NudgeKey.leftBusinessAt)
        coder.encode(self.visitedAlternativeAt, forKey: NudgeKey.visitedAlternativeAt)
        coder.encode(self.isDemo, forKey: NudgeKey.isDemo)
    }
    
}
