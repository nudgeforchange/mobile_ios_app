//
//  NudgeDetailHeaderView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/20/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class NudgeDetailHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var nudgingAwayButton: RoundedButton!
    @IBOutlet weak var mapButton: RoundedButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var backgroundColorView: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
