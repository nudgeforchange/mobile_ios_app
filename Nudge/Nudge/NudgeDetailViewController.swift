//
//  NudgeDetailViewController.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/20/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

public enum NudgeDetailType {
    case nudgingAway
    case betterOption
    case searchResultLow
    case searchResultHigh
}

class NudgeDetailViewController: UIViewController {
    
    @IBOutlet weak var statusBarBackgroundView: UIView!
    
    @IBOutlet weak var nudgingAwayHeaderView: NudgeDetailHeaderView!
    @IBOutlet weak var betterOptionHeaderView: NudgeDetailHeaderView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var colorTop: NSLayoutConstraint!
    
    fileprivate var activeHeaderView: NudgeDetailHeaderView!
    
    var nudgeDetailType: NudgeDetailType?
    var nudge: Nudge!
    var businessLocation: BusinessLocation!
    var distance: Double? {
        didSet {
            if let distance = self.distance {
                self.betterOptionHeaderView.mapButton.setTitle(distance.metersToMiles().doubleToMileString(), for: .normal)
            }
        }
    }
    var businessSearchLocation : BusinessLocation!
    
    var lastY: CGFloat = 0.0
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        switch self.nudgeDetailType! {
        case .nudgingAway:
            AnalyticsService.shared.track(AnalyticsEvent.nudgeRedFlagDetailViewed)
            self.activeHeaderView = self.nudgingAwayHeaderView
            self.activeHeaderView.nudgingAwayButton.addTarget(self, action: #selector(nudgingAwayButtonPressed(_:)), for: .touchUpInside)
            if self.nudge.acceptedAt != nil {
                self.activeHeaderView.nudgingAwayButton.setTitle("Nudged away!", for: .normal)
            }
            self.colorView.backgroundColor = AppColors.nudgeRedColor
            self.statusBarBackgroundView.backgroundColor = AppColors.nudgeRedColor
        case .betterOption:
            AnalyticsService.shared.track(AnalyticsEvent.nudgeGreenFlagDetailViewed)
            self.activeHeaderView = self.betterOptionHeaderView
            self.activeHeaderView.mapButton.addTarget(self, action: #selector(mapButtonPressed(_:)), for: .touchUpInside)
            self.activeHeaderView.nudgingAwayButton.addTarget(self, action: #selector(nudgingAwayButtonPressed(_:)), for: .touchUpInside)
            self.colorView.backgroundColor = AppColors.nudgeGreenColor
            self.statusBarBackgroundView.backgroundColor = AppColors.nudgeGreenColor
            self.activeHeaderView.mapButton.imageView?.tintColor = AppColors.nudgeGreenColor
            
            if self.nudge.isDemo == false { self.nudge.status = .alternativeViewed }
            StorageService.sharedInstance.saveNudges()
        case .searchResultHigh:
            AnalyticsService.shared.track(AnalyticsEvent.searchGreenFlagDetailViewed)
            self.activeHeaderView = self.betterOptionHeaderView
            self.activeHeaderView.mapButton.addTarget(self, action: #selector(mapButtonPressed(_:)), for: .touchUpInside)
            self.activeHeaderView.nudgingAwayButton.addTarget(self, action: #selector(nudgingAwayButtonPressed(_:)), for: .touchUpInside)
            self.colorView.backgroundColor = AppColors.nudgeGreenColor
            self.statusBarBackgroundView.backgroundColor = AppColors.nudgeGreenColor
            self.activeHeaderView.mapButton.imageView?.tintColor = AppColors.nudgeGreenColor
            self.activeHeaderView.mapButton.setTitle(self.businessSearchLocation.distance.doubleToMileString(), for: .normal)
        case .searchResultLow:
            AnalyticsService.shared.track(AnalyticsEvent.searchRedFlagDetailViewed)
            self.activeHeaderView = self.betterOptionHeaderView
            self.activeHeaderView.mapButton.addTarget(self, action: #selector(mapButtonPressed(_:)), for: .touchUpInside)
            self.activeHeaderView.nudgingAwayButton.addTarget(self, action: #selector(nudgingAwayButtonPressed(_:)), for: .touchUpInside)
            self.colorView.backgroundColor = AppColors.nudgeRedColor
            self.statusBarBackgroundView.backgroundColor = AppColors.nudgeRedColor
            self.activeHeaderView.mapButton.imageView?.tintColor = AppColors.nudgeRedColor
            self.activeHeaderView.backgroundColorView.backgroundColor = AppColors.nudgeRedColor
            self.activeHeaderView.mapButton.setTitleColor(AppColors.nudgeRedColor, for: .normal)
            self.activeHeaderView.mapButton.setTitle(self.businessSearchLocation.distance.doubleToMileString(), for: .normal)
            self.activeHeaderView.nudgingAwayButton.tintColor = AppColors.nudgeRedColor
            self.activeHeaderView.nudgingAwayButton.setTitleColor(AppColors.nudgeRedColor, for: .normal)
        }
        
        self.activeHeaderView.backButton.addTarget(self, action: #selector(backButtonPressed(_:)), for: .touchUpInside)
        self.activeHeaderView.businessNameLabel.text = self.businessLocation.canonicalName
        self.activeHeaderView.optionsButton.addTarget(self, action: #selector(optionsButtonPressed(_:)), for: .touchUpInside)
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        
        self.tableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "HeaderView")
        
        let businessDetailCellNib: UINib = UINib(nibName: "BusinessDetailCell", bundle: nil)
        self.tableView.register(businessDetailCellNib, forCellReuseIdentifier: "BusinessDetailCell")
        
        let spacerNib: UINib = UINib(nibName: "ScoredBusinessSpacerCell", bundle: nil)
        tableView.register(spacerNib, forCellReuseIdentifier: "ScoredBusinessSpacerCell")
        
        let disclaimerFooterNib: UINib = UINib(nibName: "DisclaimerFooterView", bundle: nil)
        tableView.tableFooterView = disclaimerFooterNib.instantiate(withOwner: nil, options: nil)[0] as! DisclaimerFooterView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        alignColorView(0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alignColorView(_ yOffset: CGFloat) {
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        self.colorTop.constant = headerRect.maxY - yOffset
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func backButtonPressed(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func optionsButtonPressed(_ sender: AnyObject) {
        //print("optionsButtonPressed")
        let businessName = self.businessLocation.canonicalName
        switch self.nudgeDetailType! {
        case .nudgingAway, .searchResultLow:
            let whitelistActionSheet = AlertControllerFactory.whitelistActionSheet(businessName: businessName, completion: { (scope) in
                if let scope = scope {
                    let reasonActionSheet = AlertControllerFactory.whitelistReasonActionSheet(scope: scope, completion: { (scope, reason) in
                        if let reason = reason {
                            if scope == 0 {
                                AnalyticsService.shared.track(AnalyticsEvent.whitelistedBusiness, properties: ["reason" : reason.rawValue])
                                NoNudgeService.sharedInstance.whitelistBusiness(businessUUID: self.businessLocation.business!.businessUUID, reason: reason, completion: {
                                    
                                })
                            } else {
                                AnalyticsService.shared.track(AnalyticsEvent.whitelistedLocation, properties: ["reason" : reason.rawValue])
                                NoNudgeService.sharedInstance.whitelistBusinessLocation(businessLocationUUID: self.businessLocation.businessLocationUUID, reason: reason, completion: {
                                    
                                })
                            }
                            let confirmedAlert = AlertControllerFactory.whitelistConfirmedAlert(businessName: businessName, scope: scope)
                            self.present(confirmedAlert, animated: true, completion: {
                                
                            })
                        }
                    })
                    self.present(reasonActionSheet, animated: true, completion: {
                        
                    })
                }
            })
            self.present(whitelistActionSheet, animated: true, completion: {
                
            })
        case .betterOption, .searchResultHigh:
            let blacklistActionSheet = AlertControllerFactory.blacklistActionSheet(businessName: businessName, completion: { (scope) in
                if let scope = scope {
                    let reasonActionSheet = AlertControllerFactory.blacklistReasonActionSheet(scope: scope, completion: { (scope, reason) in
                        if let reason = reason {
                            if scope == 0 {
                                AnalyticsService.shared.track(AnalyticsEvent.blacklistedBusiness, properties: ["reason" : reason.rawValue])
                                NoNudgeService.sharedInstance.blacklistBusiness(businessUUID: self.businessLocation.business!.businessUUID, reason: reason, completion: {
                                    
                                })
                            } else {
                                AnalyticsService.shared.track(AnalyticsEvent.blacklistedLocation, properties: ["reason" : reason.rawValue])
                                NoNudgeService.sharedInstance.blacklistBusinessLocation(businessLocationUUID: self.businessLocation.businessLocationUUID, reason: reason, completion: {
                                    
                                })
                            }
                            
                            let confirmedAlert = AlertControllerFactory.blacklistConfirmedAlert(businessName: businessName, scope: scope)
                            self.present(confirmedAlert, animated: true, completion: {
                                
                            })
                        }
                    })
                    self.present(reasonActionSheet, animated: true, completion: {
                        
                    })
                }
            })
            self.present(blacklistActionSheet, animated: true, completion: {
                
            })
        }
    }
    
    func presentNudgeAwayShareAlert(businessLocation : BusinessLocation) {
        let shareAlert = AlertControllerFactory.nudgingAwayShareAlert(completion: { (serviceType) in
            if let serviceType = serviceType {
                if let shareController = SharingService.nudgingAwayController(businessLocation: businessLocation, serviceType: serviceType) {
                    self.present(shareController, animated: true, completion: { })
                }
            }
        })
        self.present(shareAlert, animated: true) { }
    }
    
    func presentNudgeToShareAlert(businessLocation : BusinessLocation) {
        let shareAlert = AlertControllerFactory.nudgingToShareAlert(completion: { (serviceType) in
            if let serviceType = serviceType {
                if let shareController = SharingService.nudgingToController(businessLocation: businessLocation, serviceType: serviceType) {
                    self.present(shareController, animated: true, completion: { })
                }
            }
        })
        self.present(shareAlert, animated: true) { }
    }
    
    func presentNudgeAwayConfirmAlert(businessName: String, worstIssueName: String, sender: AnyObject) {
        let businessName = self.nudge.businessLocation.canonicalName
        //let worstIssueName = self.nudge.businessLocation.worstIssue.name
        let confirmAlert = AlertControllerFactory.nudgingAwayConfirmAlert(businessName: businessName) { (didNudgeAway) in
            if didNudgeAway {
                if self.nudge.isDemo == false { self.nudge.status = .accepted }
                StorageService.sharedInstance.saveNudges()
                
                let button = sender as! UIButton
                button.setTitle("Nudged away!", for: .normal)
                
                self.presentNudgeAwayShareAlert(businessLocation: self.nudge.businessLocation)
            } else {
                
                if self.nudge.isDemo == false { self.nudge.status = .declined }
                StorageService.sharedInstance.saveNudges()
                
            }
        }
        self.present(confirmAlert, animated: true) { }
    }
    
    func nudgingAwayButtonPressed(_ sender: AnyObject) {
        //print("nudgingAwayButtonPressed")
        //let businessName = self.businessLocation.canonicalName
        switch self.nudgeDetailType! {
        case .nudgingAway, .searchResultLow:
            self.presentNudgeAwayShareAlert(businessLocation: self.businessLocation)
        case .betterOption, .searchResultHigh:
            self.presentNudgeToShareAlert(businessLocation: self.businessLocation)
        }
        /*
         if self.nudge.acceptedAt != nil {
         self.presentNudgeAwayShareAlert(businessName: businessName, worstIssueName: worstIssueName)
         } else {
         self.presentNudgeAwayConfirmAlert(businessName: businessName, worstIssueName: worstIssueName, sender: sender)
         }
         */
    }
    
    func mapButtonPressed(_ sender: AnyObject) {
        //print("mapButtonPressed")
        switch self.nudgeDetailType! {
        case .nudgingAway:
            break
        case .betterOption:
            MapsService.openMapApp(name: self.businessLocation.canonicalName, latitude: self.businessLocation.latitude, longitude: self.businessLocation.longitude)
        default:
            MapsService.openMapApp(address: self.businessSearchLocation.address)
        }
    }
    
    func infoButtonPressed(_ sender: AnyObject) {
        let infoButton = sender as! UIButton
        let row = infoButton.tag
        let issue = self.businessLocation.issues[row/2]
        if issue.sources.count > 0 {
            let sources = Set(issue.sources.map { (source) -> String in
                let url = URL(string: source)!
                let host = url.host!
                let hostComponents = host.components(separatedBy: ".")
                let count = hostComponents.count
                if count >= 2 {
                    return hostComponents[count-2] + "." + hostComponents[count-1]
                } else {
                    return host
                }
            })
            let alert = UIAlertController(title: "Sources", message: sources.joined(separator: "\n"), preferredStyle: .alert)
            let doneAction = UIAlertAction(title: "Done", style: .cancel, handler: nil)
            alert.addAction(doneAction)
            let viewAction = UIAlertAction(title: "View", style: .default, handler: {(action) -> Void in
                let nameArray : [String] = Array(sources)
                self.openTabbedWebView(shortNames: nameArray, fullLinks: issue.sources)
            })
            alert.addAction(viewAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openTabbedWebView(shortNames : [String], fullLinks : [String]) {
        let storyBoard = UIStoryboard(name: "TabbedWebViewController", bundle: nil)
        let tabView = storyBoard.instantiateViewController(withIdentifier: "TabbedWebVC") as! TabbedWebViewController
        tabView.webLinks = fullLinks
        self.present(tabView, animated: true, completion: nil)
    }
}

extension NudgeDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.businessLocation.issues.count * 2 - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessSpacerCell", for: indexPath) as! ScoredBusinessSpacerCell
            cell.colorView.backgroundColor = UIColor(hexString: "E4E4E4")
            return cell
        }
        
        let issue: BusinessIssueScore = self.businessLocation.issues[indexPath.row/2]
        let cell: BusinessDetailCell = tableView.dequeueReusableCell(withIdentifier: "BusinessDetailCell", for: indexPath) as! BusinessDetailCell
        cell.issue = issue
        cell.issueTextLabel!.preferredMaxLayoutWidth = self.view.bounds.size.width - 68
        cell.infoButton.tag = indexPath.row
        cell.infoButton.removeTarget(nil, action: nil, for: .touchUpInside)
        cell.infoButton.addTarget(self, action: #selector(infoButtonPressed(_:)), for: .touchUpInside)
        switch indexPath.row {
        case 0:
            cell.setTop()
        case self.businessLocation.issues.count * 2 - 2:
            cell.setBottom()
        default:
            cell.setMiddle()
        }
        return cell
    }
}

extension NudgeDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UITableViewHeaderFooterView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView")!
        headerView.addSubview(self.activeHeaderView)
        let leading = NSLayoutConstraint(item: self.activeHeaderView, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: self.activeHeaderView, attribute: .trailing, relatedBy: .equal, toItem: headerView, attribute: .trailing, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: self.activeHeaderView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1, constant: 0)
        //        let bottom = NSLayoutConstraint(item: self.activeHeaderView, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: 0)
        headerView.addConstraints([leading, trailing, top]) //, top, bottom])
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.shadowRadius = 10
        headerView.layer.shadowOpacity = 0
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

extension NudgeDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        alignColorView(yOffset)
        
        // fade color view logic
        let headerRect = self.tableView.rectForHeader(inSection: 0)
        let firstCellRect = self.tableView.rectForRow(at: IndexPath(row: 0, section: 0))
        let firstCellObscurage = yOffset + headerRect.height - firstCellRect.origin.y
        self.colorView.alpha = 1.0 - max(0, min(1, firstCellObscurage/50.0))
        
        // shadow logic
        if let headerView = self.tableView.headerView(forSection: 0) {
            headerView.layer.shadowOpacity = (Float(1.0) - Float(colorView.alpha))/Float(5.0)
        }
        
        //        // overscrolling logic
        //        let currentY = scrollView.contentOffset.y
        //        if currentY > lastY {
        //            //"scrolling down"
        //            tableView.bounces = true
        //        } else {
        //            //"scrolling up"
        //            // Check that we are not in bottom bounce
        //            if scrollView.contentInset.top >= 0 {
        //                tableView.bounces = false
        //            }
        //        }
        //        lastY = scrollView.contentOffset.y
    }
    
}
