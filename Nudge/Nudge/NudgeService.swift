import Alamofire

class NudgeService: APIService {
    
    static let sharedInstance = NudgeService()
    
    func createNudge(_ nudge: Nudge, completion: @escaping (_ success: Bool, _ nudge: Nudge) -> Void) {
        let urlString = "\(APIService.baseURL)/nudges/"
        let params: Parameters = [
            "nudge": [
                "place": [
                    "place_id": nudge.businessLocation.placeId
                ],
                "created_at": ISO8601DateFormatter().string(from: nudge.createdAt)
            ]
        ]
        //print(params)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                   // print(response.response!)
                    completion(false, nudge)
                    return
                }
                
                if let result = response.result.value {
                    let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    let success = JSON["success"] as! Bool
                    nudge.nudgeUUID = JSON["nudge_uuid"] as? String
                    completion(success, nudge)
                }
            }
        }
    }
    
    func updateNudge(_ nudge: Nudge, status: NudgeStatus, statusChangedAt: Date, completion: @escaping (_ success: Bool, _ nudge: Nudge) -> Void) {
        let urlString = "\(APIService.baseURL)/nudges/\(nudge.nudgeUUID!)/"
        let params: Parameters = [
            "status": status.rawValue,
            "status_changed_at": ISO8601DateFormatter().string(from: statusChangedAt)
        ]
        //print(params)
        
        self.authenticateIfNecessary {
            let request = Alamofire.request(urlString, method: .put, parameters: params, encoding: JSONEncoding.default, headers: self.getSecureHeader())
            
            request.responseJSON { (response) in
                guard response.error == nil else {
                    completion(false, nudge)
                    return
                }
                
                if let result = response.result.value {
                    let JSON = result as! [String : AnyObject]
                    //print(JSON)
                    let success = JSON["success"] as! Bool
                    completion(success, nudge)
                }
            }
        }
    }
    
}
