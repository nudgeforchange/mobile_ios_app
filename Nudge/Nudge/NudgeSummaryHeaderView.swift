//
//  NudgeSummaryHeaderView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/19/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class NudgeSummaryHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var sectionTitleLabel: UILabel!

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let resultView = super.hitTest(point, with: event)
        return resultView === self.backButton ? self.backButton : nil
    }
}
