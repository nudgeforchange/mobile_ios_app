//
//  NudgeSummaryViewController.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/19/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

/*
 - when user pulls past the top, the top header should stick to top, but let the rest of tableView scroll down
 - there should be a header backing view, that appears behind the section's first cell. this should always have the same y position as the header.
   - when the header is at the top, and the user pushes the tableView up, the header should introduce a faint shadow below it, while the backing view should fade.
 
 */

import UIKit

class NudgeSummaryViewController: UIViewController {
    
    @IBOutlet weak var statusBarBackgroundView: UIView!
    
    @IBOutlet weak var nudgedAwayFromColorView: UIView!
    @IBOutlet weak var betterOptionsColorView: UIView!
    @IBOutlet weak var otherOptionsColorView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nudgedAwayFromHeaderView: NudgeSummaryHeaderView!
    @IBOutlet weak var betterOptionsHeaderView: NudgeSummaryHeaderView!
    @IBOutlet weak var otherOptionsHeaderView: NudgeSummaryHeaderView!
    
    @IBOutlet weak var nudgedAwayFromTop: NSLayoutConstraint!
    @IBOutlet weak var betterOptionsTop: NSLayoutConstraint!
    @IBOutlet weak var otherOptionsTop: NSLayoutConstraint!
    
    fileprivate var colorViews: [UIView]!
    fileprivate var headerViews: [NudgeSummaryHeaderView]!
    fileprivate var tops: [NSLayoutConstraint]!
    
    var nudge: Nudge!
    
    var lastY: CGFloat = 0.0
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsService.shared.track(AnalyticsEvent.nudgeViewed)

        // Do any additional setup after loading the view.
        self.colorViews = [self.nudgedAwayFromColorView, self.betterOptionsColorView, self.otherOptionsColorView]
        self.headerViews = [self.nudgedAwayFromHeaderView, self.betterOptionsHeaderView, self.otherOptionsHeaderView]
        self.tops = [self.nudgedAwayFromTop, self.betterOptionsTop, self.otherOptionsTop]
        
//        let headerViewNib: UINib = UINib(nibName: "NudgeSummaryHeaderView", bundle: nil)
        //        self.tableView.register(headerViewNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
//        self.tableView.register(NudgeSummaryHeaderView.self, forHeaderFooterViewReuseIdentifier: "HeaderView")
        self.tableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "HeaderView")
        
        let scoredBusinessCellNib: UINib = UINib(nibName: "ScoredBusinessCell", bundle: nil)
        tableView.register(scoredBusinessCellNib, forCellReuseIdentifier: "ScoredBusinessCell")
        
        let spacerNib: UINib = UINib(nibName: "ScoredBusinessSpacerCell", bundle: nil)
        tableView.register(spacerNib, forCellReuseIdentifier: "ScoredBusinessSpacerCell")
        
        let betterOptionsEmptyCellNib: UINib = UINib(nibName: "BetterOptionsEmptyCell", bundle: nil)
        tableView.register(betterOptionsEmptyCellNib, forCellReuseIdentifier: "BetterOptionsEmptyCell")
        
        let unscoredBusinessCellNib = UINib(nibName: "UnscoredBusinessCell", bundle: nil)
        tableView.register(unscoredBusinessCellNib, forCellReuseIdentifier: "UnscoredBusinessCell")
        
        let disclaimerFooterNib: UINib = UINib(nibName: "DisclaimerFooterView", bundle: nil)
        tableView.tableFooterView = disclaimerFooterNib.instantiate(withOwner: nil, options: nil)[0] as! DisclaimerFooterView
        
        if self.nudge.isDemo == false {self.nudge.status = .seen }
        StorageService.sharedInstance.saveNudges()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        for section in 0..<self.tableView.numberOfSections {
            let firstCellRect = self.tableView.rectForRow(at: IndexPath(row: 0, section: section))
            self.tops[section].constant = firstCellRect.origin.y - self.tableView.contentOffset.y
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func presentNudgeAwayShareAlert(businessLocation : BusinessLocation) {
        let shareAlert = AlertControllerFactory.nudgingAwayShareAlert(completion: { (serviceType) in
            if let serviceType = serviceType {
                if let shareController = SharingService.nudgingAwayController(businessLocation: businessLocation, serviceType: serviceType) {
                    self.present(shareController, animated: true, completion: { })
                }
            }
        })
        self.present(shareAlert, animated: true) { }
    }
    
    func presentNudgeAwayConfirmAlert(businessName: String, worstIssueName: String, sender: AnyObject) {
        let businessName = self.nudge.businessLocation.canonicalName
        //let worstIssueName = self.nudge.businessLocation.worstIssue.name
        let confirmAlert = AlertControllerFactory.nudgingAwayConfirmAlert(businessName: businessName) { (didNudgeAway) in
            if didNudgeAway {
                if self.nudge.isDemo == false { self.nudge.status = .accepted }
                StorageService.sharedInstance.saveNudges()
                
                let button = sender as! UIButton
                button.setTitle("Nudged away!", for: .normal)
                
                self.presentNudgeAwayShareAlert(businessLocation: self.nudge.businessLocation)
            } else {
                if self.nudge.isDemo == false { self.nudge.status = .declined }
                StorageService.sharedInstance.saveNudges()
            }
        }
        self.present(confirmAlert, animated: true) { }
    }
    
    func nudgingAwayButtonPressed(_ sender: AnyObject) {
        //print("nudgingAwayButtonPressed")
        //let businessName = self.nudge.businessLocation.canonicalName
        //let worstIssueName = self.nudge.businessLocation.worstIssue.name
        self.presentNudgeAwayShareAlert(businessLocation: self.nudge.businessLocation)
        /* blocking out previous workflow showing the nudged away button
        if self.nudge.acceptedAt != nil {
            self.presentNudgeAwayShareAlert(businessName: businessName, worstIssueName: worstIssueName)
        } else {
            self.presentNudgeAwayConfirmAlert(businessName: businessName, worstIssueName: worstIssueName, sender: sender)
        }
       */
    }
    
    func nudgingToShareButtonPressed(_ sender: UIButton) {
        //print("nudgingAwayButtonPressed")
        let businessLocation = self.nudge.scoredAlternatives[sender.tag]
        self.presentNudgeToShareAlert(businessLocation: businessLocation)
    }
    
    func presentNudgeToShareAlert(businessLocation : BusinessLocation) {
        let shareAlert = AlertControllerFactory.nudgingToShareAlert(completion: { (serviceType) in
            if let serviceType = serviceType {
                if let shareController = SharingService.nudgingToController(businessLocation: businessLocation, serviceType: serviceType) {
                    self.present(shareController, animated: true, completion: { })
                }
            }
        })
        self.present(shareAlert, animated: true) { }
    }

}

extension NudgeSummaryViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension NudgeSummaryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return max(2, self.nudge.scoredAlternatives.count * 2)
        default:
            return self.nudge.unscoredAlternatives.count * 2 - 1
        }
    }
    
    func nudgedAwayFromCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ScoredBusinessCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessCell", for: indexPath) as! ScoredBusinessCell
        cell.businessLocation = self.nudge.businessLocation
        cell.nudgingAwayView.expand()
        cell.nudgingAwayButton.backgroundColor = AppColors.nudgeRedColor
        cell.nudgingAwayButton.addTarget(self, action: #selector(nudgingAwayButtonPressed(_:)), for: .touchUpInside)
        cell.shareButton.isHidden = true
        
        if self.nudge.acceptedAt != nil {
            cell.nudgingAwayButton.setTitle("Nudged away!", for: .normal)
        }
        
        return cell
    }
    
    func betterOptionsCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ScoredBusinessCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessCell", for: indexPath) as! ScoredBusinessCell
        cell.businessLocation = self.nudge.scoredAlternatives[indexPath.row/2]
        cell.nudgingAwayView.collapse()
        cell.shareButton.tintColor = AppColors.nudgeGreenColor
        cell.shareButton.removeTarget(nil, action: nil, for: .touchUpInside)
        cell.shareButton.addTarget(self, action: #selector(nudgingToShareButtonPressed(_:)), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row/2
        return cell
    }
    
    func betterOptionsEmptyCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: BetterOptionsEmptyCell = tableView.dequeueReusableCell(withIdentifier: "BetterOptionsEmptyCell", for: indexPath) as! BetterOptionsEmptyCell
        return cell
    }
    
    func otherOptionsCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: UnscoredBusinessCell = tableView.dequeueReusableCell(withIdentifier: "UnscoredBusinessCell", for: indexPath) as! UnscoredBusinessCell
        cell.businessLocation = self.nudge.unscoredAlternatives[indexPath.row/2]
        
        let nudgedAwayFromLocation = CLLocation(latitude: self.nudge.businessLocation.latitude, longitude: self.nudge.businessLocation.longitude)
        let otherOptionLocation = CLLocation(latitude: cell.businessLocation.latitude, longitude: cell.businessLocation.longitude)
        cell.distanceLabel.text = nudgedAwayFromLocation.distance(from: otherOptionLocation).metersToMiles().doubleToMileString()
        
        switch indexPath.row {
        case 0:
            cell.setTop()
        case self.nudge.unscoredAlternatives.count * 2 - 2:
            cell.setBottom()
        default:
            cell.setMiddle()
        }
        return cell
    }
    
    func spacerCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: ScoredBusinessSpacerCell = tableView.dequeueReusableCell(withIdentifier: "ScoredBusinessSpacerCell", for: indexPath) as! ScoredBusinessSpacerCell
        if indexPath.section == 2 {
            cell.colorView.backgroundColor = UIColor(hexString: "E4E4E4")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            return spacerCell(tableView: tableView, indexPath: indexPath)
        }
        
        switch indexPath.section {
        case 0:
            return nudgedAwayFromCell(tableView: tableView, indexPath: indexPath)
        case 1:
            if self.nudge.scoredAlternatives.count == 0 {
                return betterOptionsEmptyCell(tableView: tableView, indexPath: indexPath)
            } else {
                return betterOptionsCell(tableView: tableView, indexPath: indexPath)
            }
        default:
            return otherOptionsCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
}

extension NudgeSummaryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
//    func nudgedAwayFromHeaderView(tableView: UITableView, section: Int) -> NudgeSummaryHeaderView {
//        let headerView: NudgeSummaryHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! NudgeSummaryHeaderView
//        headerView.colorView.backgroundColor = AppColors.nudgeRedColor
//        headerView.backButton.isHidden = false
//        headerView.backButton.addTarget(self, action: #selector(NudgeSummaryViewController.backButtonPressed(sender:)), for: .touchUpInside)
//        headerView.sectionTitleLabel.text = "Nudged away from:"
//        
//        let headerRect: CGRect = tableView.rectForHeader(inSection: section)
//        self.nudgedAwayFromColorTop.constant = headerRect.origin.y
//        
//        return headerView
//    }
//    
//    func betterOptionsHeaderView(tableView: UITableView, section: Int) -> NudgeSummaryHeaderView {
//        let headerView: NudgeSummaryHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! NudgeSummaryHeaderView
//        headerView.colorView.backgroundColor = AppColors.nudgeGreenColor
//        headerView.backButton.isHidden = true
//        headerView.sectionTitleLabel.text = "Better options:"
//        
//        let headerRect: CGRect = tableView.rectForHeader(inSection: section)
//        self.betterOptionsColorTop.constant = headerRect.origin.y
//        
//        return headerView
//    }
//    
//    func otherOptionsHeaderView(tableView: UITableView, section: Int) -> NudgeSummaryHeaderView {
//        let headerView: NudgeSummaryHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! NudgeSummaryHeaderView
//        headerView.backButton.isHidden = true
//        headerView.colorView.backgroundColor = AppColors.nudgeGrayColor
//        headerView.sectionTitleLabel.text = "Other options:"
//        
//        let headerRect: CGRect = tableView.rectForHeader(inSection: section)
//        self.otherOptionsColorTop.constant = headerRect.origin.y
//        
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView")!
        let actualHeaderView = self.headerViews[section]
        headerView.addSubview(actualHeaderView)
        let leading = NSLayoutConstraint(item: actualHeaderView, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: actualHeaderView, attribute: .trailing, relatedBy: .equal, toItem: headerView, attribute: .trailing, multiplier: 1, constant: 0)
        headerView.addConstraints([leading, trailing])
        headerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        headerView.layer.shadowRadius = 10
        headerView.layer.shadowOpacity = 0
        return headerView
        
//        switch section {
//        case 0:
//            return nudgedAwayFromHeaderView(tableView: tableView, section: section)
//        case 1:
//            return betterOptionsHeaderView(tableView: tableView, section: section)
//        default:
//            return otherOptionsHeaderView(tableView: tableView, section: section)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 1 {
            return indexPath.section < 2 ? 10 : 0.5
        }
        
        switch indexPath.section {
        case 0:
            return 184
        case 1:
            return 140
        default:
            switch indexPath.row {
            case self.nudge.unscoredAlternatives.count * 2 - 2:
                return 52
            default:
                return 50
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section < 2 && indexPath.row % 2 == 0 {
            let storyBoard = UIStoryboard(name: "NudgeDetailViewController", bundle: nil)
            let nudgeDetailVC = storyBoard.instantiateViewController(withIdentifier: "NudgeDetailVC") as! NudgeDetailViewController
            
            nudgeDetailVC.nudgeDetailType = indexPath.section == 0 ? .nudgingAway : .betterOption
            switch nudgeDetailVC.nudgeDetailType! {
            case .nudgingAway:
                nudgeDetailVC.nudge = self.nudge
                nudgeDetailVC.businessLocation = self.nudge.businessLocation
            case .betterOption:
                guard self.nudge.scoredAlternatives.count > 0 else { return }
                let betterOption = self.nudge.scoredAlternatives[indexPath.row/2]
                nudgeDetailVC.nudge = self.nudge
                nudgeDetailVC.businessLocation = betterOption
                let nudgedAwayFromLocation = CLLocation(latitude: self.nudge.businessLocation.latitude, longitude: self.nudge.businessLocation.longitude)
                let betterOptionLocation = CLLocation(latitude: betterOption.latitude, longitude: betterOption.longitude)
                nudgeDetailVC.distance = nudgedAwayFromLocation.distance(from: betterOptionLocation)
            default:
                break
            }
            self.navigationController?.pushViewController(nudgeDetailVC, animated: true)
        } else if indexPath.section == 2 && indexPath.row % 2 == 0 {
            let otherOption = self.nudge.unscoredAlternatives[indexPath.row/2]
            MapsService.openMapApp(name: otherOption.canonicalName, latitude: otherOption.latitude, longitude: otherOption.longitude)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}

extension NudgeSummaryViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        
        for section in 0..<self.tableView.numberOfSections {
            let headerView = self.headerViews[section]
            let colorView = self.colorViews[section]
            let top = self.tops[section]
            
            let headerRect = self.tableView.rectForHeader(inSection: section)
            let sectionRect = self.tableView.rect(forSection: section)
            
            let isScrollingUp = yOffset > 0
            let sectionPartiallyObscured = yOffset > sectionRect.minY && sectionRect.maxY >= yOffset
            
            // floating header logic
            var headerY: CGFloat!
            if isScrollingUp && sectionPartiallyObscured {
                let visibleHeight = sectionRect.maxY - yOffset
                headerY = min(0, visibleHeight - headerRect.height)
                self.statusBarBackgroundView.backgroundColor = colorView.backgroundColor
            } else {
                headerY = headerRect.origin.y - yOffset
            }
            
            // fade color view logic
            let firstCellRect = self.tableView.rectForRow(at: IndexPath(row: 0, section: section))
            top.constant = firstCellRect.origin.y - yOffset
            let firstCellObscurage = yOffset + headerRect.height - firstCellRect.origin.y
            colorView.alpha = 1.0 - max(0, min(1, firstCellObscurage/50.0))
            
            // shadow logic
            if let actualHeaderView = self.tableView.headerView(forSection: section) {
                actualHeaderView.layer.shadowOpacity = (Float(1.0) - Float(colorView.alpha))/Float(5.0)
            }
            
            // show back button logic
            UIView.animate(withDuration: 0.1, animations: {
                let headerNotObscured = headerY >= 0
                let firstHeaderIsTop = section == 0
                let lowerHeaderIsTop = section > 0 && headerY < headerRect.height
                let showBackButton = headerNotObscured && (firstHeaderIsTop || lowerHeaderIsTop)
                headerView.backButton.alpha = showBackButton ? 1 : 0
                if section == 0 && headerY >= 0 {
                    headerView.backButton.alpha = 1
                }
            })
        }
        
//        // overscrolling logic
//        let currentY = scrollView.contentOffset.y
//        if currentY > lastY {
//            //"scrolling down"
//            tableView.bounces = true
//        } else {
//            //"scrolling up"
//            // Check that we are not in bottom bounce
//            if scrollView.contentInset.top >= 0 {
//                tableView.bounces = false
//            }
//        }
//        lastY = scrollView.contentOffset.y
    }
    
}
