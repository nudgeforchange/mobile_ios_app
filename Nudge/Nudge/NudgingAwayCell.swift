//
//  NudgingAwayCellTableViewCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/24/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class NudgingAwayCell: UITableViewCell {
    
    @IBOutlet weak var nudgingAwayButton: RoundedButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.nudgingAwayButton.backgroundColor = AppColors.nudgeRedColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
