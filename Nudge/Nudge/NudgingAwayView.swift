//
//  NudgingAwayView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/24/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class NudgingAwayView: UIView {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    func collapse() {
        self.topConstraint.constant = 0
        self.bottomConstraint.constant = 0
        self.heightConstraint.constant = 0
        self.isHidden = true
    }
    
    func expand() {
        self.heightConstraint.constant = 44
        self.topConstraint.constant = 8
        self.bottomConstraint.constant = 8
        self.isHidden = false
    }
}
