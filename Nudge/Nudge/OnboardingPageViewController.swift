//
//  OnboardingPageViewController.swift
//  Nudge
//
//  Created by Ultron on 1/27/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class OnboardingPageViewController: UIPageViewController {
    
    var orderedViewControllers : [UIViewController] = []
    var pageCount = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsService.shared.track(AnalyticsEvent.onboardingFirstPageViewed)

        // Do any additional setup after loading the view.
        //set the data source for the page controller
        dataSource = self
        delegate = self
        self.setBackgroundGradient()
        //setup the view controllers for the page controller
        self.setupViewControllers()
        //setup the colors on the page controller
        self.setupPageControler()
        //setup the content controller for the page view controller
        let pageContentController = self.orderedViewControllers[0]
        setViewControllers([pageContentController], direction: .forward, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent    
    }
    
    
    private func setupPageControler() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = AppColors.nudgeYellowColor
        appearance.currentPageIndicatorTintColor = AppColors.nudgeWhiteColor
        appearance.backgroundColor = UIColor.clear
        appearance.isOpaque = false
    }
    
    private func setupViewControllers() {
        let storyboard = UIStoryboard(name: "OnboardingViewController", bundle: nil)
        let viewController1 = storyboard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
        //viewController1.actionButton.isHidden = true
        viewController1.iconImage = UIImage(named: "LogoIcon")
        viewController1.headerString = "Ready to change it up?"
        viewController1.messageString = "Nudge empowers you to put your money where your beliefs are.\n\nWe make it easy by alerting you if you're about to spend your hard-earned cash in a way that doesn't align with your core values, and nudging you toward alternatives you can feel good about supporting.\n\n\n\n"
        viewController1.hideButton = true
        viewController1.isBulletList = false
        viewController1.isLocationScreen = false
        viewController1.isLastScreen = false
        self.orderedViewControllers.append(viewController1)
        let viewController2 = storyboard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
        //viewController2.actionButton.isHidden = true
        viewController2.iconImage = UIImage(named: "GearsIcon")
        viewController2.headerString = "How does it work?"
        viewController2.messageString = "Swipe right or left to let us know which issues matter to you the most.\n\nGet on with your life.\n\nWhen you walk into a business that doesn't share your values, we'll give you a heads up and alert you to better options nearby.\n\n"
        viewController2.hideButton = true
        viewController2.isBulletList = false
        viewController2.isLocationScreen = false
        viewController2.isLastScreen = false
        self.orderedViewControllers.append(viewController2)
        let viewController3 = storyboard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
        //viewController3.actionButton.isHidden = false
        viewController3.iconImage = UIImage(named: "WalletIcon")!
        viewController3.headerString = "Small $$ = Big change"
        viewController3.messageString = "Soon all of our small purchases will add up to big change.\n\nChoose the issues most important to you to get started. (This will only take a minute.)\n\n\n\n"
        viewController3.hideButton = false
        viewController3.isBulletList = false
        viewController3.isLocationScreen = false
        viewController3.isLastScreen = false
        self.orderedViewControllers.append(viewController3)
    }
    
    
}

extension OnboardingPageViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}

extension OnboardingPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if finished {
            if let currentVC = pageViewController.viewControllers?[0] {
                if currentVC === self.orderedViewControllers[1] {
                    AnalyticsService.shared.track(AnalyticsEvent.onboardingSecondPageViewed)
                } else if currentVC === self.orderedViewControllers[2] {
                    AnalyticsService.shared.track(AnalyticsEvent.onboardingThirdPageViewed)
                }
            }
        }
    }
}
