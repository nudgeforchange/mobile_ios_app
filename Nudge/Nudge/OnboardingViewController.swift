//
//  OnboardingViewController.swift
//  Nudge
//
//  Created by Ultron on 1/27/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import UserNotifications
import DeviceKit

class OnboardingViewController: UIViewController {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var dashContainerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var dashView: UIView!
    
    var iconImage : UIImage!
    var headerString : String!
    var messageString : String!
    var hideButton : Bool = false
    var isBulletList : Bool = false
    var isLocationScreen : Bool = false
    var isLastScreen : Bool = false
    var isLocationSecondaryScreen : Bool = false
    var isNotificationSecondaryScreen : Bool = false
    var hideIcon : Bool = false
    var isLargeScreen : Bool = false
    var isiPad: Bool = false
    
    var locationManager : CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let device = Device()
        let largeDevices : [Device] = [.iPhone6Plus, .iPhone6sPlus, .iPhone7Plus, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone7Plus)]
        self.isLargeScreen = device.isOneOf(largeDevices)
        self.isiPad = device.isPad
        
        self.setBackgroundGradient()
        self.setupActionButton()
        self.setupTitleLabel()
        self.setupMessageLabel()
        self.setupImageView()
        
        if self.isNotificationSecondaryScreen == true {
            NotificationCenter.default.addObserver(self, selector: #selector(cameBackFromSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setupDashImage()
    }
    
    func cameBackFromSettings() {
        //print("came back from settings")
        if self.isNotificationSecondaryScreen == true {
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings(completionHandler: {(settings) in
                //print(settings)
                if settings.authorizationStatus == .authorized {
                    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
                    let main = OperationQueue.main
                    main.addOperation {
                        self.showQuestionCards()
                    }
                }
            })
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupImageView() {
        if hideIcon == false {
            self.iconImageView.image = iconImage
//            self.iconImageView.tintColor = AppColors.nudgeWhiteColor
        }
    }
    
    private func setupTitleLabel() {
        self.headerLabel.text = headerString
        let fontSize: CGFloat = (27.0 * (UIScreen.main.bounds.height/667.0)).rounded()
        self.headerLabel.font = UIFont(name: AppFontNames.regularFont, size: fontSize)
//        self.headerLabel.textColor = AppColors.nudgeWhiteColor
//        self.headerLabel.font = UIFont(name: AppFontNames.regularFont, size: 28)
    }
    
    private func setupMessageLabel() {
        self.messageLabel.text = messageString
        //print(UIScreen.main.bounds.height)
        var fontSize : CGFloat
        if self.isNotificationSecondaryScreen == true || self.isLocationSecondaryScreen == true {
            fontSize = (15.0 * (UIScreen.main.bounds.height/667.0)).rounded()
        }
        else {
            fontSize = (18.0 * (UIScreen.main.bounds.height/667.0)).rounded()
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.9
        let attributes : [String : Any] = [ NSFontAttributeName : UIFont(name: AppFontNames.regularFont, size: fontSize)!,
                           NSForegroundColorAttributeName : UIColor.white,
                           NSParagraphStyleAttributeName : paragraphStyle ]
        let attributedText = NSMutableAttributedString(string: messageString,
                                                       attributes: attributes)
        self.messageLabel.attributedText = attributedText
//        self.messageLabel.adjustsFontSizeToFitWidth = true
//        self.messageLabel.textColor = AppColors.nudgeWhiteColor
//        self.messageLabel.font = UIFont(name: AppFontNames.regularFont, size: 17)
    }
    
    private func setupActionButton() {
//        self.actionButton.backgroundColor = AppColors.nudgeWhiteColor
        self.actionButton.setTitleColor(AppColors.nudgePurpleColor, for: .normal)
        self.actionButton.layer.cornerRadius = 20
        
        if self.hideButton == false {
            self.actionButton.setTitle("Get Started", for: .normal)
        }
        else {
            self.actionButton.isHidden = true
        }
        
        //change the text for the location screen
        if self.isLocationScreen == true {
            self.actionButton.setTitle("Set location preferences", for: .normal)
            //clear the other target
            self.actionButton.removeTarget(nil, action: nil, for: .allEvents)
            //add the location target
            self.actionButton.addTarget(self, action: #selector(OnboardingViewController.locationButtonPressed), for: .touchUpInside)
        }
        
        //change the text for the location secondary screen
        if self.isLocationSecondaryScreen == true {
            self.actionButton.setTitle("Continue", for: .normal)
            self.actionButton.removeTarget(nil, action: nil, for: .allEvents)
            self.actionButton.addTarget(self, action: #selector(OnboardingViewController.locationContinuePressed), for: .touchUpInside)
        }
        
        //change the text for the notification secondary screen
        if self.isNotificationSecondaryScreen == true {
            self.actionButton.setTitle("Continue", for: .normal)
            self.actionButton.removeTarget(nil, action: nil, for: .allEvents)
            self.actionButton.addTarget(self, action: #selector(OnboardingViewController.notificationContinuePressed), for: .touchUpInside)
        }
        
        //change text for last screen
        if self.isLastScreen == true {
            self.actionButton.setTitle("Close", for: .normal)
            //clear other targets
            self.actionButton.removeTarget(nil, action: nil, for: .allEvents)
            //add the new event
            self.actionButton.addTarget(self, action: #selector(OnboardingViewController.closeButtonPressed), for: .touchUpInside)
        }
    }
    
    func locationButtonPressed() {
        //ask the location question
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    func secondaryLocationButtonPressed() {
        //self.showLastPage()
        self.openSettingsApp()
    }
    
    func locationContinuePressed() {
        self.showLastPage()
    }
    
    func notificationContinuePressed() {
        self.showQuestionCards()
    }
    
    func openSettingsApp() {
        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)!
        UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
    }
    
    func closeButtonPressed() {
        self.dismiss(animated: true, completion: {
            AnalyticsService.shared.track(AnalyticsEvent.onboardingCompleted)
            BusinessLocationVisitService.sharedInstance.startMonitoring()
            //LocationService.sharedInstance.startBackgroundTask()
        })
    }
    
    private func setupDashImage() {
        //let dashImage = NudgeStyleKit.imageOfCanvasDashLine()
        //self.dashView.backgroundColor = UIColor(patternImage: dashImage)
        self.dashView.addDashedLineToView(UIColor.white, isWideScreen: self.isLargeScreen)
    }
    
    @IBAction func notificationButtonPressed(_ sender: Any) {
        //register for background notifications
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {(granted, error) -> Void in
            if granted {
                AnalyticsService.shared.track(AnalyticsEvent.onboardingNotificationsEnabled)
                //push screen using main queue
                let main = OperationQueue.main
                main.addOperation {
                    self.showQuestionCards()
                }
            }
            else {
                let main = OperationQueue.main
                main.addOperation {
                    self.showNotificaitonErrorPage()
                }
            }
        })
    }
    
    func showQuestionCards() {
        let storyboard = UIStoryboard(name: "QuestionsViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "QuestionVC")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showNotificaitonErrorPage() {
        let storyBoard = UIStoryboard(name: "OnboardingViewController", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
        viewController.iconImage = UIImage(named: "QuestionIcon")
        viewController.headerString = "Not into notifications?"
        viewController.messageString = "No problem. Nudge is mainly designed to notify you when you are about to enter a business that doesn't match your values, but you can still use the app's great search feature to reveal the truth about companies that interest you.\n\nAnd you can always turn on notifications in your phone's system settings later on.\n\n"
        viewController.isNotificationSecondaryScreen = true
        viewController.isLocationSecondaryScreen = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showLocationSecondaryPage() {
        let storyBoard = UIStoryboard(name: "OnboardingViewController", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
        viewController.iconImage = UIImage(named: "QuestionIcon")
        viewController.headerString = "Sick of location tracking?"
        viewController.messageString = "We get that. Sure the app works best with location services turned on, but it is not required. You can still explore detailed score information for all sorts of companies using the search screen.\n\nAnd if you change your mind about location, just enable it in your phone's settings for apps.\n\n"
        viewController.isLocationSecondaryScreen = true
        viewController.isNotificationSecondaryScreen = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showLastPage() {
        //update preferences
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: AppStrings.locationBool)
        defaults.synchronize()
        //show the last screen
        let storyBoard = UIStoryboard(name: "OnboardingViewController", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
        viewController.iconImage = UIImage(named: "CompassIcon")
        viewController.headerString = "You're all set!"
        viewController.messageString = "Nudge for Change's moral compass is now calibrated with your own.\n\nSoon we'll start sending you alerts so together we can create the change we want to see in the world.\n\n"
        viewController.isLastScreen = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension OnboardingViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            AnalyticsService.shared.track(AnalyticsEvent.onboardingLocationEnabled)
            //show the last screen
            self.showLastPage()
        }
        else if status == .denied {
            self.showLocationSecondaryPage()
        }
    }
}
