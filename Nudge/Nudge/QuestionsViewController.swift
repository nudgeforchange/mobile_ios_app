//
//  QuestionsViewController.swift
//  Nudge
//
//  Created by Thanos on 12/23/16.
//  Copyright © 2016 Level 3 Studios. All rights reserved.
//

import UIKit
import ChameleonFramework
import Koloda
import pop
import UserNotifications
import DeviceKit


class QuestionsViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var disagreeButton: UIButton!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var neutralButton: UIButton!
    
    
    let systemDefaults = UserDefaults.standard
    var cardArray : [String] = []
    var beliefKeyArray : [String] = []
    var beliefs: [String : Int] = [String : Int]()
    let frameAnimationSpringBounciness: CGFloat = 9
    let frameAnimationSpringSpeed: CGFloat = 16
    let kolodaCountOfVisibleCards = 1
    let kolodaAlphaValueSemiTransparent: CGFloat = 0.1
    
    var boolWelcomeDeleted = true
    var boolNotificationDeleted = true
    var userReset : Bool = false
    
    var locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //setup the card array
        /*
         let device = Device()
         let wideDevices : [Device] = [.iPhone6Plus, .iPhone6sPlus, .iPhone7Plus, .simulator(.iPhone6Plus), .simulator(.iPhone6sPlus), .simulator(.iPhone7Plus)]
         if wideDevices.contains(device) {
         cardArray.append("EnvironmentCardWide")
         cardArray.append("FemaleEqualityCardWide")
         cardArray.append("RaceEqualityCardWide")
         cardArray.append("LGBTQEqualityCardWide")
         cardArray.append("WageEqualityCardWide")
         }
         else {
         cardArray.append("EnvironmentCard")
         cardArray.append("FemaleEqualityCard")
         cardArray.append("RaceEqualityCard")
         cardArray.append("LGBTQEqualityCard")
         cardArray.append("WageEqualityCard")
         }
         */
        cardArray.append("qcard_trump")
        cardArray.append("qcard_workers")
        cardArray.append("qcard_environment")
        cardArray.append("qcard_women")
        cardArray.append("qcard_racial")
        cardArray.append("qcard_lgbtq")
        //add the keys for saving scores in preferences
        beliefKeyArray.append(AppSlugs.trump)
        beliefKeyArray.append(AppSlugs.workersrights)
        beliefKeyArray.append(AppSlugs.environment)
        beliefKeyArray.append(AppSlugs.femaleEquality)
        beliefKeyArray.append(AppSlugs.racialEquality)
        beliefKeyArray.append(AppSlugs.lgbtqEquality)
        //setup Stepper
        //self.setupStepper()
        //add the koloda view datasource and delegate
        kolodaView.backgroundColor = UIColor.clear
        kolodaView.delegate = self
        kolodaView.dataSource = self
        //set the background color and add the orange header color
        self.setBackgroundColors()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func disagreeButtonPressed(_ sender: Any) {
        //get the current index
        self.saveScoreForIndex(index: self.kolodaView.currentCardIndex, score: BeliefScores.NO)
        //mimic swipe action
        self.kolodaView.swipe(.left)
    }
    
    @IBAction func neutralButtonPressed(_ sender: Any) {
        self.saveScoreForIndex(index: self.kolodaView.currentCardIndex, score: BeliefScores.SKIP)
        self.kolodaView.swipe(.down)
    }
    @IBAction func agreeButtonPressed(_ sender: Any) {
        self.saveScoreForIndex(index: self.kolodaView.currentCardIndex, score: BeliefScores.YES)
        kolodaView.swipe(.right)
    }
    
    //MARK : Score Saving
    func saveScoreForIndex(index : Int, score : BeliefScores) {
        //get the key string
        let keyString = self.beliefKeyArray[index]
        self.beliefs[keyString] = score.rawValue
    }
    
}

extension QuestionsViewController : KolodaViewDataSource, KolodaViewDelegate {
    //MARK: Koloda View Delegate/Datasource
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        //UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.cardArray.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let cardName : String = self.cardArray[index]
        let imageView = UIImageView(image: UIImage(named: cardName))
        imageView.contentMode = .scaleAspectFit
        //imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return imageView
    }
    
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        
    }
    
    func kolodaSwipeThresholdRatioMargin(_ koloda: KolodaView) -> CGFloat? {
        return 0.75
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("CustomOverlayView", owner: self, options: nil)?[0] as? OverlayView
        
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right, .down]
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        if direction == .left {
            self.saveScoreForIndex(index: index, score: BeliefScores.NO)
        }
        else if direction == .right {
            self.saveScoreForIndex(index: index, score: BeliefScores.YES)
        }
        else if direction == .down {
            self.saveScoreForIndex(index: index, score: BeliefScores.SKIP)
        }
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        AnalyticsService.shared.track(AnalyticsEvent.onboardingBeliefsSet)
        
        SettingsService.sharedInstance.beliefs = self.beliefs
        SettingsService.sharedInstance.didCompleteOnboarding = true
        //if from reset close screen and go back to menu
        if self.userReset == true {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            //after the last card is shown, close the onboarding screen
            //show the last step screen
            let storyBoard = UIStoryboard(name: "OnboardingViewController", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "OnboardingImageVC") as! OnboardingViewController
            viewController.iconImage = UIImage(named: "LocationIcon")
            viewController.headerString = "One last thing..."
            viewController.messageString = "Click the button below to turn on your phone's location services, then you're all set to be nudged in the right direction.\n\nAnd don't worry, if you prefer not to enable location services, the app still offers useful features.\n\n\n\n"
            viewController.isBulletList = false
            viewController.hideButton = false
            viewController.isLocationScreen = true
            viewController.isLastScreen = false
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        //self.dismiss(animated: true, completion: nil)
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
}
