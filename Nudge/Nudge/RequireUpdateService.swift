import UIKit

class RequireUpdateService: NSObject {
    
    static func requireUpdate() {
        //BusinessLocationVisitService.sharedInstance.stopMonitoring()
        
        let alert = UIAlertController(title: "Update Required", message: "Update to the latest version of this app to continue nudging for change!", preferredStyle: .alert)
        
        let application = UIApplication.shared
        let appDelegate = application.delegate as! AppDelegate
        if let rootVC = appDelegate.window?.rootViewController {
            if let presentedVC = rootVC.presentedViewController {
                presentedVC.dismiss(animated: true, completion: {
                    rootVC.present(alert, animated: true, completion: nil)
                })
            } else {
                rootVC.present(alert, animated: true, completion: nil)
            }
        }
    }

}
