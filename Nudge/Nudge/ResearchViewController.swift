//
//  ResearchViewController.swift
//  Nudge
//
//  Created by Ultron on 2/13/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import Alamofire

let commentPlaceholder = "Comments"

class ResearchViewController: UIViewController {
    
    var alternatePlace : BusinessLocation!
    
    class ResearchInfo : NSObject {
        var title = ""
        var environmentOption = false
        var racialEqualityOption = false
        var workersOptions = false
        var lgbtqEqualityOption = false
        var womensEqualityOption = false
        var comments = ""
    }
    
    var researchInfo = ResearchInfo()
    var dataSource = TKDataFormEntityDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Research Request"
        self.setupBackgroundColor()
        //add close button
        let closeButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(ResearchViewController.closeButtonPressed))
        self.navigationItem.leftBarButtonItem = closeButton
        //create data form
        let dataForm = TKDataForm(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width-16, height: self.view.frame.size.height-50))
        dataForm.groupSpacing = 12
        dataForm.commitMode = .immediate
        dataForm.delegate = self
        dataForm.tintColor = AppColors.nudgeOrangePrimaryColor
        //setup the data source
        self.researchInfo.title = "We don't have info on \(self.alternatePlace.name) yet. Enter a message and hit submit if you'd like us to research and review \(self.alternatePlace.name)"
        dataSource.sourceObject = researchInfo
        //update datasource editors
        dataSource["title"].readOnly = true
        dataSource["title"].editorClass = TKDataFormMultilineTextEditor.self 
        dataSource["comments"].editorClass = TKDataFormMultilineTextEditor.self
        dataSource["environmentOption"].image = NudgeStyleKit.imageOfCanvasGlobe()
        dataSource["environmentOption"].displayName = "Environment"
        dataSource["racialEqualityOption"].image = NudgeStyleKit.imageOfCanvasHandShake()
        dataSource["racialEqualityOption"].displayName = "Racial Equality"
        dataSource["workersOptions"].image = NudgeStyleKit.imageOfCanvasWorker()
        dataSource["workersOptions"].displayName = "Workers Rights"
        dataSource["lgbtqEqualityOption"].image = NudgeStyleKit.imageOfCanvasHeart()
        dataSource["lgbtqEqualityOption"].displayName = "LGBTQ Equality"
        dataSource["womensEqualityOption"].image = NudgeStyleKit.imageOfCanvasFemale()
        dataSource["womensEqualityOption"].displayName = "Womens Equality"
        dataSource.addGroup(withName: "", propertyNames: ["title"])
        dataSource.addGroup(withName: "Research Issues", propertyNames: ["environmentOption", "racialEqualityOption", "workersOptions", "lgbtqEqualityOption", "womensEqualityOption"])
        dataSource.addGroup(withName: "Optional Comments", propertyNames: ["comments"])
        //add the data source to the form
        dataForm.dataSource = dataSource
        //add form to view
        self.view.addSubview(dataForm)
        //add the submit button to the bottom
        let buttonContainer = UIView(frame: CGRect(x: 8, y: dataForm.frame.size.height, width: self.view.frame.size.width-16, height: 50))
        buttonContainer.backgroundColor = UIColor.white
        let submitButton = UIButton(frame: CGRect(x: 8, y: 0, width: buttonContainer.frame.size.width-16, height: 44))
        submitButton.setTitle("Submit", for: .normal)
        submitButton.setTitleColor(UIColor.white, for: .normal)
        submitButton.backgroundColor = AppColors.nudgeOrangePrimaryColor
        submitButton.layer.cornerRadius = 3
        submitButton.layer.masksToBounds = true
        submitButton.addTarget(self, action: #selector(ResearchViewController.submitButtonPressed), for: .touchUpInside)
        buttonContainer.addSubview(submitButton)
        self.view.addSubview(buttonContainer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupBackgroundColor() {
        let colors : [UIColor] = [AppColors.nudgeOrangeSecondaryColor, AppColors.nudgeOrangePrimaryColor, AppColors.nudgeOrangePrimaryColor]
        let gradientColor = UIColor(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: colors)
        self.view.backgroundColor = gradientColor
    }
    
    func closeButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func submitButtonPressed() {
        //self.showIndicator("Submitting request...")
        //get the values that were entered
        let researchItems = self.researchInfo
        let environmentChoice = researchItems.environmentOption
        let raceChoice = researchItems.racialEqualityOption
        let workerChoice = researchItems.workersOptions
        let lgbtqChoice = researchItems.lgbtqEqualityOption
        let womensChoice = researchItems.womensEqualityOption
        let comments = researchItems.comments.characters.count > 0 ? researchItems.comments : "No comments entered"
        //build the issue list
        var issueList : [String] = []
        if environmentChoice == true {
            issueList.append(AppSlugs.environment)
        }
        if raceChoice == true {
            issueList.append(AppSlugs.racialEquality)
        }
        if workerChoice == true {
            issueList.append(AppSlugs.workersrights)
        }
        if lgbtqChoice == true {
            issueList.append(AppSlugs.lgbtqEquality)
        }
        if womensChoice == true {
            issueList.append(AppSlugs.femaleEquality)
        }
        //let issueString = issueList.joined(separator: ",")
        //get the most recent location
        /*
        LocationService.sharedInstance.getCurrentLocation(completion: {currentLocation in
            let locationDict : [String : String] = ["lat" : currentLocation.coordinate.latitude.description,
                                                    "lng" : currentLocation.coordinate.longitude.description]
            //setup the parameters
            let parameters : Parameters = ["location" : locationDict,
                                           "issues" : issueList,
                                           "comments" : comments]
            //call the API
            BusinessService.sharedInstance.submitResearchRequest(self.alternatePlace.placeId, parameters: parameters, parent : self)
        })
        */
    }
    
    func finishSendingRequest() {
        //self.closeIndicator()
        self.dismiss(animated: true, completion: nil)
    }

}

extension ResearchViewController : TKDataFormDelegate {
    
    func dataForm(_ dataForm: TKDataForm, setupEditor editor: TKDataFormEditor, for property: TKEntityProperty) {
        let font = UIFont(name: "Lato-Regular", size: 14)!
        editor.textLabel.font = font
        editor.style.imageViewSize = CGSize(width: 22 , height: 22)
        //let gridLayout = editor.gridLayout
        let feedbackLabel = editor.feedbackLabel
        feedbackLabel.font = font
        
        if property.name == "title" {
            //hide the label
            editor.style.textLabelDisplayMode = .hidden
            //update layout
            let titleDef = editor.gridLayout.definition(for: editor.textLabel)
            editor.gridLayout.setWidth(0, forColumn: (titleDef?.column.intValue)!)
            let textView = editor as! TKDataFormMultilineTextEditor
            textView.textView.font = font
        }
        if ["environmentOption", "racialEqualityOption", "workersOptions", "lgbtqEqualityOption", "womensEqualityOption"].contains(property.name) {
            editor.style.textLabelOffset = UIOffsetMake(20, 0)
            let switchEditor = editor as! TKDataFormSwitchEditor
            switchEditor.switchView.tintColor = AppColors.nudgeOrangePrimaryColor
        }
        
        if property.name == "comments" {
            editor.style.textLabelDisplayMode = .hidden
            let titleDef = editor.gridLayout.definition(for: editor.textLabel)
            editor.gridLayout.setWidth(0, forColumn: (titleDef?.column.intValue)!)
            editor.style.insets = UIEdgeInsetsMake(1, 8, 1, 8)
            let textView = editor as! TKDataFormMultilineTextEditor
            textView.textView.font = font
        }
    }
    
    func dataForm(_ dataForm: TKDataForm, update groupView: TKEntityPropertyGroupView, forGroupAt groupIndex: UInt) {
        let font = UIFont(name: "Lato-Bold", size: 16)
        groupView.titleView.titleLabel.font = font
    }
    
    func dataForm(_ dataForm: TKDataForm, heightForHeaderInGroup groupIndex: UInt) -> CGFloat {
        if groupIndex == 0 {
            return 2
        }
        return 20
    }
    
    func dataForm(_ dataForm: TKDataForm, heightForEditorInGroup groupIndex: UInt, at editorIndex: UInt) -> CGFloat {
        if groupIndex == 0 || groupIndex == 2 {
            return 80
        }
        return 44
    }
}
