//
//  ReviewKit.swift
//  Nudge
//
//  Created by Thanos on 10/4/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation
import StoreKit

let runIncrementSetting = "numberOfRuns"
let minimumRunCount = 5

class ReviewKit {
    
    public static let sharedInstance = ReviewKit()
    
    func getRunCounts() -> Int {
        let defaults = UserDefaults.standard
        let currentCount = defaults.value(forKey: runIncrementSetting)
        if currentCount != nil {
            return currentCount as! Int
        }
        return 0
    }
    
    func updateRunCount() {
        let defaults = UserDefaults.standard
        let currentCount = getRunCounts() + 1
        defaults.setValue(currentCount, forKey: runIncrementSetting)
        defaults.synchronize()
        showReview()
    }
    
    func showReview() {
        let runCount = getRunCounts()
        let value = runCount % minimumRunCount
        if value == 0 {
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                // Fallback on earlier versions
            }
        }
    }
}
