//
//  RoundedButton.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/24/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = self.bounds.size.height/2
        if let imageView = self.imageView {
            imageView.contentMode = .scaleAspectFit
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
