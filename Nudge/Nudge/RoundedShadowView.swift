//
//  RoundedShadowView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/20/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class RoundedShadowView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 4
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
    }

}
