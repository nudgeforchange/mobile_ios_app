//
//  ScoreCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/19/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class ScoreCell: UICollectionViewCell {
    
    @IBOutlet weak var scoreImageView: UIImageView!
    @IBOutlet weak var issueImageView: UIImageView!
    @IBOutlet weak var issueLabel: UILabel!
    
    var issue: BusinessIssueScore! {
        didSet {
            self.scoreImageView.image = UIImage(named: "radialscore_\(issue.score)")
            self.issueImageView.image = issue.issueImage
            self.issueImageView.tintColor = AppColors.tintColorForScore(issue.score)
            self.issueLabel.text = issue.name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
