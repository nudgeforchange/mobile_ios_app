//
//  BusinessCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/15/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class ScoredBusinessCell: UITableViewCell {
    
    @IBOutlet weak var roundedShadowView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var disclosureIndicatorView: UIImageView!
    @IBOutlet weak var scoreCollectionView: UICollectionView!
    @IBOutlet weak var nudgingAwayButton: RoundedButton!
    
    @IBOutlet weak var nudgingAwayView: NudgingAwayView!
    
    var businessLocation: BusinessLocation! {
        didSet {
            // configure cell
//            self.businessNameLabel.text = "\(self.businessLocation.canonicalName)/\(self.businessLocation.name)" // debug
             self.businessNameLabel.text = "\(self.businessLocation.canonicalName)"
//            let tintColor = AppColors.tintColorForScore(Int(round(businessLocation.score)))
//            self.businessNameLabel.textColor = tintColor
//            self.disclosureIndicatorView.tintColor = tintColor
            self.issues = self.businessLocation.issues
        }
    }
    
    var issues: [BusinessIssueScore]! {
        didSet {
            self.scoreCollectionView.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.nudgingAwayButton.imageView?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        self.nudgingAwayButton.imageView?.contentMode = .scaleAspectFit
        self.nudgingAwayButton.setNeedsLayout()
        
        self.nudgingAwayView.collapse()
        
        self.backgroundColor = UIColor.clear
        self.scoreCollectionView.backgroundColor = UIColor.clear
        
        let scoreCellNib: UINib = UINib(nibName: "ScoreCell", bundle: nil)
        self.scoreCollectionView.register(scoreCellNib, forCellWithReuseIdentifier: "ScoreCell")
        let spacerNib: UINib = UINib(nibName: "ScoreSpacerCell", bundle: nil)
        self.scoreCollectionView.register(spacerNib, forCellWithReuseIdentifier: "ScoreSpacerCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.roundedShadowView.backgroundColor = selected ? UIColor(white: 0.95, alpha: 1) : UIColor.white
    }
}

extension ScoredBusinessCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let issues = self.issues {
            return (min(issues.count, 3) * 2) - 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row % 2 == 0 {
            let issue: BusinessIssueScore = self.issues[indexPath.row/2]
            let cell: ScoreCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScoreCell", for: indexPath) as! ScoreCell
            cell.issue = issue
            return cell
        } else {
            let cell: ScoreSpacerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScoreSpacerCell", for: indexPath) as! ScoreSpacerCell
            return cell
        }
    }
}

extension ScoredBusinessCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row % 2 == 0 {
            return CGSize(width: Int(collectionView.bounds.size.width / 3) - 1, height: Int(collectionView.bounds.size.height))
        } else {
            return CGSize(width: 1, height: Int(collectionView.bounds.size.height))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let cellWidth: CGFloat = (collectionView.bounds.size.width / 3) - 1
        let numberOfItems: Int = (collectionView.numberOfItems(inSection: section) + 1)/2
        let totalCellWidth: CGFloat = (cellWidth * CGFloat(numberOfItems)) + CGFloat(numberOfItems - 1)
        
        let leftInset = (collectionView.bounds.size.width - totalCellWidth) / 2;
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
}
