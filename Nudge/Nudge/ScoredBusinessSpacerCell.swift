//
//  SpacerTableViewCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/19/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class ScoredBusinessSpacerCell: UITableViewCell {
    
    @IBOutlet weak var colorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
