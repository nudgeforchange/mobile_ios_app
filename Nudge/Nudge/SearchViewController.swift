//
//  SearchViewController.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/12/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Alamofire
import ChameleonFramework
import SwiftDate

class SearchViewController: UIViewController {
    
    public class SearchData : NSObject {
        var searchText : String = ""
        var locationName : String = ""
        var currentLocation : CLLocation?
        var isNonCoordLoc : Bool = false
    }
    
    
    @IBOutlet weak var nameSearchField: UITextField!
    @IBOutlet weak var locationSearchField: UITextField!
    @IBOutlet weak var mainTable: UITableView!
    
    var disclaimerFooterView = UINib(nibName: "DisclaimerFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DisclaimerFooterView
    
    
    let NAMESEARCHTAG = 0
    let LOCATIONSEARCHTAG = 1
    
    var dataSource : [BusinessLocation] = []
    var scoredDataSource : [BusinessLocation] = []
    var unscoredDataSource : [BusinessLocation] = []
    var locationDataSource : [LocationSearchResult] = []
    var nameDataSource : [BusinessSearchResult] = []
    var isLocationSearchActive : Bool = false
    var isNameSearchActive : Bool = false
    var currentSearch = SearchData()
    var locationSearchPlaceholder = "using current location"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        //self.view.backgroundColor = AppColors.nudgeRedColor
        self.setupGradientBackground()
        self.mainTable.backgroundColor = AppColors.nudgeWhiteColor
        self.mainTable.delegate = self
        self.mainTable.dataSource = self
        self.mainTable.emptyDataSetSource = self
        self.mainTable.emptyDataSetDelegate = self
        //setup the text fields
        let searchImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 16))
        searchImageView.contentMode = .scaleAspectFit
        searchImageView.tintColor = AppColors.nudgeGrayColor
        searchImageView.image = UIImage(named: "SearchIcon")
        nameSearchField.delegate = self
        nameSearchField.tag = NAMESEARCHTAG
        nameSearchField.leftViewMode = .always
        nameSearchField.leftView = searchImageView
        nameSearchField.placeholder = "search companies"
        nameSearchField.tintColor = AppColors.nudgeGrayColor
        nameSearchField.textColor = AppColors.nudgeGrayColor
        nameSearchField.returnKeyType = .search
        nameSearchField.clearButtonMode = .whileEditing
        nameSearchField.inputAccessoryView = self.addKeyboardTool()
        let locationImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 16))
        locationImageView.contentMode = .scaleAspectFit
        locationImageView.tintColor = AppColors.nudgeBlueColor
        locationImageView.image = UIImage(named: "LocationEmptyIcon")
        locationSearchField.delegate = self
        locationSearchField.tag = LOCATIONSEARCHTAG
        locationSearchField.leftViewMode = .always
        locationSearchField.leftView = locationImageView
        locationSearchField.tintColor = AppColors.nudgeBlueColor
        locationSearchField.textColor = AppColors.nudgeBlueColor
        locationSearchField.text = self.locationSearchPlaceholder
        locationSearchField.returnKeyType = .search
        locationSearchField.clearButtonMode = .whileEditing
        locationSearchField.inputAccessoryView = self.addKeyboardTool()
        //set the current search on first load
        self.setupCurrentLocation()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupGradientBackground() {
        let viewHeight = self.mainTable.frame.origin.y
        let colors = [AppColors.nudgeYellowColor, AppColors.nudgePurpleColor]
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: viewHeight)
        let gradient = UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: colors)
        self.view.backgroundColor = gradient
    }
    
    func setupCurrentLocation() {
        let locationManager = CLLocationManager()
        if let currentLocation = locationManager.location {
        //LocationService.sharedInstance.getCurrentLocation(completion: {(location) in
            self.currentSearch.locationName = self.locationSearchPlaceholder
            self.currentSearch.currentLocation = currentLocation
            self.currentSearch.isNonCoordLoc = false
            self.currentSearch.searchText = ""
            self.performBusinessSearch()
       // })
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let currentLocation = self.currentSearch.currentLocation {
            let lastTime = currentLocation.timestamp
            let difference = lastTime.timeIntervalSinceNow.in(.minute)!
            if difference > 10 {
                self.setupCurrentLocation()
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.cancelOtherSearches()
    }
    
    func addKeyboardTool() -> UIToolbar {
        //setup the cancel bar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = AppColors.nudgeOragneColor
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(SearchViewController.cancelButtonPressed))
        let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexButton,cancelButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
    func cancelButtonPressed() {
        self.nameSearchField.resignFirstResponder()
        self.locationSearchField.resignFirstResponder()
        //check if location field is empty and default to "current location"
        if let text = self.locationSearchField.text, text.isEmpty {
            print("location field is empty")
            self.locationSearchField.text = self.currentSearch.locationName
        }
    }
    
    func clearDataSources() {
        self.dataSource.removeAll()
        self.locationDataSource.removeAll()
        self.nameDataSource.removeAll()
        self.scoredDataSource.removeAll()
        self.unscoredDataSource.removeAll()
        self.mainTable.reloadData()
        self.mainTable.tableFooterView = nil
    }
    
    func showStatusBarActivity() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func dismissStatusBarActivity() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func cancelOtherSearches() {
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler({(dataTasks, uploadTasks, downloadTasks) in
            dataTasks.forEach({$0.cancel()})
        })
        
        //let sessionManager = Alamofire.SessionManager.default
        //sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in dataTasks.forEach { $0.cancel() } uploadTasks.forEach { $0.cancel() } downloadTasks.forEach { $0.cancel() } }
    }
    
    func performBusinessSearch() {
        print("searching for businesses")
        AnalyticsService.shared.track(AnalyticsEvent.searchedBusinesses)
        
        self.clearDataSources()
        self.showStatusBarActivity()
        BusinessService.sharedInstance.fetchNearbyBusinesses(searchData: self.currentSearch) { bussinessLocations in
            print("search done")
            if bussinessLocations.count > 0 {
                for place in bussinessLocations {
                    if place.score >= 0 {
                        self.scoredDataSource.append(place)
                    }
                    else {
                        self.unscoredDataSource.append(place)
                    }
                }
            }
            self.searchDone()
        }
    }
    
    func searchDone() {
        self.dismissStatusBarActivity()
        self.mainTable.reloadData()
        self.mainTable.tableFooterView = self.disclaimerFooterView
    }
    
    func performNameAutoCompleteSearch() {
        self.clearDataSources()
        self.showStatusBarActivity()
        BusinessService.sharedInstance.businessSearchAutoComplete(searchData: self.currentSearch, completion: { businessList in
            if businessList.count > 0 {
                self.dismissStatusBarActivity()
                self.nameDataSource = businessList
                self.mainTable.reloadData()
                self.mainTable.tableFooterView = self.disclaimerFooterView
            }
        })
    }
    
    func performLocationAutoCompleteSearch() {
        self.clearDataSources()
        self.showStatusBarActivity()
        BusinessService.sharedInstance.locationSearchAutoComplete(searchData: self.currentSearch, completion: { locationList in
            if locationList.count > 0 {
                self.dismissStatusBarActivity()
                self.locationDataSource = locationList
                self.mainTable.reloadData()
                self.mainTable.tableFooterView = self.disclaimerFooterView
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isLocationSearchActive == true {
            return 1
        }
        else if self.isNameSearchActive == true {
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isLocationSearchActive == true {
            return self.locationDataSource.count
        }
        else if self.isNameSearchActive == true {
            return self.nameDataSource.count
        }
        else if section == 0 {
            return self.scoredDataSource.count
        }
        return self.unscoredDataSource.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.isLocationSearchActive == true && self.locationDataSource.count > 0 {
            return "location results"
        }
        else if self.isNameSearchActive == true && self.nameDataSource.count > 0 {
            return "search results"
        }
        else if section == 0 && self.scoredDataSource.count > 0 {
            return "nearby companies"
        }
        else if section == 1 && self.unscoredDataSource.count > 0 {
            return "unscored results"
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mainTable.dequeueReusableCell(withIdentifier: "cell")!
        let scoredCell = mainTable.dequeueReusableCell(withIdentifier: "scoredCell")!
        let unscoredCell = mainTable.dequeueReusableCell(withIdentifier: "unscoredCell")!
        let locationCell = mainTable.dequeueReusableCell(withIdentifier: "locationCell")!
        
        if self.isLocationSearchActive == true {
            locationCell.backgroundColor = UIColor.clear
            let containerView = locationCell.viewWithTag(99)!
            let nameLabel = locationCell.viewWithTag(98) as! UILabel
            let colorView = locationCell.viewWithTag(95)!
            let separatorView = locationCell.viewWithTag(94)!
            containerView.backgroundColor = UIColor.white
            //locationCell.layer.shadowColor = AppColors.lightTextColor.cgColor
            //locationCell.layer.shadowOffset = CGSize(width: 2, height: 2)
            //locationCell.layer.shadowOpacity = 0.5
            //locationCell.layer.shadowRadius = 8
            //setup labels
            nameLabel.textColor = AppColors.darkTextColor
            colorView.backgroundColor = AppColors.nudgeBlueColor
            separatorView.backgroundColor = AppColors.nudgeMutedGrayColor
            if indexPath.row == tableView.numberOfRows(inSection: 0)-1 {
                separatorView.backgroundColor = AppColors.nudgeMutedGrayColor.withAlphaComponent(0.5)
            }
            nameLabel.text = self.locationDataSource[indexPath.row].name
            return locationCell
        }
            
        else if self.isNameSearchActive == true {
            scoredCell.backgroundColor = UIColor.clear
            unscoredCell.backgroundColor = UIColor.clear
            let containerView = unscoredCell.viewWithTag(99)!
            let nameLabel = unscoredCell.viewWithTag(98) as! UILabel
            let distanceLabel = unscoredCell.viewWithTag(97) as! UILabel
            let addressLabel = unscoredCell.viewWithTag(96) as! UILabel
            let colorView = unscoredCell.viewWithTag(95)!
            let separatorView = unscoredCell.viewWithTag(94)!
            let infoButton = unscoredCell.viewWithTag(93) as! UIButton
            infoButton.tintColor = AppColors.nudgeMutedGrayColor
            containerView.backgroundColor = UIColor.white
            
            //setup labels
            nameLabel.textColor = AppColors.darkTextColor
            distanceLabel.textColor = AppColors.nudgeMutedGrayColor
            addressLabel.textColor = AppColors.lightTextColor
            
            colorView.backgroundColor = AppColors.nudgeMutedGrayColor
            separatorView.backgroundColor = AppColors.nudgeMutedGrayColor
            if indexPath.row == tableView.numberOfRows(inSection: 0)-1 {
                separatorView.backgroundColor = AppColors.nudgeMutedGrayColor.withAlphaComponent(0.5)
            }
            
            let dataItem = self.nameDataSource[indexPath.row]
            nameLabel.text = dataItem.placeName
            addressLabel.text = dataItem.placeAddress
            
            return unscoredCell
            
        }
            
        else if indexPath.section == 0 {
            scoredCell.backgroundColor = UIColor.clear
            let containerView = scoredCell.viewWithTag(99)!
            let nameLabel = scoredCell.viewWithTag(98) as! UILabel
            let distanceLabel = scoredCell.viewWithTag(97) as! UILabel
            let addressLabel = scoredCell.viewWithTag(96) as! UILabel
            let colorView = scoredCell.viewWithTag(95)!
            let separatorView = scoredCell.viewWithTag(94)!
            let arrowImageView = scoredCell.viewWithTag(93) as! UIImageView
            arrowImageView.tintColor = AppColors.nudgeMutedGrayColor
            containerView.backgroundColor = UIColor.white
            
            separatorView.backgroundColor = AppColors.nudgeMutedGrayColor
            if indexPath.row == tableView.numberOfRows(inSection: 0)-1 {
                separatorView.backgroundColor = AppColors.nudgeMutedGrayColor.withAlphaComponent(0.5)
            }
            //setup labels
            nameLabel.textColor = AppColors.darkTextColor
            distanceLabel.textColor = AppColors.nudgeMutedGrayColor
            addressLabel.textColor = AppColors.lightTextColor
            
            let alternatePlace = self.scoredDataSource[indexPath.row]
            nameLabel.text = alternatePlace.name
            let distanceString = String(format: "%.2f", alternatePlace.distance)
            distanceLabel.text = "\(distanceString) miles"
            addressLabel.text = alternatePlace.address
            let averageScore = alternatePlace.score
            let lowestIssueScore = alternatePlace.lowestIssueScore
            colorView.backgroundColor = AppColors.nudgeMutedGrayColor
            
            if lowestIssueScore < 5 {
                colorView.backgroundColor = AppColors.nudgeRedColor
            } else if 5..<7.5 ~= averageScore {
                colorView.backgroundColor = AppColors.nudgeYellowColor
            } else if 7.5...10 ~= averageScore {
                colorView.backgroundColor = AppColors.nudgeGreenColor
            }
            
            return scoredCell
        }
            
        else if indexPath.section == 1 {
            unscoredCell.backgroundColor = UIColor.clear
            let containerView = unscoredCell.viewWithTag(99)!
            let nameLabel = unscoredCell.viewWithTag(98) as! UILabel
            let distanceLabel = unscoredCell.viewWithTag(97) as! UILabel
            let addressLabel = unscoredCell.viewWithTag(96) as! UILabel
            let colorView = unscoredCell.viewWithTag(95)!
            let separatorView = unscoredCell.viewWithTag(94)!
            let infoButton = unscoredCell.viewWithTag(93) as! UIButton
            infoButton.tintColor = AppColors.nudgeMutedGrayColor
            containerView.backgroundColor = UIColor.white
            colorView.backgroundColor = AppColors.nudgeMutedGrayColor
            separatorView.backgroundColor = AppColors.nudgeMutedGrayColor
            if indexPath.row == tableView.numberOfRows(inSection: 0)-1 {
                separatorView.backgroundColor = AppColors.nudgeMutedGrayColor.withAlphaComponent(0.5)
            }
            //setup labels
            nameLabel.textColor = AppColors.darkTextColor
            distanceLabel.textColor = AppColors.nudgeMutedGrayColor
            addressLabel.textColor = AppColors.lightTextColor
            
            let alternatePlace = self.unscoredDataSource[indexPath.row]
            nameLabel.text = alternatePlace.name
            let distanceString = String(format: "%.2f", alternatePlace.distance)
            distanceLabel.text = "\(distanceString) miles"
            addressLabel.text = alternatePlace.address
            return unscoredCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //if location list is showing add the selected location to the search field
        if self.isLocationSearchActive == true {
            //print("did select location cell")
            let selectedLocation = self.locationDataSource[indexPath.row]
            self.currentSearch.locationName = selectedLocation.name
            self.currentSearch.isNonCoordLoc = true
            self.isLocationSearchActive = false
            self.locationSearchField.text = self.currentSearch.locationName
            self.performBusinessSearch()
        }
        else if self.isNameSearchActive == true {
            
        }
        else if indexPath.section == 0 {
            //get the selected scored item
            let selectedItem = self.scoredDataSource[indexPath.row]
            
            let storyBoard = UIStoryboard(name: "NudgeDetailViewController", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "NudgeDetailVC") as! NudgeDetailViewController
            viewController.businessLocation = BusinessLocation(business: selectedItem.business, businessLocationUUID: selectedItem.businessLocationUUID, placeId: selectedItem.placeId, name: selectedItem.name, canonicalName: selectedItem.canonicalName, address: selectedItem.address, latitude: selectedItem.location.latitude, longitude: selectedItem.location.longitude, geofenceRadius: selectedItem.geofenceRadius, score: selectedItem.score, issues: selectedItem.issues, scoredAlternatives: [], unscoredAlternatives: [], dateAdded: Date(), shouldMonitor: false)
            viewController.businessSearchLocation = selectedItem
            viewController.nudgeDetailType = selectedItem.lowestIssueScore >= 5 ? NudgeDetailType.searchResultHigh : NudgeDetailType.searchResultLow
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if indexPath.section == 1 {
            //unscored items go straight to the map
            let selectedItem = self.unscoredDataSource[indexPath.row]
            MapsService.openMapApp(address: selectedItem.address)
        }
    }
}

extension SearchViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == NAMESEARCHTAG {
            //print("started editing name search field")
        }
        else if textField.tag == LOCATIONSEARCHTAG {
            //clear the location search field is still showing "current location"
            if self.locationSearchField.text == self.locationSearchPlaceholder {
                self.locationSearchField.text = ""
            }
            //print("started editing location search field")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.cancelOtherSearches()
        
        let nameText = self.nameSearchField.text
        if nameText != nil && nameText!.characters.count > 0 {
            self.currentSearch.searchText = nameText!
            self.isNameSearchActive = true
        }
        else
        {
            self.isNameSearchActive = false
            self.currentSearch.searchText = ""
        }
        
        if textField === self.nameSearchField {
            self.isNameSearchActive = true
        }
        
        //get the text entered
        let locationText = self.locationSearchField.text
        if locationText != nil && locationText!.characters.count > 0 && locationText! != self.locationSearchPlaceholder {
            self.isLocationSearchActive = true
            self.currentSearch.locationName = locationText!
            self.currentSearch.isNonCoordLoc = true
        }
        else {
            self.isLocationSearchActive = true
            self.currentSearch.locationName = self.locationSearchPlaceholder
            self.currentSearch.isNonCoordLoc = false
            self.locationSearchField.text = self.currentSearch.locationName
        }
        
        if self.isNameSearchActive == true && self.isLocationSearchActive == false {
            print("only the name field was changed")
            self.performNameAutoCompleteSearch()
        }
        else if self.isNameSearchActive == false && self.isLocationSearchActive == true {
            print("only the location field was changed")
            self.performLocationAutoCompleteSearch()
        }
        else if self.isNameSearchActive == true && self.isLocationSearchActive == true {
            print("both fields were changed")
            self.isNameSearchActive = false
            self.isLocationSearchActive = false
            self.performBusinessSearch()
            
        }
        textField.resignFirstResponder()
        return true
    }
    
}

extension SearchViewController : DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "graphic_nudgehog-sleuth")?.alpha(0.5)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string = "Nudgehog is looking for places"
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = .center
        let attributes = [NSFontAttributeName : UIFont(name: AppFontNames.regularFont, size: 16)!,
                          NSForegroundColorAttributeName : AppColors.nudgeMutedGrayColor,
                          NSParagraphStyleAttributeName : paragraphStyle]
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView!) -> UIColor! {
        return AppColors.nudgeWhiteColor
    }
}

extension SearchViewController : DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}
