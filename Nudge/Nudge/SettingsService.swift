import KeychainSwift

class SettingsService: NSObject {
    static let sharedInstance = SettingsService()
    
    var currentVersion: String? {
        get {
            return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        }
    }
    
    let deviceVendorId: String = {
        let defaults = UserDefaults.standard
        var deviceVendorId: String? = defaults.object(forKey: "DeviceVendorId") as? String
        if deviceVendorId == nil {
            deviceVendorId = UIDevice.current.identifierForVendor?.uuidString
            defaults.set(deviceVendorId, forKey: "DeviceVendorId")
            defaults.synchronize()
        }
        return deviceVendorId!
    }()
    
    var nudgeUserId: String? {
//        let defaults = UserDefaults.standard
//        var nudgeUserId: String? = defaults.object(forKey: "NudgeUserId") as? String
//        if nudgeUserId == nil {
//            let keychain = KeychainSwift()
//            nudgeUserId = keychain.get("NudgeUserId")
//            if nudgeUserId == nil {
//                nudgeUserId = UUID().uuidString
//                defaults.set(nudgeUserId, forKey: "NudgeUserId")
//                defaults.synchronize()
//                keychain.synchronizable = true
//                keychain.set(nudgeUserId!, forKey: "NudgeUserId", withAccess: .accessibleAfterFirstUnlock)
//            }
//        }
        get {
            let keychain = KeychainSwift()
            keychain.synchronizable = true
            var nudgeUserId: String? = keychain.get("NudgeUserId")
            if keychain.lastResultCode == 0 || keychain.lastResultCode == -25300 {
                if nudgeUserId == nil {
                    nudgeUserId = UUID().uuidString
                    keychain.set(nudgeUserId!, forKey: "NudgeUserId", withAccess: .accessibleAlways)
                }
                return nudgeUserId!
            } else {
                return nil
            }
        }
    }
    
    var nudgeToken: String? = {
        let defaults = UserDefaults.standard
        var nudgeToken: String? = defaults.object(forKey: "NudgeToken") as? String
        return nudgeToken
    }()
    {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(nudgeToken, forKey: "NudgeToken")
            defaults.synchronize()
        }
    }
    
    var nudgeTokenCreatedAt: Date? = {
        let defaults = UserDefaults.standard
        var date: Date? = defaults.object(forKey: "NudgeTokenCreatedAt") as? Date
        return date
        }()
        {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(nudgeTokenCreatedAt, forKey: "NudgeTokenCreatedAt")
            defaults.synchronize()
        }
    }
    
    var distanceFilter: Double = {
        let defaults = UserDefaults.standard
        let locationOption = defaults.double(forKey: "DistanceFilter")
        return locationOption == 0 ? 100 : locationOption
    }()
    {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(distanceFilter, forKey: "DistanceFilter")
            defaults.synchronize()
            
            if self.didCompleteOnboarding {
                // TODO: - SwiftLocation doesn't take account of distance filters when running in background
                BusinessLocationVisitService.sharedInstance.updateDistanceFilter(distanceFilter)
            }
        }
    }
    
    var beliefs: [String : Int]? = {
        let defaults = UserDefaults.standard
        var beliefs: [String : Int]? = defaults.object(forKey: "Beliefs") as? [String : Int]
        return beliefs
    }()
    {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(beliefs, forKey: "Beliefs")
            defaults.synchronize()
            BeliefsService.sharedInstance.postBeliefs(beliefs!)
        }
    }
    
    var didCompleteOnboarding: Bool = {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: AppStrings.questionaireBool)
    }()
    {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(didCompleteOnboarding, forKey: AppStrings.questionaireBool)
            defaults.synchronize()
        }
    }
    
    private override init() {}
    
    
}
