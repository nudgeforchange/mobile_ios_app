import Social

class SharingService: NSObject {
    
    static func isServiceAvailable(_ serviceType: String!) -> Bool {
        return SLComposeViewController.isAvailable(forServiceType: serviceType)
    }
    
    static func nudgingAwayMessage(businessLocation : BusinessLocation) -> String {
        let title = TwitterHandles().getTwitterHandleForBusiness(businessLocation.canonicalName)
        let worstIssue = businessLocation.worstIssue.name
        //return "Just left \(title) w/o spending any $ bc they're bad for \(worstIssue) per #N4C Find more ethical options via @nudgeforchange"
        return "Just left 💩 \(title) 💩 w/o spending any $ bc they're bad for \(worstIssue) per ❤️ #N4C ❤️ Find more ethical options via @nudgeforchange"
        //return "Just left \(title) w/o spending any $ bc they're bad for \(worstIssue) Find more options via @nudgeforchange #Makeachange"
    }
    
    static func nudgingToMessage(businessLocation : BusinessLocation) -> String {
        let title = TwitterHandles().getTwitterHandleForBusiness(businessLocation.canonicalName)
        let bestIssue = businessLocation.bestIssue.name
        return "Going to \(title) because of their high \(bestIssue) score per #N4C. Find more ethical options via @nudgeforchange."
    }
    
    static func nudgingAwayController(businessLocation : BusinessLocation!, serviceType: String!) -> SLComposeViewController? {
        if let shareController = SLComposeViewController(forServiceType: serviceType) {
            switch serviceType {
            case SLServiceTypeFacebook:
                shareController.setInitialText("#N4C")
                shareController.add(URL(string: "https://nudgeforchange.com"))
                shareController.completionHandler = { (result) in
                    switch result {
                    case .done:
                        AnalyticsService.shared.track(AnalyticsEvent.facebookShared)
                    case .cancelled:
                        AnalyticsService.shared.track(AnalyticsEvent.facebookCancelled)
                    }
                }
            case SLServiceTypeTwitter:
                shareController.setInitialText(self.nudgingAwayMessage(businessLocation: businessLocation))
                shareController.completionHandler = { (result) in
                    switch result {
                    case .done:
                        AnalyticsService.shared.track(AnalyticsEvent.twitterShared)
                    case .cancelled:
                        AnalyticsService.shared.track(AnalyticsEvent.twitterCancelled)
                    }
                }
            default:
                break
            }
            return shareController
        } else {
            return nil
        }
    }
    
    static func nudgingToController(businessLocation : BusinessLocation, serviceType: String!) -> SLComposeViewController? {
        if let shareController = SLComposeViewController(forServiceType: serviceType) {
            switch serviceType {
            case SLServiceTypeFacebook:
                shareController.setInitialText("#N4C")
                shareController.add(URL(string: "https://nudgeforchange.com"))
                shareController.completionHandler = { (result) in
                    switch result {
                    case .done:
                        AnalyticsService.shared.track(AnalyticsEvent.facebookShared)
                    case .cancelled:
                        AnalyticsService.shared.track(AnalyticsEvent.facebookCancelled)
                    }
                }
            case SLServiceTypeTwitter:
                shareController.setInitialText(self.nudgingToMessage(businessLocation: businessLocation))
                shareController.completionHandler = { (result) in
                    switch result {
                    case .done:
                        AnalyticsService.shared.track(AnalyticsEvent.twitterShared)
                    case .cancelled:
                        AnalyticsService.shared.track(AnalyticsEvent.twitterCancelled)
                    }
                }
            default:
                break
            }
            return shareController
        } else {
            return nil
        }
    }
    
    /*
    static func nudgingAwayFacebookController(businessName: String, worstIssueName: String) -> SLComposeViewController? {
        return self.nudgingAwayController(businessName: businessName, worstIssueName: worstIssueName, serviceType: SLServiceTypeFacebook)
    }
    
    static func nudgingAwayTwitterController(businessName: String, worstIssueName: String) -> SLComposeViewController? {
        return self.nudgingAwayController(businessName: businessName, worstIssueName: worstIssueName, serviceType: SLServiceTypeTwitter)
    }
 */

}
