//
//  StarRatingView.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/22/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class StarRatingView: UIStackView {
    func starImageView(name: String) -> UIImageView {
        return UIImageView(image: UIImage(named: name))
    }
    
    func configureForScore(_ score: Int) {
        for view: UIView in self.arrangedSubviews {
            self.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
        
        let colorFullCount: Int = score / 2
        let colorHalfCount: Int = score % 2
        let grayCount: Int = 5 - (colorFullCount + colorHalfCount)
        var starColorFullName: String = ""
        var starColorHalfName: String = ""
        switch colorFullCount {
        case 0...2:
            starColorFullName = "StarRedFull"
            starColorHalfName = "StarRedHalf"
        case 3:
            starColorFullName = "StarYellowFull"
            starColorHalfName = "StarYellowHalf"
        case 4...5:
            starColorFullName = "StarGreenFull"
            starColorHalfName = "StarGreenHalf"
        default:
            break
        }
        
        for _ in 0..<colorFullCount {
            self.addArrangedSubview(self.starImageView(name: starColorFullName))
        }
        for _ in 0..<colorHalfCount {
            self.addArrangedSubview(self.starImageView(name: starColorHalfName))
        }
        for _ in 0..<grayCount {
            self.addArrangedSubview(self.starImageView(name: "Star"))
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
