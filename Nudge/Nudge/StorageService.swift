//
//  StorageService.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/22/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import Foundation

protocol NudgeObserver: AnyObject {
    func nudgesDidChange()
}

class StorageService {
    
    static let sharedInstance: StorageService = StorageService()
    
    
    // MARK: - Location
    
    var monitoredBusinessLocations: [BusinessLocation] = {
        if let data = UserDefaults.standard.data(forKey: AppStrings.savedBusinessLocationsKey) {
            let monitored = NSKeyedUnarchiver.unarchiveObject(with: data) as! [BusinessLocation]
            return monitored
        } else {
            return [BusinessLocation]()
        }
    }()
    
    func saveMonitoredBusinessLocations() {
        let data = NSKeyedArchiver.archivedData(withRootObject: self.monitoredBusinessLocations)
        UserDefaults.standard.set(data, forKey: AppStrings.savedBusinessLocationsKey)
        UserDefaults.standard.synchronize()
    }
    
    
    // MARK: - Nudges
    
    var nudgeObservers: [NudgeObserver] = [NudgeObserver]()
    
    var nudges: [Nudge] = {
        let data = UserDefaults.standard.data(forKey: AppStrings.nudgesKey)
        if data == nil {
            return [Nudge]()
        } else {
            let nudges = NSKeyedUnarchiver.unarchiveObject(with: data!) as! [Nudge]
            return nudges
        }
    }()
    {
        didSet {
            self.nudgeObservers.forEach { (observer) in
                observer.nudgesDidChange()
            }
            self.saveNudges()
        }
    }
    
    func addNudge(_ nudge: Nudge) {
        self.nudges.insert(nudge, at: 0)
    }
    
    func saveNudges() {
        var nudges: [Nudge] = self.nudges
        
        let limit = 100
        if nudges.count > limit {
            nudges = Array<Nudge>(nudges[0..<limit]) // limit to last 20
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: nudges)
        UserDefaults.standard.set(data, forKey: AppStrings.nudgesKey)
        UserDefaults.standard.synchronize()
    }
    
    func addNudgesObserver(_ observer: NudgeObserver) {
        if !self.nudgeObservers.contains(where: { (existingObserver) -> Bool in
            existingObserver === observer
        }) {
            self.nudgeObservers.append(observer)
        }
    }
    
    func removeNudgesObserver(_ observer: NudgeObserver) {
        if let index = self.nudgeObservers.index(where: { (existingObserver) -> Bool in
            return existingObserver === observer
        }) {
            self.nudgeObservers.remove(at: index)
        }
    }
    
    
    // MARK: - No Nudge List
    
//    var whitelistedBusinesses: [String]
//    var whitelistedBusinessLocations: [String]
//    var whitelist: [String] {
//        get {
//            
//        }
//    }
}
