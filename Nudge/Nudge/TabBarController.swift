//
//  TabBarController.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/11/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    var didShowOnboarding: Bool = false
    var onboardingPageVC: OnboardingPageViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Check if user went through onboarding, show if not.
        // Check if location tracking enabled, show Enable Location Tracking screen if not.
        // Check if notifications enabled, show Enable Notifications screen if not?
//        self.tabBar.layer.borderColor = UIColor.clear.cgColor
//        self.tabBar.layer.borderWidth = 0
        self.tabBar.shadowImage = UIImage()
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.tabBar.layer.shadowRadius = 4
        self.tabBar.layer.shadowOpacity = 1.0/5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !SettingsService.sharedInstance.didCompleteOnboarding {
            self.view.isHidden = true
        }
        if self.didShowOnboarding {
            self.didShowOnboarding = false
            self.view.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !SettingsService.sharedInstance.didCompleteOnboarding {
            self.didShowOnboarding = true
            self.showQuestionaire()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showQuestionaire() {
        let storyBoard = UIStoryboard(name: "OnboardingPageViewController", bundle: nil)
        self.onboardingPageVC = (storyBoard.instantiateViewController(withIdentifier: "OnboardingPageVC") as! OnboardingPageViewController)
        self.onboardingPageVC!.modalPresentationCapturesStatusBarAppearance = true
        
        //setup the navigation controller
        let navCon = UINavigationController(rootViewController: self.onboardingPageVC!)
        navCon.isNavigationBarHidden = true
        self.present(navCon, animated: false, completion: nil)
    }

}
