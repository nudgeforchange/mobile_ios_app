//
//  TabbedWebViewController.swift
//  Nudge
//
//  Created by Ultron on 5/10/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit
import MaterialComponents

class TabbedWebViewController: UIViewController {
    
    var webView : UIWebView!
    var webLinks : [String]!
    var tabBar : MDCTabBar!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabBarContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 48))
        navigationBar.backgroundColor = AppColors.nudgeMutedGrayColor
        let doneButton = UINavigationItem(title: "Sources")
        doneButton.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(TabbedWebViewController.doneButtonPressed))
        navigationBar.setItems([doneButton], animated: false)
        view.addSubview(navigationBar)
        tabBar = MDCTabBar(frame: CGRect(x: 0, y: navigationBar.frame.size.height, width: view.frame.size.width, height: view.frame.size.height-navigationBar.frame.size.height))
        tabBar.delegate = self
        var tabItems : [UITabBarItem] = []
        for i in 0...webLinks.count-1 {
            let url = URL(string: webLinks[i])!
            let host = url.host!
            let hostComponents = host.components(separatedBy: ".")
            let count = hostComponents.count
            var titleString = ""
            if count >= 2 {
                titleString = hostComponents[count-2] + "." + hostComponents[count-1]
            } else {
                titleString = host
            }
            let tabItem = UITabBarItem(title: titleString, image: nil, tag: i)
            tabItems.append(tabItem)
        }
        tabBar.items = tabItems
        tabBar.itemAppearance = .titles
        tabBar.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabBar.sizeToFit()
        tabBar.backgroundColor = AppColors.nudgeMutedGrayColor
        tabBar.tintColor = UIColor.white
        view.addSubview(tabBar)
        webView = UIWebView(frame: CGRect(x: 0, y: navigationBar.frame.size.height+48, width: view.frame.size.width, height: view.frame.size.height))
        webView.scrollView.isScrollEnabled = true
        webView.scalesPageToFit = true
        webView.isUserInteractionEnabled = true
        let firstURL = URL(string: webLinks[0])
        webView.loadRequest(URLRequest(url: firstURL!))
        view.addSubview(webView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doneButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TabbedWebViewController : MDCTabBarDelegate {
    
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        //print(item.tag)
        let url = URL(string: webLinks[item.tag])!
        webView.loadRequest(URLRequest(url: url))
    }
}
