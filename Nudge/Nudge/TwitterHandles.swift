//
//  TwitterHandles.swift
//  Nudge
//
//  Created by Thanos on 9/25/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class TwitterHandles: NSObject {

    public func getTwitterHandleForBusiness(_ businessName : String) -> String {
        var title = businessName
        switch businessName {
        case "Walgreens":
            title = "@walgreens"
            break
        case "Whole Foods Market":
            title = "@WholeFoods"
            break
        case "Apple":
            title = "@Apple"
            break
        case "Rite Aid":
            title = "@riteaid"
            break
        case "CVS":
            title = "@CVSHealth"
            break
        case "Aldi":
            title = "@AldiUSA"
            break
        case "Trader Joe's":
            title = "@TraderJoesUSA"
            break
        case "Verizon":
            title = "@Verizon"
            break
        case "Comcast":
            title = "@Comcast @Xfinity"
            break
        case "Nordstrom":
            title = "@Nordstrom"
            break
        case "Nordstrom Rack":
            title = "@Nordstromrack"
            break
        case "Gap":
            title = "@Gap"
            break
        case "Banana Republic":
            title = "@BananaRepublic"
            break
        case "Old Navy":
            title = "@oldNavy"
            break
        case "Intermix":
            title = "@Intermix"
            break
        case "Athleta":
            title = "@Athleta"
            break
        case "JCPenney":
            title = "@jcpenney"
            break
        case "Bed Bath & Beyond":
            title = "@BedBathBeyond"
            break
        case "buybuy Baby":
            title = "@buybuyBABY"
            break
        case "World Market":
            title = "@worldmarket"
            break
        case "Cost Plus World Market":
            title = "@worldmarket"
            break
        case "AT&T":
            title = "@ATT"
            break
        case "SuperValu":
            title = "@SuperValuPR"
            break
        case "Hornbacher's":
            title = "@Hornbachers"
            break
        case "Shop 'n Save":
            title = "@Shopnsave"
            break
        case "Shoppers Food":
            title = "@Shoppers"
            break
        case "Cub Foods":
            title = "@Cubfoods"
            break
        case "Farm Fresh":
            title = "@FarmFresh"
            break
        case "Victoria's Secret":
            title = "@VictoriasSecret"
            break
        case "Bath and Body Works":
            title = "@BathBodyWorks"
            break
        case "BI-LO":
            title = "@BILOSuperSaver"
            break
        case "Winn-Dixie":
            title = "@WinnDixie"
            break
        case "Fresco ye Mas":
            title = "@Frescoymas"
            break
        case "7-eleven":
            title = "@7eleven"
            break
        case "Health Mart Pharmacy":
            title = "@healthmart"
            break
        case "Toys\"R\"Us":
            title = "@ToysRUs"
            break
        case "Babies\"R\"Us":
            title = "@BabiesRUs"
            break
        case "Family Fair":
            title = "@ShopFamilyFare"
            break
        case "DICK's Sporting Goods":
            title = "@DICKS"
            break
        case "Affinity Sports":
            title = "@AffinitySports_"
            break
        case "Blue Sombrero":
            title = "@blue_sombrero"
            break
        case "Field & Stream":
            title = "@FieldStreamShop"
            break
        case "Galyan's":
            title = "@DICKS"
            break
        case "Golf Galaxy":
            title = "@golfgalaxy"
            break
        case "The GolfWorks":
            title = "@GolfWorksTeam"
            break
        case "Sport's Authority":
            title = "@SportsAuthority"
            break
        case "True Runner":
            title = "@TrueRunnerShop"
            break
        case "Office Depot":
            title = "@OfficeDepot"
            break
        case "OfficeMax":
            title = "@OfficeDepot"
            break
        case "Grand & Toy":
            title = "@GrandandtoyCA"
            break
        case "Viking Direct":
            title = "@viking_chat"
            break
        case "Applebee's":
            title = "@Applebees"
            break
        case "IHOP":
            title = "@IHOP"
            break
        case "Sherwin-Williams Paint Store":
            title = "@SherwinWilliams"
            break
        case "Dutch Boy":
            title = "@DutchBoyPaints"
            break
        case "Pratt & Lambert":
            title = "@PrattandLambert"
            break
        case "Martin-Senour":
            title = "@MartinSenourAP"
            break
        case "Olive Garden":
            title = "@Olivegarden"
            break
        case "Red Lobster":
            title = "@redlobster"
            break
        case "Longhorn Steakhouse":
            title = "@LongHornSteaks"
            break
        case "Bahama Breeze":
            title = "@BahamaBreeze"
            break
        case "Eddie V's Prime Seafood":
            title = "@EddieVs"
            break
        case "The Capital Grille":
            title = "@CapitalGrille"
            break
        case "Yarde House":
            title = "@YardHouse"
            break
        case "GameStop":
            title = "@GameStop"
            break
        case "EB Games":
            title = "@EBGamesCanada"
            break
        case "Kongregate":
            title = "@Kongregate"
            break
        case "ThinkGeek":
            title = "@thinkgeek"
            break
        case "Geeknet":
            title = "@GameStop"
            break
        case "Dillard's":
            title = "@Dillard's"
            break
        case "Hudson's Bay":
            title = "@Hudson's Bay"
            break
        case "Lord & Taylor":
            title = "@LordandTaylor"
            break
        case "Saks Fifth Avenue":
            title = "@saks"
            break
        case "Galeria Kaufhof":
            title = "@galeriakaufhof"
            break
        case "Saks OFF 5th":
            title = "@saks"
            break
        case "Gilt":
            title = "@Gilt"
            break
        case "Home Outfitters":
            title = "@HomeOutfitters"
            break
        case "Petsmart":
            title = "@PetSmart"
            break
        case "Tractor Supply Co.":
            title = "@TractorSupply"
            break
        case "Foot Locker":
            title = "@Footlocker"
            break
        case "Kids Foot Locker":
            title = "@KidsFootLocker"
            break
        case "Lady Foot Locker":
            title = "@LadyFootLocker"
            break
        case "Champs Sports":
            title = "@Champssports"
            break
        case "Big Lots":
            title = "@BigLots"
            break
        case "Burlington Coat Factory":
            title = "@Burlington"
            break
        case "Walmart":
            title = "@Walmart"
            break
        case "Dollar General":
            title = "@DollarGeneral"
            break
        case "Dollar Tree":
            title = "@DollarTree"
            break
        case "Sam's Club":
            title = "@SamsClub"
            break
        case "PictureMe! Portrait Studios":
            title = "@PicMePortraits"
            break
        case "Costco Wholesale":
            title = "@Costco"
            break
        case "The Home Depot":
            title = "@HomeDepot"
            break
        case "Target":
            title = "@Target"
            break
        case "SuperTarget":
            title = "@Target"
            break
        case "CityTarget":
            title = "@Target"
            break
        case "Lowe's Home Improvement":
            title = "@Lowes"
            break
        case "T.J. Maxx":
            title = "@TJmaxx"
            break
        case "WINNERS":
            title = "@Winners"
            break
        case "HomeGoods":
            title = "@HomeGoods"
            break
        case "HomeSense":
            title = "@Homesense"
            break
        case "Sierra Trading Post":
            title = "@Sierratp"
            break
        case "Marshalls":
            title = "@Marshalls"
            break
        case "Marshalls Mega Store":
            title = "@Marshalls"
            break
        case "Best Buy":
            title = "@BestBuy"
            break
        case "McDonald's":
            title = "@McDonalds"
            break
        case "Chick-Fil-A":
            title = "@ChickfilA"
            break
        case "In-N-Out":
            title = "@innoutburger"
            break
        case "KFC":
            title = "@KFC"
            break
        case "Taco Bell":
            title = "@Tacobell"
            break
        case "Pizza Hut":
            title = "@Pizzahut"
            break
        case "Subway":
            title = "@Subway"
            break
        case "Chipotle Mexican Grill":
            title = "@ChipotleTweets"
            break
        case "Jack in the Box":
            title = "@JackBox"
            break
        case "Qdoba Mexican Grill":
            title = "@qdoba"
            break
        case "Burger King":
            title = "@BurgerKing"
            break
        case "Tim Hortons":
            title = "@TimHortons"
            break
        case "Carl's Jr.":
            title = "@CarlsJr"
            break
        case "Hardee's":
            title = "@Hardees"
            break
        case "Green Burrito":
            title = "@CarlsJr"
            break
        case "Dunkin' Donuts":
            title = "@DunkinDonuts"
            break
        case "Baskin-Robbins":
            title = "@BaskinRobbins"
            break
        case "Wendy's":
            title = "@Wendys"
            break
        case "Starbucks":
            title = "@Starbucks"
            break
        case "Williams-Sonoma":
            title = "@williamsSonoma"
            break
        case "Pottery Barn":
            title = "@PotteryBarn"
            break
        case "Pottery Barn Kids":
            title = "@PotteryBarnKids"
            break
        case "West Elm":
            title = "@westelm"
            break
        case "Mark and Graham":
            title = "@markandgraham"
            break
        case "Rejuvenation":
            title = "@RejuvenationInc"
            break
        case "PB Teens":
            title = "@potterybarnteen"
            break
        case "Williams-Sonoma Home":
            title = "@WilliamsSonoma"
            break
        case "Ann Taylor":
            title = "@AnnTaylor"
            break
        case "LOFT":
            title = "@LOFT"
            break
        case "LOFT Outlet":
            title = "@LOFT"
            break
        case "Dressbarn":
            title = "@dressbarn"
            break
        case "Lou & Grey":
            title = "@louandgrey"
            break
        case "Lane Bryant":
            title = "@lanebryant"
            break
        case "Catherines":
            title = "@CatherinesPlus"
            break
        case "Maurices":
            title = "@Maurices"
            break
        case "Zales":
            title = "@zales"
            break
        case "Kay Jewelers":
            title = "@KayJewelers"
            break
        case "Jared":
            title = "@ThatsJared"
            break
        case "Panera Bread":
            title = "@Panerabread"
            break
        case "Paradise Bakery & Cafe":
            title = "@Paradise_Bakery"
            break
        case "Saint Louis Bread Company":
            title = "@Panerabread"
            break
        case "IKEA":
            title = "@IKEAUSA"
            break
        case "Domino's Pizza":
            title = "@dominos"
            break
        case "Peet's Coffee & Tea":
            title = "@Peetscoffee"
            break
        case "Krispy Kreme Doughnuts":
            title = "@krispykreme"
            break
        case "Stumptown Coffee Roasters":
            title = "@stumptowncoffee"
            break
        case "Caribou Coffee":
            title = "@caribou_coffee"
            break
        case "Einstein Bros. Bagels":
            title = "@EinsteinBros"
            break
        case "Green Mountain Coffee Roasters":
            title = "@keurig"
            break
        case "Jacobs Douwe Egberts":
            title = "@DouweEgbertsNL"
            break
        case "Panda Express":
            title = "@PandaExpress"
            break
        case "Big 5 Sporting Goods":
            title = "@big5since55"
            break
        case "Jo-Ann Fabrics":
            title = "@JoAnn_Stores"
            break
        case "Jo-Ann Etc.":
            title = "@JoAnn_Stores"
            break
        case "Sprouts Farmers Markets":
            title = "@sproutsfm"
            break
        case "Philz coffee":
            title = "@PhilzCoffee"
            break
        case "P.F. Chang's":
            title = "@PFChangs"
            break
        case "Fry's Electronics":
            title = "@Fryselectronics"
            break
        case "Lion Food Center":
            title = "@FoodLion"
            break
        case "Stop and Shop New England":
            title = "@StopandShop"
            break
        case "Stop and Shop New York":
            title = "@StopandShop"
            break
        case "Hannaford Supermarket":
            title = "@Hannaford"
            break
        case "GIANT Food Stores":
            title = "@GiantFood"
            break
        case "Martin's Food Market":
            title = "@MartinsFoodMkts"
            break
        case "Giant Landover":
            title = "@GiantFood"
            break
        case "Peapod by Giant Food":
            title = "@peapodDelivers"
            break
        case "bFresh":
            title = "@bfreshmarket"
            break
        case "Bi-Mart":
            title = "@CareeratBiMart"
            break
        case "Exxon Mobil Gas and Car Wash":
            title = "@ExxonMobil"
            break
        case "Mobil":
            title = "@ExxonMobil"
            break
        case "Shell":
            title = "@shell"
            break
        case "Pennzoil 10 Minute Oil Change":
            title = "@Pennzoil"
            break
        case "Kroger":
            title = "@Kroger"
            break
        case "Baker's Supermarkets":
            title = "@BakersGrocery"
            break
        case "City Market":
            title = "@CityMarket"
            break
        case "Copps Food Center":
            title = "@RndysFoundation"
            break
        case "Dillons Food Stores":
            title = "@DillonsGrocery"
            break
        case "Gerbers Super Markets":
            title = "@GerbesMarket"
            break
        case "Food 4 Less":
            title = "@Food4Less"
            break
        case "Foods Co.":
            title = "@Food4Less"
            break
        case "Fry's Food and Drug":
            title = "@FrysFoodStores"
            break
        case "Harris Teeter":
            title = "@HarrisTeeter"
            break
        case "Jay C Foods":
            title = "@Kroger"
            break
        case "King Soopers":
            title = "@MyKingSoopers"
            break
        case "Mariano's Fresh Market":
            title = "@RndysFoundation"
            break
        case "Metro Market":
            title = "@RndysFoundation"
            break
        case "Owen's Market":
            title = "@Kroger"
            break
        case "Pay Less Super Markets":
            title = "@Kroger"
            break
        case "Pick 'n Save":
            title = "@RndysFoundation"
            break
        case "QFC":
            title = "@QFCGrocery"
            break
        case "Ralphs Grocery":
            title = "@RalphsGrocery"
            break
        case "Ruler Foods":
            title = "@Kroger"
            break
        case "Smith's":
            title = "@MySmithsGrocery"
            break
        case "Dillons Marketplace":
            title = "@DillonsGrocery"
            break
        case "Fry's Marketplace":
            title = "@FrysFoodStores"
            break
        case "Kroger Marketplace":
            title = "@Kroger"
            break
        case "Smith's Marketplace":
            title = "@MySmithsGrocery"
            break
        case "CVS Pharmacy":
            title = "@CVSHealth"
            break
        case "Arbor Drugs":
            title = "@CVSHealth"
            break
        case "Omnicare":
            title = "@Omnicare"
            break
        case "CVS Caremark":
            title = "@CVSHealth"
            break
        case "Albertsons":
            title = "@Albertsons"
            break
        case "Albertsons Market":
            title = "@Albertsons"
            break
        case "Acme":
            title = "@AcmeMarkets"
            break
        case "Amigos":
            title = "@AmigosTexas"
            break
        case "Carrs":
            title = "@safeway"
            break
        case "Jewel-Osco":
            title = "@jewelosco"
            break
        case "Market Street Grocery":
            title = "@MarketStreet_TX"
            break
        case "Pavilions":
            title = "@Pavilions"
            break
        case "Randalls":
            title = "@Randalls_Stores"
            break
        case "Safeway":
            title = "@safeway"
            break
        case "Shaws":
            title = "@Shaws"
            break
        case "Star Market":
            title = "@Star_Mrkt"
            break
        case "SuperSaver":
            title = "@SuperSaverFoods"
            break
        case "Tom Thumb":
            title = "@TomThumb_Stores"
            break
        case "Vons":
            title = "@vons"
            break
        case "United Supermarkets":
            title = "@UnitedWestTexas"
            break
        case "Sears":
            title = "@SearsDeals"
            break
        case "Kmart":
            title = "@KmartDeals"
            break
        case "Publix":
            title = "@Publix"
            break
        case "Hobby Lobby":
            title = "@HobbyLobby"
            break
        case "LL Bean":
            title = "@LLBean"
            break
        case "Macy's":
            title = "@Macys"
            break
        case "Bloomingdale's":
            title = "@Macys"
            break
        case "Bon-Ton":
            title = "@bonton"
            break
        case "Bergner's":
            title = "@bonton"
            break
        case "Boston Store":
            title = "@BostonStore"
            break
        case "Carson's":
            title = "@CarsonsStores"
            break
        case "Elder-Beerman":
            title = "@ElderBeerman"
            break
        case "Herberger's":
            title = "@Herberger's"
            break
        case "Younkers":
            title = "@Younkers"
            break
        case "Century 21":
            title = "@century21stores"
            break
        case "DSW":
            title = "@DSWShoeLovers"
            break
        case "Ross Dress for Less":
            title = "@Ross_Stores"
            break
        case "Starbucks":
            title = "@Starbucks"
            break
        case "Godfather's Pizza":
            title = "@godfatherspizza"
            break
        case "Pep Boys":
            title = "@pepboysauto"
            break
        case "True Value Hardware":
            title = "@TrueValue"
            break
        case "Grand Rental Station":
            title = "@TrueValue"
            break
        case "Party Central":
            title = "@TrueValue"
            break
        case "Taylor Rental":
            title = "@TrueValue"
            break
        case "Induserve Supply":
            title = "@TrueValue"
            break
        case "Home & Garden Showplace":
            title = "@TrueValue"
            break
        case "Trump International Hotel":
            title = "@TrumpHotels"
            break
        case "Trump Winery Tasting Room":
            title = "@TrumpWinery"
            break
        case "Trump Golf":
            title = "@TrumpGolf"
            break
        case "Scion Hotels":
            title = "@TrumpHotels"
            break
        case "Wegman's":
            title = "@TrumpHotels"
            break
        case "Perfumania":
            title = "@Perfumania"
            break
        case "Stein Mart":
            title = "@SteinMart"
            break
        case "Belk":
            title = "@belk"
            break
        case "Coors Brewing Company":
            title = "@MillerCoors"
            break
        case "Millers Brewing Company":
            title = "@MillerCoors"
            break
        case "New Balance Factor Store":
            title = "@NewBalance"
            break
        case "Spencers Gifts":
            title = "@Spencers"
            break
        case "Under Armour Factor House":
            title = "@UnderArmour"
            break
        case "Wollman Skating Rink":
            title = "@WollmanRink"
            break
        case "Michaels":
            title = "@MichaelsStores"
            break
        case "Aaron Brothers":
            title = "@AaronBrothers"
            break
        case "Pat Catan's":
            title = "@PatCatans"
            break
        case "Save Mart":
            title = "@SaveMart"
            break
        case "S-Mart Food":
            title = "@SaveMart"
            break
        case "Lucky":
            title = "@luckyfoods"
            break
        case "FoodMaxx":
            title = "@SaveMart"
            break
        case "Academy Sports + Outdoors":
            title = "@academy"
            break
        case "Barnes and Noble":
            title = "@BNBuzz"
            break
        case "Chevron":
            title = "@chevron"
            break
        case "Sonic":
            title = "@sonicdrivein"
            break
        case "Cyclops Grill":
            title = "@EvilGrill"
            break
        default:
            break
        }
        return title
    }
}
