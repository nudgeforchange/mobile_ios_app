//
//  UnscoredBusinessCell.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 3/19/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class UnscoredBusinessCell: UITableViewCell {
    
    @IBOutlet weak var topRoundedShadowView: RoundedShadowView!
    @IBOutlet weak var middleRoundedShadowView: RoundedShadowView!
    @IBOutlet weak var bottomRoundedShadowView: RoundedShadowView!
    
    @IBOutlet weak var businessNameLabel: UILabel!
    @IBOutlet weak var businessNameCenterY: NSLayoutConstraint!
    @IBOutlet weak var distanceLabel: UILabel!
    
    fileprivate var visibleRoundedShadowView: RoundedShadowView!
    
    var businessLocation: BusinessLocation! {
        didSet {
            self.businessNameLabel.text = businessLocation.name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.backgroundColor = UIColor.clear
        self.visibleRoundedShadowView = self.middleRoundedShadowView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.visibleRoundedShadowView.backgroundColor = selected ? UIColor(white: 0.95, alpha: 1) : UIColor.white
    }
    
    func setTop() {
        self.topRoundedShadowView.isHidden = false
        self.middleRoundedShadowView.isHidden = true
        self.bottomRoundedShadowView.isHidden = true
        self.visibleRoundedShadowView = self.topRoundedShadowView
        self.businessNameCenterY.constant = 0
    }
    
    func setMiddle() {
        self.topRoundedShadowView.isHidden = true
        self.middleRoundedShadowView.isHidden = false
        self.bottomRoundedShadowView.isHidden = true
        self.visibleRoundedShadowView = self.middleRoundedShadowView
        self.businessNameCenterY.constant = 0
    }
    
    func setBottom() {
        self.topRoundedShadowView.isHidden = true
        self.middleRoundedShadowView.isHidden = true
        self.bottomRoundedShadowView.isHidden = false
        self.visibleRoundedShadowView = self.bottomRoundedShadowView
        self.businessNameCenterY.constant = 1
    }
    
}
