//
//  WebViewController.swift
//  Nudge
//
//  Created by Walter Kim (NFC) on 4/6/17.
//  Copyright © 2017 Level 3 Studios. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var statusBarBackgroundView: UIView!
    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    var titleText: String!
    var url: String!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gradientColor(height: CGFloat) -> UIColor {
        let colors = [AppColors.nudgeRedColor, AppColors.nudgeOragneColor]
        return UIColor(gradientStyle: .topToBottom, withFrame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: height), andColors: colors)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.statusBarBackgroundView.backgroundColor = AppColors.nudgeRedColor
        self.titleBarView.backgroundColor = self.gradientColor(height: 44)
        self.titleBarView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.titleBarView.layer.shadowRadius = 10
        self.titleBarView.layer.shadowOpacity = 1.0/5.0

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.titleLabel.text = self.titleText
        let requestURL = URL(string: self.url)!
        self.webView.loadRequest(URLRequest(url: requestURL))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}

extension WebViewController: UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.scrollView.contentInset = UIEdgeInsets.zero
        webView.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        webView.scrollView.contentSize = CGSize(width: webView.frame.size.width, height: webView.scrollView.contentSize.height)
    }
    
}
